{% if header %}
<div class="meso" id="meso{{num}}">
    <div class="card">
        <div class="card-header">
            <h4>
            {% if meso['url'] %}
            <a href="{{ meso['url'] }}">{{ meso['name'] }}</a>
            {% else %}
            {{ meso['name'] }}
            {% endif %}
            {% if meso['gpscoordinates'] %}
            <div>
                <a href="https://www.openstreetmap.org/?mlat={{meso['gpscoordinates'][0]}}&mlon={{meso['gpscoordinates'][1]}}&zoom=16">
                <img src="../theme/img/position_full.png" alt="Position" title="Position" />
                </a>
            </div>
            <div style="display:none" class="meso_marker" data-gps='[" <a href=&#39;#meso{{ num }}&#39; > {{ meso['name'] }} </a>", "{{ meso['gpscoordinates'][0] }}", "{{ meso['gpscoordinates'][1] }}" ]'>
            </div>
            {% endif %}
            </h4>
            <div class="plus-minus plus" onclick="$(this).closest('.card-header').find('.collapse').collapse('toggle'); $(this).toggleClass('plus minus')"></div>
            <p>
              {{ meso['shortdescription'] }}
            </p>
            <p class="collapse">
              {{ meso['fulldescription'] }}
            </p>

        </div>
        <div class="card-body">
            {% if meso['contactname'] %}
                <p><img src="../theme/img/mail.png" alt="Contact" title="Contact" />
                {{ meso['contactname'] }}
                </p>
            {% endif %}
            {% if meso['institutesname'] %}
                <p><img src="../theme/img/employer.png" alt="Institutions" title="Institutions" />
                {{ meso['institutesname'] | join_names }}
                </p>
            {% endif %}
            {% if meso['financersname'] %}
                <p><img src="../theme/img/piggy_bank.png" alt="Financeurs" title="Financeurs" />
                {{ meso['financersname'] | join_names }}
                </p>
            {% endif %}
            {% if meso['accesspolicy'] %}
                <p><img src="../theme/img/contract.png" alt="Conditions d'accès" title="Conditions d'accès" />
                {{ meso['accesspolicy'] | join_sentences }}
                </p>
            {% endif %}
            {% if meso['servicename'] %}
                <p><img src="../theme/img/services.png" alt="Services proposés" title="Services proposés" />
                {{ meso['servicename'] | join_sentences }}
                </p>
            {% endif %}

        </div>
        <div class="card-body row">
            {% if meso['mesocorenumber'] %}
                <p class="col-sm-3"><img src="../theme/img/cpu.png" alt="Nombre de cœurs" title="Nombre de cœurs" />
                {{ meso['mesocorenumber'] }} cœurs
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/cpu.png" alt="Nombre de cœurs" title="Nombre de cœurs" />
                </p>
            {% endif %}
            {% if meso['mesoramsize'] %}
                <p class="col-sm-3"><img src="../theme/img/memory.png" alt="Mémoire" title="Mémoire" />
                {{ meso['mesoramsize'] | format_storage(3) }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/memory.png" alt="Mémoire" title="Mémoire" />
                </p>
            {% endif %}
            {% if meso['mesostoragesize'] %}
                <p class="col-sm-3"><img src="../theme/img/storage.png" alt="Stockage" title="Stockage"/>
                {{ meso['mesostoragesize'] | format_storage(4) }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/storage.png" alt="Stockage" title="Stockage"/>
                </p>
            {% endif %}
            {% if meso['mesogpunumber'] %}
                <p class="col-sm-3"><img src="../theme/img/gpu.png" alt="Nombre de GPUs" title="Nombre de GPUs"/>
                {{ meso['mesogpunumber'] }} GPUs
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/gpu.png" alt="Nombre de GPUs" title="Nombre de GPUs"/>
                </p>
            {% endif %}
        </div>
        <div>
{% endif %}
{% if footer %}
        </div>
        <div class="card-footer">
        </div>
    </div>
</div>
{% endif %}
