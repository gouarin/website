from jinja2 import Template
from textwrap import dedent

# set locale in order to have french date for output string
import locale
locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")


def indico_event_to_rst(url, event):
    """
    Parse an indico event and return an rst like describing the schedule.

    Parameters
    ----------

    url: url
        url of the indico

    event: int
        event number

    Returns
    -------

    string: schedule of the event in a rst format

    """
    from datetime import date, datetime
    import maya
    import collections
    from urllib.request import urlopen
    import json
    import ssl

    context = ssl.SSLContext()

    def get_contributions(day: dict) -> list:
        """Return a list of contributions for given day"""

        contributions = []
        for session in day.values():
            if session.get('entries'):
                # This session contains contributions: add them to the list
                contributions += list(session['entries'].values())
            else:
                contributions.append(session)
        return contributions

    try:
        response = urlopen(url + '/export/timetable/' + str(event) + ".json",
                           context=context).read()
    except Exception as e:
        logger.warning(
            "indico content could not be fetched because of : {}".format(e))
        return None

    data = json.loads(response.decode('utf-8'))

    schedule = {}
    for day_k, day_v in data['results'][str(event)].items():
        if day_v:  # do not add day if empty
            date = datetime.strptime(day_k, '%Y%m%d')
            day = schedule.get(date, {})

            for contrib in get_contributions(day_v):
                start_time_str = contrib['startDate']['date'] + \
                    'T' + contrib['startDate']['time'] + 'Z'
                end_time_str = contrib['endDate']['date'] + \
                    'T' + contrib['endDate']['time'] + 'Z'
                start_time = maya.parse(start_time_str).datetime(
                    to_timezone='Europe/Paris', naive=False)
                end_time = maya.parse(end_time_str).datetime(
                    to_timezone='Europe/Paris', naive=False)

                day[start_time] = {
                    'start': start_time.strftime("%H:%M"),
                    'end': end_time.strftime("%H:%M"),
                    'type': contrib['entryType'],
                    'title': contrib['title'],
                }

                if contrib['entryType'] != 'Break':
                    speakers = []

                    try:
                        # A basic contribution
                        contributors = contrib['presenters']
                    except KeyError:
                        try:
                            # A session with conveners
                            contributors = contrib['conveners']
                        except KeyError:
                            contributors = ["Orateur à confirmer"]
                    for person in contributors:
                        speakers.append(person['firstName'].capitalize(
                        ) + ' ' + person['familyName'].capitalize())

                    resources = [mm['url'] for m in contrib['material']
                                 for mm in m['resources']]

                    day[start_time].update({
                        'description': contrib['description'].split('\n'),
                        'speaker': ', '.join(speakers),
                        'resources': resources,
                    })

            schedule[date] = collections.OrderedDict(sorted(day.items()))

    schedule = collections.OrderedDict(sorted(schedule.items()))

    return _indico_event_rst_template.render(schedule=schedule)


# Pre-loading Jinja2 template as static variable
_indico_event_rst_template = Template(dedent("""
                {%- for key, value in schedule.items() -%}
                .. day:: {{ key.strftime("%d-%m-%Y") }}
                    {%- for vv in value.values() %}
                        {% if vv['type'] == 'Break' %}
                        .. break_event:: {{ vv['title'] }}
                            :begin: {{ vv['start'] }}
                            :end: {{ vv['end'] }}

                        {% else %}
                        .. event:: {{ vv['title'] }}
                            :begin: {{ vv['start'] }}
                            :end: {{ vv['end'] }}
                            :speaker: {{ vv['speaker'] }}
                            {%- if vv['resources'] %}
                            :support:
                            {%- for r in vv['resources'] %}
                                [support {{ loop.index }}]({{ r }})
                            {%- endfor -%}
                            {% endif %}

                            {% for p in vv['description'] %}
                            {{ p }}
                            {%- endfor -%}
                        {% endif -%}
                    {% endfor %}
                {% endfor %}
                """))


if __name__ == '__main__':
    print(indico_event_to_rst('https://indico.mathrice.fr', 225))
