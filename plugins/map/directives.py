from docutils.parsers.rst import directives, Directive
from docutils import nodes

###############################################################################
# Nodes

class markers_map(nodes.General, nodes.Element):
    """ map """


###############################################################################
# Directives

class MarkersMap(Directive):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {
        'latitude': float,
        'longitude': float,
        'zoom': int,

    }
    has_content = False

    def run(self):
        node = markers_map()
        node["marker_name"] = self.arguments[0]
        for key in self.option_spec:
            node[key] = self.options.get(key, None)

        # Default values
        node['latitude'] = node['latitude'] or 46.227638
        node['longitude'] = node['longitude'] or 2.213749
        node['zoom'] = node['zoom'] or 5


        self.state.nested_parse(self.content, self.content_offset, node)

        return [node]


def register():
    directives.register_directive('map', MarkersMap)

