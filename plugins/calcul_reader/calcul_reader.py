# -*- coding: utf-8 -*-
""" Custom reader for Calcul website """

import logging
from datetime import date
import re

from pelican import signals, readers, contents
from pelican.readers import RstReader
from pelican.readers import MarkdownReader
from pelican.utils import get_date

logger = logging.getLogger(__name__)


class CalculReader:
    """
    A reader Metaclass for CalculRstReader and CalculMarkdownReader classes
    """

    enabled = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.custom_metadata_processors = self.settings.get(
            'CUSTOM_METADATA_PROCESSORS', dict())
        self.category_mandatory_metadata = self.settings.get(
            'CATEGORY_MANDATORY_METADATA', set())

    def process_metadata(self, name, value):
        """ Use custom metadata processors. """
        if name in self.custom_metadata_processors:
            return self.custom_metadata_processors[name](value, self.settings)
        else:
            return super().process_metadata(name, value)

    def _check_mandatory_metadata(self, metadata):
        """ Check mandatory metadata per category. """
        try:
            diff = self.category_mandatory_metadata[metadata['category']] \
                   - set(metadata.keys())
        except KeyError:
            return

        if diff:
            raise RuntimeError("Missing mandatory metadata"
                               " {} for category {}"
                               .format(str(diff), metadata['category']))

    def read(self, source_path):
        """Read article"""
        content, metadata = super().read(source_path)

        # Checking mandatory metadata
        self._check_mandatory_metadata(metadata)

        # Checking if event is finished
        if 'end_date' in metadata:
            metadata['is_finished'] = date.today() \
                                      > metadata['end_date'].date()

        # Adding the sort key equal to ...
        try:
            # ... start_date for an event
            metadata['sort_key'] = metadata['start_date']
        except KeyError:
            try:
                # ... date for other article
                metadata['sort_key'] = metadata['date']
            except KeyError:
                # ... nothing otherwise
                pass

        # Adding custom tag from file path (for scientific and technic events)
        path_match = re.search("/content/([^/]*)/", source_path)
        if path_match and path_match.group(1) in ['evt_sci', 'evt_tech']:
            metadata.setdefault('tags', list()).append(
                contents.Tag(path_match.group(1), self.settings))

        return content, metadata


class CalculRstReader(CalculReader, RstReader):
    """Custom Rst reader"""

    file_extensions = ['rst']


class CalculMarkdownReader(CalculReader, MarkdownReader):
    """Custom Markdown reader"""

    file_extensions = ['md']


def add_reader(readers):
    readers.reader_classes['rst'] = CalculRstReader
    readers.reader_classes['md'] = CalculMarkdownReader


def register():
    signals.readers_init.connect(add_reader)
