# -*- coding: utf-8 -*-
"""
Extract Table of Content
========================

A Pelican plugin to extract table of contents (ToC) from `article.content` and
place it in its own `article.toc` variable for use in templates.
"""

from os import path
from bs4 import BeautifulSoup
from pelican import signals, readers, contents
import logging

logger = logging.getLogger(__name__)


def extract_toc(content):
    if isinstance(content, contents.Static):
        return

    soup = BeautifulSoup(content._content, 'html.parser')
    filename = content.source_path
    extension = path.splitext(filename)[1][1:]
    toc_list = []

    # default reStructuredText reader
    if readers.RstReader.enabled and extension in readers.RstReader.file_extensions:
        toc = soup.find('div', class_='contents topic')
        if toc:
            toc.extract()
            tag = BeautifulSoup(str(toc), 'html.parser')
            for link in tag.find_all('a'):
                toc_list.append({'name': link.string,
                                 'link': link.get('href')})

    if toc_list:
        content.toc = toc_list
        content._content = soup.decode()

def register():
    signals.content_object_init.connect(extract_toc)
