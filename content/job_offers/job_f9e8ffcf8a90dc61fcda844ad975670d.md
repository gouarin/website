Title:  Identification et visualisation en temps réel des défauts de fabrication par LBM
Date: 2021-02-03 07:40
Slug: job_f9e8ffcf8a90dc61fcda844ad975670d
Category: job
Authors: Basile Marchand
Email: basile.marchand@mines-paristech.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Evry
Job_Duration: 6 mois
Job_Website: 
Job_Employer: Centre des Matériaux - Mines ParisTech
Expiration_Date: 2021-03-28
Attachment: job_f9e8ffcf8a90dc61fcda844ad975670d_attachment.pdf

Le travail dans ce stage consistera à la mise en place de la chaîne logicielle complète permettant l’acquisition et l’analyse en temps réel des images issues de l’acquisition infrarouge sur le procédé de fabrication additive LBM. L’objectif est d’avoir à disposition un outil d’identification en temps-réel des défauts géométriques issus de la fabrication par LBM. 