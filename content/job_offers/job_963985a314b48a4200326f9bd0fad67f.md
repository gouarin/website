Title: Study of the flow around the Leonardo Next-Generation Civil Tiltrotor aircraft
Date: 2021-06-08 08:46
Slug: job_963985a314b48a4200326f9bd0fad67f
Category: job
Authors: CCD 1 an
Email: hoarau@unistra.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Strasbourg
Job_Duration: 1 an
Job_Website: http://icube.unistra.fr/
Job_Employer: Université de Strasbourg
Expiration_Date: 2021-08-31
Attachment: job_963985a314b48a4200326f9bd0fad67f_attachment.pdf

In the context of the CleanSky European research program AFC4TR “Active Flow Control for Tilt-Rotor aircraft”, <https://www.afc4tr.eu> N° 886718, the flow around the Leonardo Next-Generation Civil Tiltrotor aircraft is examined. The objective of the study is to investigate the use of Active Flow Control using pulsed air blowing devices with Zero Net Mass Flux (ZNMF) actuators to control the flow separation around the wing. 3D simulations are carried out using the in-house code NSMB (Navier-Stokes Multi-Block) using HPC resources and supercomputers. Near stall configurations are considered at high angle of attack and high Reynolds number corresponding to the take-off configuration. The flow is characterized by the presence of corner vortices formed in junctions located near the fuselage and the nacelle which results in a loss of the aerodynamic performances. The flow is separated around the aileron and causes a considerable loss in its efficiency during the airplane maneuverability. This study examines these corner vortices using adapted turbulence models to capture their correct sizes. The rotation of the engine propellers is also considered due to the unsteady periodic effects produced on the pressure distribution of the wing and fuselage surfaces leading to an unstable evolution of these corner vortices. The study examines the effects of the ZNMF devices by producing a synthetic jet using a sinusoidal oscillating membrane or a piston to alternatively force the fluid through an orifice into the external flow field. These devices (also known as synthetic jet actuators) do not require external fluid supply of complex piping, in addition to their low cost and small size. This work focuses on the optimization of different parameters as the frequency of actuation, the velocity amplitude, the angle of the jet and the dimensions of the actuators to better reattach the flow through the enhancement of the mixing and momentum transfer within the boundary layer to explore the capabilities of the AFC applicability to a full-scale aircraft configuration at real flight conditions.
