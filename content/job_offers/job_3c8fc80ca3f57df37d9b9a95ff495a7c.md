Title: Ingénieur-e en visualisation, traitement et analyse de données
Date: 2020-07-01 05:38
Slug: job_3c8fc80ca3f57df37d9b9a95ff495a7c
Category: job
Authors: Mondher CHEKKI
Email: mondher.chekki@univ-grenoble-alpes.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Grenoble
Job_Duration: 1 an renouvelable
Job_Website: http://www.ige-grenoble.fr/
Job_Employer: CNRS
Expiration_Date: 2020-07-31
Attachment: job_3c8fc80ca3f57df37d9b9a95ff495a7c_attachment.pdf

Au sein de l’Institut des Géosciences de l’Environnement (IGE), l’ingénieur-e en
visualisation, traitement et analyse de données recruté-e aura pour charge deux missions à
quotité de temps équivalente :

**Data Manager du projet Européen H2020 PROTECT** : il/elle organisera l&#39;ensemble des
informations et données scientifiques provenant des 26 partenaires du projet.

**Administrateur.rice de la salle de visualisation de l&#39;IGE** : il/elle développera des outils de
visualisation des données scientifiques, qu&#39;elles soient issues de modèles numériques ou
d&#39;images satellites, en coordination avec les chercheurs IGE, pour pouvoir les afficher sur le
mur d&#39;images de l&#39;institut.