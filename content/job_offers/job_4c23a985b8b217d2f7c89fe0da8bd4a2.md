Title: Simulations numériques directes pleinement résolues d’écoulements chargés en particules ellipsoïdales
Date: 2021-01-19 16:32
Slug: job_4c23a985b8b217d2f7c89fe0da8bd4a2
Category: job
Authors: Amine Chadil
Email: amine.chadil@cnrs.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: 
Job_Duration: 
Job_Website: 
Job_Employer: Université Gustave Eiffel
Expiration_Date: 2021-04-13
Attachment: job_4c23a985b8b217d2f7c89fe0da8bd4a2_attachment.pdf

Les écoulements chargés en particules sont utilisés dans beaucoup de processus industriels (Lits fluidisés dans des centrales électriques : capture de CO2, combustion d’hydrogène …). Dû à la disparité des échelles entre celle des particules et celle de l’application, les simulations numériques de ces process sont basées sur des équations de Navier-Stokes moyennées, avec des termes d&#39;échanges interfaciaux nécessitants des modèles de fermeture.

Le développement significatif de moyens de calcul massivement parallèles fait des simulations numériques, à la plus petite échelle, le meilleur moyen pour développer ces modèles. Ces simulations appelés ‘Particle-Resolved Direct Numerical Simulation’ (PR-DNS) sont capables de capturer tous les phénomènes ayant lieu à l’échelle des particules, et ce pour une large plage de valeurs des paramètres caractérisant l&#39;écoulement. Les résultats de ces PR-DNS fournissent les données nécessaires pour le développement des modèles de fermeture pour les termes d’échanges interfaciaux présents dans les équations macroscopiques.

Les particules rencontrées dans ces écoulements sont rarement parfaitement sphériques et cette anisotropie affecte non seulement la dynamique des particules, et ainsi les structures issues de leurs agrégations, mais aussi les vitesses et températures du fluide porteur du fait qu’elle modifie les coefficients de transfert de chaleur et de quantité de mouvement entre le fluide et les particules.

Un travail conséquent de recherche bibliographique et de développement a déjà été réalisé afin de doter le code maison RESPECT des fonctionnalités nécessaires à la réalisation de simulations (PR-DNS) d’écoulements chargés en particules ellipsoïdales. Ce stage fera suite à ce précédant travail en développant, dans le code RESPECT, un modèle de collision entre deux ellipsoïdes, et réaliser ensuite des simulations pleinement résolues d’écoulements chargés en particules ellipsoïdales de type lits fluidisés.

Ce stage permettra à l’étudiant.e retenu.e de renforcer ses connaissances en mécanique des fluides, méthodes numériques et en programmation, et de découvrir un domaine très stimulant de la recherche académique, dans des collaborations de haut niveau. De plus, il/elle aura accès à un outil de calcul très puissant (RESPECT) lui permettant de réaliser des PR-DNS d’écoulements complexes et s’assurer ainsi une expérience formatrice et enrichissante.