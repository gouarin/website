Title: Ingénieur.e Informatique Scientifique (Stockage et HPC)
Date: 2020-11-25 15:19
Slug: job_066f6eecc6f49e058dbc497e8add7081
Category: job
Authors: Philippe MARTINEZ
Email: philippe.martinez@synchrotron-soleil.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Saint Aubin
Job_Duration: 
Job_Website: https://www.synchrotron-soleil.fr/fr/emplois/ingenieure-informatique-scientifique-stockage-et-hpc
Job_Employer: Synchrotron SOLEIL
Expiration_Date: 2021-02-17
Attachment: 

Responsabilité du bon fonctionnement de l’infrastructure de calcul scientifique installée à SOLEIL (clusters HPC et logiciels associés tels que compilateurs, gestionnaires de batch, bibliothèques et logiciels scientifiques, etc.), et de celui de l’infrastructure de stockage et archivage des données scientifiques.
A terme, être également en charge des relations opérationnelles avec les fournisseurs/partenaires de SOLEIL des plateformes externes de calcul intensif et solutions externes de stockage, qu’il s’agisse de partenaires industriels ou des centres de calcul et/ou stockage nationaux.