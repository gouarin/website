Title: Development of a new and efficient adaptive phase field method: application to the prediction of patient-specific bone remodeling 
Date: 2021-04-20 08:57
Slug: job_92cb0ca8a0092caac2ca987a3587d9eb
Category: job
Authors: Luisa Silva
Email: Luisa.Rocha-Da-Silva@ec-nantes.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: École Centrale de Nantes
Job_Duration: 3 years
Job_Website: https://ici.ec-nantes.fr/english-version/join-us
Job_Employer: Institut de Calcul Intensif – École Centrale de Nantes
Expiration_Date: 2021-07-13
Attachment: job_92cb0ca8a0092caac2ca987a3587d9eb_attachment.pdf

In order to adapt to the external loadings and maintain its mechanical performance, bone undergoes a continuous renewal process called remodeling. Mechanical loading controls the bone mass and structure, where different cells are involved, but where mechanico-sensing cells called osteocytes have a primal role. The chemical signal induced by the osteocytes controls two competing cells: osteoclasts that lead to bone resorbtion and osteoblasts which deposit bone. The understanding of this mechano-biological system is of prime importance to understand how the bone microstructure evolves in the ostheoporosis context for instance. Various modeling and numerical approaches have been developed to tackle this scientific challenge in a patient-specific context, like voxel-based methods for instance. These methods can tackle both the complex geometry of the bone and its evolution. Unfortunately, they are not efficient from a computational point of view. To overcome efficiency issues, phase field models have been developed, based on a diffuse interface approach and have been used to compute the mechanical properties of trabecular bone structures.  With this approach,  the  interface  is  modeled  with  a  finite thickness,  and  the  computational  domain  is constituted of the bone and surrounding tissue. The smaller this thickness lengthscale, the closer the results are to a sharp interface solution. Mesh adaptation is necessary to resolve the fields at this lengthscale at a reasonable cost, but work done until now provides results in very small domains.  The purpose of this PhD to develop numerical strategies that improve (even more) the efficiency of phase field methods for such applications, while keeping the specific-patient context. For that, several steps are foreseen within this work:

1. implement aphase field finite element solver
2. reduce  the  computational  cost  by  developing  automatic  adaptive  strategy  techniques, coupled to parallel computing
3. integrate the developments in a patient-specific context.
4. application to trabecular bone remodelling

