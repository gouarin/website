Title: Heuristiques pour la mémoire hétérogène
Date: 2020-10-23 10:43
Slug: job_f842bee52bab348cc9335695aa9e717e
Category: job
Authors: Brice Goglin
Email: Brice.Goglin@inria.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Talence
Job_Duration: 2 ans
Job_Website: https://jobs.inria.fr/public/classic/fr/offres/2020-03093
Job_Employer: Inria Bordeaux - Sud-Ouest
Expiration_Date: 2020-11-27
Attachment: job_f842bee52bab348cc9335695aa9e717e_attachment.pdf

L&#39;équipe TADaaM d&#39;Inria Bordeaux Sud-Ouest recrute un postdoctorant pour 2 ans pour travailler sur le placement des tâches et données sur architectures HPC à mémoire hétérogène. C&#39;est un poste financé par un projet ANR-DFG avec RWTH Aachen. 