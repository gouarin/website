Title: Configuration and control of computing precision, application to low energy gamma radiation measurements
Date: 2021-05-12 13:05
Slug: job_7d83313223cfc91b562ac1ee8340c3b0
Category: job
Authors: Fabienne Jézéquel
Email: Fabienne.Jezequel@lip6.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: IJCLab, Orsay &amp; LIP6, Paris
Job_Duration: 3 ans
Job_Website: https://www-pequan.lip6.fr/~jezequel/OFFERS/PhD_accuracy_gamma_radiation.pdf
Job_Employer: CNRS
Expiration_Date: 2021-09-30
Attachment: 

This PhD is part of a collaboration between IJCLab (Orsay, France) and LIP6 (Sorbonne
University, Paris, France), and linked to a European collaboration for the measurement of high
energy electromagnetic radiation, AGATA. It is funded thanks to the financial support from
CNRS through the MITI interdisciplinary program.
The subject is detailed in 
<https://www-pequan.lip6.fr/~jezequel/OFFERS/PhD_accuracy_gamma_radiation.pdf>
