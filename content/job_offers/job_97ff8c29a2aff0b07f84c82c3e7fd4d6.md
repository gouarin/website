Title: Analyse de la dynamique d’évolution des enchevêtrements dans un matériau polymère à partir de graphes
Date: 2020-11-16 08:44
Slug: job_97ff8c29a2aff0b07f84c82c3e7fd4d6
Category: job
Authors: Sébastien Garruchet
Email: sebastien.garruchet@michelin.com
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Clermont-Ferrand
Job_Duration: 6 mois
Job_Website: https://recrutement.michelin.fr/fr/stage-michelin
Job_Employer: Michelin
Expiration_Date: 2020-12-07
Attachment: job_97ff8c29a2aff0b07f84c82c3e7fd4d6_attachment.pdf

Le leadership de Michelin en matière d’innovation repose entre autre sur un niveau très élevé
de connaissance de la relation entre structure et propriétés des matériaux qui constituent le
pneumatique. Cette connaissance repose sur la maîtrise d’outils performants de mesures et de
modélisation ainsi que d’un travail collaboratif à la jonction entre la physique, la chimie et la
science des matériaux. Dans cette optique d’amélioration et de performance, vient s’insérer ce
stage sur l’analyse de la dynamique d’évolution des enchevêtrements dans un matériau
polymère à partir de graphes. En effet, un morceau de gomme constituant le pneumatique se
comporte comme des pelotes de ficelles entrelacées au hasard. C’est cet entrelacement qui
donne aux matériaux polymères leur caractéristiques mécaniques. Les contraintes
topologiques générées par cet entrelacement se nomment des enchevêtrements et forment un
réseau tridimensionnel évolutif. Bien connaitre, la topologie de notre matériau et son
évolution au cours du temps est donc primordial pour renforcer notre connaissance des
mécanismes intervenant dans le matériau. C’est cette compréhension fine qui nous permettra
de réaliser des rebonds de conception associés.

L’objectif du stage sera de modéliser comment ces enchevêtrements se forment et se défont
au cours du temps, afin de formaliser proprement le concept de la persistance d&#39;un
enchevêtrement. Le système d&#39;étude est disponible sous la forme simplifiée d&#39;une série
temporelle de graphes dont les sommets correspondent aux enchevêtrements. Le stagiaire aura
à sa disposition des outils numériques nécessaires à la bonne réalisation du stage.

Les principales missions de ce stage seront les suivantes :

- Etat de l’art
- Développement d’un protocole permettant, à partir de configurations de matrices
- polymères décrites sous forme de graphes, de déterminer l’évolution temporelle des
- enchevêtrements.
- Comparer les résultats aux théories de la physique des polymères.
- Mettre en œuvre des actions de communication relatives à l’outil développé auprès des
- équipes de recherche MICHELIN.
- Rédiger un rapport de synthèse à la fin du stage.