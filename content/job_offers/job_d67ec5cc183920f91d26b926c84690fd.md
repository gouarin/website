Title: Méthodes multi-échelles en temps pour la simulation numérique en fabrication additive
Date: 2020-11-08 14:29
Slug: job_d67ec5cc183920f91d26b926c84690fd
Category: job
Authors: Susanne Claus
Email: susanne.claus@onera.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Palaiseau
Job_Duration: 4-6 mois
Job_Website: https://w3.onera.fr/stages/sites/w3.onera.fr.stages/files/dtis-2021-22.pdf
Job_Employer: Onera
Expiration_Date: 2021-01-31
Attachment: job_d67ec5cc183920f91d26b926c84690fd_attachment.pdf

La fabrication additive est un domaine qui devient stratégique pour l&#39;industrie aéronautique et spatiale. Le couplage fort entre les effets thermiques et mécaniques durant le procédé rendent la mise au point de celui-ci très difficile et coûteux.

La simulation numérique des procédés de fabrication est un outil indispensable de prédiction et d&#39;optimisation. Dans le cas de la fabrication additive, celle-ci est rendue très complexes du fait des couplages forts entre thermique et mécanique et de la forte hétérogénéité des temps caractéristiques des phénomènes. De plus la durée globale de mise en œuvre des procédés peut être très élevée, conduisant à des coûts de calcul prohibitifs, du fait de la nécessité de petits pas de temps pour capter des phénomènes très localisés intervenant de manière fondamentale dans les qualités mécaniques de l&#39;objet produit.

Le but de ce stage est de commencer à travailler sur les méthodes d&#39;intégration temporelle à plusieurs échelles pour la simulation des seuls effets thermiques pour commencer. Avec deux objectifs: limiter le coût de la simulation en adaptant au mieux les pas de temps aux phénomènes locaux et permettre une parallélisation en temps.

Le stage comprendra une phase d&#39;étude bibliographique sur des schémas de type &#34;pararéel&#34; ou &#34;multi-grille&#34; en temps, suivie par une mise en œuvre sur des problèmes modèles en utilisant un code d&#39;éléments finis open-source.