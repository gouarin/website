Title: Ingénieur développement logiciel, modélisation climat Antarctique
Date: 2021-06-07 10:41
Slug: job_899b920d63ee840ba3d498e478cac06e
Category: job
Authors: Thomas Dubos
Email: dubos@lmd.polytechnique.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Palaiseau
Job_Duration: 1 à 3 ans
Job_Website: https://www.polytechnique.edu/fr/content/erc-synergy-climat-en-antarctique-et-role-de-lhippocampe-dans-le-cerveau
Job_Employer: École Polytechnique
Expiration_Date: 2021-06-30
Attachment: job_899b920d63ee840ba3d498e478cac06e_attachment.pdf

**Titre : Ingénieur développement logiciel, modélisation climat Antarctique.**
Laboratoire de Météorologie Dynamique / École Polytechnique / Palaiseau
Laboratoire des Sciences du Climat et de l’Environnement / CEA / Gif-sur-Yvette

**Contexte et objectifs**
Dans le cadre du projet ERC AWACA financé par l’Union Européenne, nous recherchons un ingénieur en développement logiciel pour soutenir nos activités innovantes en développement de code de calcul scientifique et en modélisation climatique.

Les précipitations neigeuses sur le continent Antarctique et la glace qui se détache de ses côtes contribueront de façon majeure mais encore incertaine aux futurs niveaux des mers. Le projet AWACA vise à mieux comprendre le cycle de l’eau Antarctique à partir de nouvelles observations in-situ et de modèles de climat innovants. Cette combinaison d’observations et de modélisation permettra de retracer le parcours et les états de l’eau dans l’atmosphère, les nuages et les précipitations, jusqu’à son arrivée en surface de la calotte. Ces avancées permettront de comprendre l’origine, la formation et la quantité de neige accumulée sur l’Antarctique et ses évolutions passées et futures.

L’ingénieur.e sera en charge de l’interfaçage de deux modèles : le solveur fluide massivement parallèle DYNAMICO (<https://gitlab.in2p3.fr/ipsl/projets/dynamico/dynamico>) et le modèle atmosphérique régional MAR spécialisé dans la représentation des climats polaires (<https://mar.cnrs.fr/>). Elle.Il sera responsable de la mise en œuvre de configurations de ces modèles focalisées sur la région Antarctique. 
Elle.Il sera recruté.e par l’École Polytechnique sur un CDD de 1 à 3 ans et conformément à la grille salariale de l’École, en fonction de ses qualifications et de son expérience. 

**Environnement de travail**
L’ingénieur.e travaillera principalement à l’École Polytechnique, au Laboratoire de Météorologie Dynamique, sous l’encadrement du Pr. Thomas Dubos, expert en modélisation numérique de l’atmosphère. Elle.il travaillera également au Laboratoire des Sciences du Climat et de l’Environnement tout proche (CEA, Gif-sur-Yvette), co-encadré.e par Cécile Agosta, experte en modélisation du climat Antarctique.
Outre sa bibliothèque et son restaurant, le campus de Polytechnique offre l’accès à de nombreux équipements sportifs.  
L’ingénieur.e bénéficiera d’un environnement de travail de premier plan au sein de l’Ecole Polytechnique : insertion dans l’équipe scientifique INTRO du LMD et dans un réseau d’ingénieurs en calcul scientifique au LMD, au LSCE et à l’IPSL, contact avec la recherche de pointe sur le climat menée au LMD et au LSCE, accès aux mésocentre ESPRI (IPSL) et aux supercalculateurs nationaux (GENCI).

Le projet AWACA est un projet collaboratif entre 4 scientifiques de renommée internationale, en France et en Suisse, qui assemblent leurs expertises pour comprendre et modéliser le cycle de l’eau Antarctique. Au-delà de son travail sur l’architecture et l’implémentation informatique de modèles de climat, l’ingénieur.e sera immergé.e dans l’équipe plus large du projet AWACA comprenant développement d’instruments innovants et campagnes de terrain en Antarctique.

**Missions**
Mise en oeuvre de configurations Antarctique de modèles atmosphériques :

- améliorer la modularité du code MAR
- définir et mettre en oeuvre une interface permettant l’utilisation de modules de MAR par DYNAMICO : gestions des entrées-sorties, mémoire
- construire et mettre à disposition de AWACA des configurations opérationnelles focalisées sur la région Antarctique

Ces actions se feront dans le cadre d’une démarche de qualité de code rigoureuse.

**Activités**

- Proposer, discuter, définir, documenter les choix techniques avec les équipes de développement MAR et DYNAMICO
- Contribuer aux codes MAR et DYNAMICO pour réaliser les objectifs
- Contribuer à la politique de gestion et de qualité des codes
- Participer à l’activité scientifique plus large d’AWACA

**Compétences essentielles**

- Connaissance et expérience des techniques et langages de programmation scientifique
- Compétences en architecture logicielle
- Compétences en gestion de code source (svn, git …) et qualité logicielle (gitlab-CI, …)
- Expérience du système d’exploitation Unix

**Compétences appréciées**

- Fortran
- Calcul haute performance, parallélisme MPI
- Architectures émergentes (GPU …)

**Contact**
Thomas Dubos <dubos@lmd.polytechnique.fr>
Cécile Agosta <cecile.agosta@lsce.ipsl.fr>



**Title : Scientific software Engineer, Antarctic climate modelling.**
Laboratoire de Météorologie Dynamique / École Polytechnique / Palaiseau France
Laboratoire des Sciences du Climat et de l’Environnement / CEA / Gif-sur-Yvette France

**Context and objectives**
The recently funded AWACA ERC project is opening a 1 to 3-year engineer position to support its innovative climate modelling activity, a cornerstone of the project. Snow falling on Antarctic and ice shedding from the Antarctic coasts are a major contribution to sea level and its potential future rise. The AWACA project aims to achieve major breakthroughs in the understanding of the Antarctic water cycle. AWACA will use novel in-situ observations and innovative climate models to link the atmospheric circulation to clouds, snow and their isotopic composition. These advances will help close the crucial open question of the origin, formation and amount of snow falling on Antarctica, and its past and future evolution.

The Engineer will be in charge of interfacing two models: the massively-parallel fluid solver DYNAMICO (<https://gitlab.in2p3.fr/ipsl/projets/dynamico/dynamico>) and the regional atmospheric model MAR, specialized in polar climate modelling (<https://mar.cnrs.fr/>). She/he will be responsible for setting up Antarctic configurations of these models.

She/he will be recruited by the École Polytechnique (Palaiseau, France) on a fixed-term contract of 1 to 3 years and in accordance with the salary scale of the employer depending on her qualifications and experience.

**Working environment**
The engineer will mainly work at the Laboratoire de Météorologie Dynamique (École Polytechnique) under the supervision of Prof. Thomas Dubos, an expert in numerical modeling of the atmosphere. She/He will also work at the nearby Laboratoire des Sciences du Climat et de l’Environnement (CEA, Gif-sur-Yvette), co-supervised by Cécile Agosta, an expert in Antarctic climate modeling.
In addition to its library and restaurant, the Polytechnique campus offers access to many sports facilities.
The engineer will benefit from a first-rate working environment within the Ecole Polytechnique: inclusion in the INTRO scientific team of the LMD, in a network of engineers in scientific computation at LMD, LSCE and IPSL, contact with world-class climate research conducted at LMD and LSCE, access to the ESPRI mesocentre (IPSL), to national supercomputers (GENCI).

The AWACA project is a collaborative project between 4 internationally renowned scientists, in France and Switzerland, who will combine their expertise to understand and model the Antarctic water cycle. Beyond his.her work on the architecture and the implementation of climate models, the engineer will immerse him.herself in the larger team of the AWACA project including the development of innovative instruments and field campaigns in Antarctica. 

**Missions**
Set-up of atmosphere model configurations over the Antarctic region:

- improve MAR code modularity
- define and set-up an interface allowing the use of MAR modules by DYNAMICO: management of inputs-outputs, memory.
- build and share operational configurations focus on the Antarctic region.

These actions will be carried out as part of a rigorous code quality approach.

**Activities**

- Suggest, discuss, define and document technical choices with the MAR and DYNAMICO development teams :
- Contribute to the MAR and DYNAMICO codes to achieve the missions
- Contribute to the code management and quality policy
- Participate in the wider scientific activity of AWACA

**Skills**

- Knowledge and experience of programming languages
- Software architecture skills
- Source code management (svn, git ...) and software quality (gitlab-CI, ...)
- experience in Unix operating system 

Valued:

- Fortran
- High performance computing, MPI parallelism
- Emerging architectures (GPU ...)

**Contact**
Thomas Dubos <dubos@lmd.polytechnique.fr>
Cécile Agosta <cecile.agosta@lsce.ipsl.fr>




