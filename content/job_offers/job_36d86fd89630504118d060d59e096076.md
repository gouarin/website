Title: Dynamic selective storage of hydrogen in a pressurized gas mixture in porous media
Date: 2021-03-03 18:06
Slug: job_36d86fd89630504118d060d59e096076
Category: job
Authors: Brahim Amaziane
Email: brahim.amaziane@univ-pau.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Pau
Job_Duration: 3 ans
Job_Website: 
Job_Employer: Université de Pau
Expiration_Date: 2021-05-30
Attachment: job_36d86fd89630504118d060d59e096076_attachment.pdf

The aim of this thesis is to contribute to a better understanding of the phenomena of diffusion and transport of pressurised gaseous mixtures containing hydrogen for the purpose of separation or storage of this compound in porous media. For this, a new experimental device will be developed for the dynamic study of permeability and selective separation of gaseous compounds under pressure on porous materials. It will also include a theoretical part dedicated to the modeling and numerical simulation of the flow of pressurized gas mixtures through porous media, taking into account the phenomenon of adsorption and possible swelling of the microporous matrix of the adsorbent.

Keywords : Dynamic adsorption, Diffusive transport, Adsorbent swelling, Numerical Simulation
