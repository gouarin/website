Title: Modélisation de l’érosion de cavitation par simulation couplée fluide-structure
Date: 2020-02-13 09:47
Slug: job_8ba8e20ce0c63b032e88cd2acbd2970f
Category: job
Authors: Eric Goncalves
Email: eric.goncalves@ensma.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Poitiers
Job_Duration: 3 ans
Job_Website: https://pprime.fr/
Job_Employer: Institut Pprime
Expiration_Date: 2020-05-07
Attachment: job_8ba8e20ce0c63b032e88cd2acbd2970f_attachment.pdf

Ce projet s’inscrit dans le contexte d’étude des effets néfastes du collapse violent de structures diphasiques sur le fonctionnement et la durée de vie d’éléments de systèmes propulsifs (hélices, injecteurs de carburant,...). Un cas modèle pour étudier ces collapses et les effets sur les matériaux est celui de l’interaction d’une onde de choc avec une bulle isolée (ou un réseau de bulles) placée à proximité d’une paroi solide.  De nombreuses études ont été menées pour décrire les phénomènes physiques sous-jacents (génération d&#39;ondes de choc de forte intensité), modéliser les pics de pression pariétale évaluer l&#39;endommagement du solide.  Cependant, à ce jour, rares sont les simulations tridimensionnelles suffisamment résolues ainsi que les simulations couplées fluide-structure nécessaires pour étudier l’endommagement du matériau. 
Dans ce contexte, on se propose de mettre en place des simulations numériques 3D de collapse de bulles avec couplage fort entre le fluide et la structure.

Mots clés: interaction fluide-structure, calcul haute performance (HPC), frontières immergées (IBM), ondes de choc, comportement élasto-plastique