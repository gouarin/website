Title: Ordonnancement sur architecture hétérogène en environnement distribué
Date: 2021-03-17 15:02
Slug: job_7f9650cacd4c9af382a50e987399a41c
Category: job
Authors: Bertrand MICHEL
Email: bertrand.michel@onera.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Chatillon (92)
Job_Duration: 3 ans
Job_Website: https://w3.onera.fr/formationparlarecherche/sites/w3.onera.fr.formationparlarecherche/files/mfe-daaa-2021-20.pdf
Job_Employer: ONERA
Expiration_Date: 2021-06-09
Attachment: job_7f9650cacd4c9af382a50e987399a41c_attachment.pdf

Au cours des 20 dernières années, l’utilisation d’outils de simulation numérique en mécanique des fluides est devenue incontournable pour le développement, le dimensionnement et même la maintenance des produits portés par les industriels du secteur aéronautique. Pour répondre à, ce besoin, l’ONERA a développé, en partenariat avec Airbus et Safran, une suite logicielle nommée elsA [1], largement utilisée au sein des bureaux d’études de ces deux grands acteurs.

L’apparition de nouvelles architectures matérielles, associant différentes technologies de processeurs (CPU, GPU ou encore VE(1)), la maturité de certaines technologies, notamment celle des outils de Différentiation Algorithmique pour l’optimisation et l’émergence de nouvelles approches en terme d’architecture logicielle (Framework IA) ont conduit l&#39;ONERA, en partenariat avec Safran, à initier le développement d’un solveur CFD de nouvelle génération, nommé SoNICS.

Principalement, l’architecture de SoNICS est basée sur la construction, dans un environnement parallèle distribué, d&#39;un graphe d’opérations élémentaires – des « opérateurs » – permettant décrire le problème à traiter. Ce choix, original en mécanique des fluides, de construction d&#39;un graphe d’opérateurs a été notamment motivé pour pouvoir naturellement inverser l&#39;ordonnancement du graphe pour un calcul adjoint utilisé pour l&#39;optimisation industriel.

La problématique que l’on souhaite aborder dans cette thèse est la distribution et l’exécution de ce graphe d’opérateurs sur une architecture hétérogène composée de CPU, GPU et/ou VE. Cette problématique est rendue assez complexe par le simple volume des données (c.à.d. les maillages) traitées et échangées par les opérateurs. Ainsi, répartir statiquement les calculs pour une utilisation optimale de toutes les capacités de calculs, en évitant au plus le vol de tâches, est une priorité. Un tel placement pourra être fait par un algorithme inspiré de l’ordonnanceur type HEFT de nouvelle génération [2], sur lequel il faudra étudier de possibles améliorations. Pour l’exécution du graphe, la priorité sera mise sur : la fusion de tâches afin d’optimiser les accès mémoires sur CPU; la concurrence et la communication asynchrone dans un environnement distribué où le recouvrement des échanges joue un rôle primordiale dans l’efficacité globale du solveur [3,4].

Il existe bon nombre de bibliothèques (StarPU, HPX, Kokkos, …) ou de directives de programmation (OpenMP, OmpSS, …) qui abordent cette thématique de graphe de tâches, son ordonnancement pour des architectures matérielles hétérogènes en mémoire partagée ou même en mémoire distribué [5]. Une bibliographie détaillée du potentiel de chacune devra être préalablement évaluée et devra permettre d’orienter et de dégager les choix en termes de modèle, d’algorithmes et d’implémentation pour le développement futur de SoNICS visant des calculs sur architecture matérielle hétérogène.

Au terme de cette thèse, le candidat devra être capable dans SoNICS de réaliser un calcul sur architecture matérielle hétérogène (CPU/GPU ou CPU/VE) dans un environnement hybride distribué/partagé sur un cas d’épreuve en turbomachine.

Les résultats feront l&#39;objet de publications dans des revues et des conférences scientifiques.

[1] L. Cambier, S. Heib, S. Plot. The ONERA elsA CFD software: input from research and feedback from industry ,Mech.Ind (2013)
[2] Thomas McSweeney, Neil Walton, Mawussi Zounon. An Efficient New Static Scheduling Heuristic for Accelerated Architectures, ICCS 2020.
[3] Qingyu Meng, Justin Luitjens, Martin Berzins, Dynamic Task Scheduling for the Uintah Framework, Proceedings of the 2010 TeraGrid Conference
[4] Qingyu Meng, Alan Humphrey, Martin Berzins, The Uintah framework: A unified heterogeneous task scheduling and runtime system, 2012 SC Companion: High Performance Computing, Networking Storage and Analysis
[5] Peter Thoman et al.· A taxonomy of task-based parallel programming technologies for high-performance computing, Parallel Processing and Applied Mathematics, 2018

(1) Vecteur Engine, la nouvelle génération des technologies développées par NEC au format carte PCI