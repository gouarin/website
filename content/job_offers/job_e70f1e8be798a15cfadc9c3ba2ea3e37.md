Title: Chercheur postdoctoral en modélisation et simulation de réseaux neuronaux (H/F)
Date: 2021-06-29 12:41
Slug: job_e70f1e8be798a15cfadc9c3ba2ea3e37
Category: job
Authors: Andrew Davison
Email: andrew.davison@cnrs.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Gif-sur-Yvette
Job_Duration: 18 mois
Job_Website: https://emploi.cnrs.fr/Offres/CDD/UMR9197-ANDDAV-007/Default.aspx
Job_Employer: CNRS
Expiration_Date: 2021-07-15
Attachment: 

Un poste en modélisation et simulation de réseaux neuronaux est disponible dans le groupe de recherche en Neuroinformatique de l&#39;Institut des Neurosciences Paris-Saclay, dans le cadre du Human Brain Project.

Le grand nombre d&#39;environnements de simulation différents dans le domaine des neurosciences constitue à la fois un problème et une opportunité. Comme chaque simulateur possède sa propre interface de programmation et son propre format de données de sortie, la reproduction, la réutilisation ou la combinaison de modèles écrits pour différents simulateurs présente généralement des difficultés considérables. D&#39;un autre côté, comme chaque environnement de simulation présente un équilibre différent entre l&#39;efficacité, la flexibilité, la scalabilité et la simplicité d&#39;utilisation, nous pouvons choisir celui qui convient le mieux à un problème donné ou confronter les résultats obtenus avec différents outils.

Les langages de représentation de modèles agnostiques tels que NeuroML et PyNN nous permettent de préserver les avantages de la diversité des simulateurs tout en réduisant ou en éliminant les inconvénients, facilitant ainsi la reproductibilité et la réutilisation. Ces langages nous permettent de prendre une seule description de modèle et de la simuler dans différents environnements sans avoir à réimplémenter le modèle à chaque fois.

L&#39;objectif de ce projet est d&#39;étendre le champ d&#39;application de PyNN des réseaux de modèles de neurones simplifiés, tels que le modèle &#34;integrate-and-fire&#34;, aux réseaux constitués de modèles de neurones détaillés sur le plan biophysique et morphologique, en ciblant les simulateurs NEURON et Arbor, les systèmes informatiques neuromorphiques SpiNNaker et BrainScaleS, et l&#39;interopérabilité avec NeuroML et le format SONATA.

<https://emploi.cnrs.fr/Offres/CDD/UMR9197-ANDDAV-007/Default.aspx>
