Title: Ingénieur DevOps (H/F)
Date: 2021-06-07 20:09
Slug: job_c464668f7390b0c7ea33cabbd4d5db9b
Category: job
Authors: Wilfried Kirschenmann
Email: wkirschenmann@aneo.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Boulogne-Billancourt
Job_Duration: 
Job_Website: http://www.aneo.eu
Job_Employer: ANEO
Expiration_Date: 2021-08-30
Attachment: 

Notre communauté Advanced Computing Technologies (ACT) composée en partie d&#39;experts en Intelligence Artificielle, Big Data, Cloud et Calcul Haute Performance souhaite s&#39;étoffer d&#39;autre(s) ingénieur(s) en DevOps H/F \!

### Missions &amp; activités

En tant que consultant(e) au sein d’ANEO, vous participerez, pour le compte de nos clients, à des projets qui seront pour la plupart basés à Paris ou en région parisienne. Cependant, nous sommes régulièrement sollicités sur d’autres parties du territoire français qui peuvent nécessiter des déplacements.

Vos activités consisteront par exemple à :
 
 * Mettre en place des chaînes de déploiement et de configuration d&#39;Infrastructure as a Code
 * Mettre en coeuvre des infrastructure multi-environnement (cloud interne, externes) basées sur des environnements Kubernetes
 * Mettre en oeuvre des pratiques de résilience des infrastructure (approches de type chaos monkey)

### Profil
Notre communauté (ACT) recherche donc une personnalité en adéquation avec son mode de vie : un profil curieux, ouvert, bon communicant, soucieux de faire grandir les plus jeunes et d’apprendre des plus expérimentés ; une personnalité qui a des compétences parmi les suivantes :

* Terraform ;
* Kubernetes ;
* Cloud (idéalement, une certification d&#39;architecte chez un des 3 grands fournisseurs américains) ;
* Une connaissance des enjeux du HPC et du HTC ;
* Aptitude à travailler en équipe avec des interlocuteurs d&#39;horizons et de niveaux différents ;
* Publication de travaux de recherche ;
* Anglais.

La mise en œuvre de certaines de ces compétences dans le cadre d’une thèse sera un plus.

Infos pratiques
Démarrage souhaité : Dès que possible et jusqu’en septembre ou octobre ; pour les doctorants en fin de thèse, nous pouvons anticiper des recrutement jusque janvier 2022.

Remunération selon profil
