Title: Ingénieur⸱e de recherche en calcul intensif et optimisation de code
Date: 2021-06-25 11:21
Slug: job_eafc7f56cb5a015dba49d301bf617625
Category: job
Authors: Loic Gouarin
Email: loic.gouarin@polytechnique.edu
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Ecole polytechnique / CMAP
Job_Duration: 18 mois
Job_Website: http://initiative-hpc-maths.gitlab.labos.polytechnique.fr/site/
Job_Employer: Ecole polytechnique
Expiration_Date: 2021-10-01
Attachment: job_eafc7f56cb5a015dba49d301bf617625_attachment.pdf

**Missions**
L’ingénieur⸱e de Recherche apportera son expertise sur l’optimisation de méthodes numériques et leur programmation parallèle sur différentes architectures faisant intervenir différents paradigmes. Il/Elle mènera des campagnes de benchmark en local et sur les centres de calcul nationaux afin de valider les choix opérés et de rendre visible les performances observées. Enfin, les travaux réalisés devront s’intégrer dans un logique de partage et de transmission en développant des logiciels Open Source et en les diffusant largement au sein de la communauté des mathématiques et leurs interactions.

**Contexte**
L’ingénieur⸱e de Recherche sera recruté⸱e dans le cadre de l’initiative HPC@Maths.
L’initiative HPC@Maths a pour but de développer au sein du Centre de Mathématiques Appliquées une compétence en Calcul Scientifique et Calcul Haute Performance (HPC) articulée avec les axes d’excellence du laboratoire en mathématiques. 
Cette compétence permet de créer un pôle visible dans le domaine de la modélisation mathématique et numérique, couplé au calcul scientifique et intensif, point d’interaction forte et de transfert efficace vers les entreprises et en particulier les PME et ETI. 
La stratégie du projet repose sur la construction d’un cercle vertueux : Recherche - Formation (élèves) - Partenariats et s’appuie sur le développement innovant d’algorithmes mathématiques de dernière génération pour le calcul et le HPC.
 
L’ingénieur⸱e de Recherche participera à l’émergence et à l’amélioration de logiciels Open Source mettant en avant les recherches sur de nouveaux algorithmes et structures de données (JosiePy, pylbm, samurai, ...) en s’intégrant à une équipe d’Ingénieurs de Recherche ayant une grande expertise en calcul scientifique et intensif. Il/Elle sera source de propositions pour l’amélioration des performances des algorithmes développés ainsi que leur passage à l’échelle sur des architectures hétérogènes. Il/Elle participera à la rédaction de demandes d’heures de calcul au sein des grands centres nationaux ainsi qu’aux rapports scientifiques associés.
 
**Candidature**
Pour candidater, merci d’envoyer un CV accompagné d’une lettre de motivation par email à:

* loic.gouarin@polytechnique.edu
* marc.massot@polytechnique.edu
* laurent.series@polytechnique.edu

L’expérience professionnelle sera prise en compte dans le niveau de rémunération.
