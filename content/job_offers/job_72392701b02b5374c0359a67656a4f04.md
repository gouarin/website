Title: HPC applications performance &amp; Benchmark specialist (H/F)
Date: 2020-02-13 15:59
Slug: job_72392701b02b5374c0359a67656a4f04
Category: job
Authors: MORTIER
Email: sylvain.mortier@atos.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: France or Europe
Job_Duration: 
Job_Website: https://career5.successfactors.eu/sfcareer/jobreqcareer?jobId=219131&amp;company=Atos&amp;username=
Job_Employer: Atos
Expiration_Date: 2020-12-31
Attachment: 

La Division Big Data &amp; Security d&#39;**Atos** délivre des solutions de **Calcul Haute Performance (High Performance Computing – HPC)** parmi les plus performantes au monde et leaders en Europe pour résoudre les problèmes scientifiques les plus complexes d’aujourd’hui et de demain.

Afin d&#39;accompagner notre développement nous recrutons un **Ingénieur Applicatif HPC.**

Vous travaillerez au sein de l’équipe **Applications et Performances** dont la principale mission est de répondre aux appels d’offre HPC en étant garant de la **performance des applications scientifiques de nos clients sur nos supercalculateurs.**
 
**Vos missions :**

* Recherche de l’environnement de compilation optimal d’applications scientifiques
* Recherche du meilleur environnement d’exécution (placement des processus de calcul, tuning de linux, tuning des bibliothèques parallèles, etc.)
* Tuning d’application scientifiques
* Recherche de l’architecture des supercalculateurs la plus adapté aux besoins de nos clients
* Extrapolation des performances applicatives sur des technologies futures
* Rédaction des rapports de performances
* Présentation de nos engagements et défense de notre offre devant nos clients
* Démonstration de la véracité de nos engagements.
 
Le poste que nous vous proposons vous offrira un parcours diversifié, sur des projets uniques et passionnants.

Vous serez au contact de nos clients et entouré d’une équipe qui saura vous accompagner.
 
**Votre profil :**

 De formation supérieure niveau Bac +5, école d’ingénieur ou cycle universitaire équivalent, vous disposez d’une expérience significative  dans le domaine du HPC.

Vous maitrisez les environnements/technologies suivants :

* environnements HPC et Linux : batch scheduler, systèmes de fichiers parallèles, réseaux d’interconnection haute performance 
* langages de programmation C/C++ et Fortran,
* paradigmes de parallélisation MPI, OpenMP et autres,
* outils de profiling et d’analyse de performance : gprof, map …
Anglais opérationnel
 
La maitrise des langages Cuda, OpenAcc seraient un plus apprécié ainsi que la connaissance des méthodes d’optimisation de code afin de les adapter aux architectures de nos supercalculateurs (vectorisation, scalabilité, cache blocking, …)

Le métier du benchmark requiert une forte résilience et une remise en question de ses certitudes.

Vous faites preuve d’une forte curiosité et d’un esprit d’équipe affirmé tout en restant autonome.

De nature pugnace vous savez résoudre des problèmes d’optimisation complexes.

Organisé et rigoureux vous excellez dans la gestion du temps et des priorités.