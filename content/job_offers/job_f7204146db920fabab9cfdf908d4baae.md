Title: Adaptation de maillage anisotrope sur la sphère pour la modélisation de risques côtiers
Date: 2020-11-25 14:35
Slug: job_f7204146db920fabab9cfdf908d4baae
Category: job
Authors: Nicolas Barral
Email: nicolas.barral@inria.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Inria Bordeaux
Job_Duration: 6 mois
Job_Website: https://team.inria.fr/cardamom
Job_Employer: Inria / Bordeaux INP
Expiration_Date: 2021-02-17
Attachment: job_f7204146db920fabab9cfdf908d4baae_attachment.pdf

Nous recherchons un(e) étudiant(e) pour un stage visant à étendre des méthodes d&#39;adaptation de maillage sur la sphère, dans un contexte de modélisation des risques côtiers. Les méthodes d&#39;adaptation anisotrope de maillage visent à optimiser la taille et l&#39;orientation des éléments d&#39;un maillage pour améliorer la précision du calcul tout en réduisant son coût. Dans le cadre de la modélisation des risques côtiers, les équations, de type Saint Venant, peuvent être résolues sur des maillages sphériques, représentant la courbure de la Terre. L&#39;objectif du stage est d&#39;introduire des méthodes adaptatives dans un modèle océanique sur la sphère.

Début souhaité: Février/mars 2019
Lieu: Inria Bordeaux, hébergé dans l&#39;équipe Cardamom
Des compétences en calcul scientifique et programmation en C/C++ seront un plus

Un sujet détaillé est joint à l&#39;offre, ou disponible ici:
<https://assets.jobteaser.com/upload_file/uploads/stage_sphereAero.641310036.pdf>