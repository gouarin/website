Title: 2 administrateurs système Linux et 1 développeur d&#39;applications
Date: 2020-10-08 13:47
Slug: job_56ef4117bfe2ceeb5a5fb5d09e6f9888
Category: job
Authors: Olivier Rouchon
Email: olivier.rouchon@cines.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Montpellier
Job_Duration: 2 ans
Job_Website: https://www.cines.fr/actualites/recrutement-et-stages/
Job_Employer: Centre Informatique National de l&#39;Enseignement Supérieur
Expiration_Date: 2020-12-31
Attachment: 

Le CINES recrute trois ingénieurs d&#39;étude contractuels pour contribuer aux projets Européens dans lesquels le Centre est impliqué : deux administrateurs système Linux avec des compétences en stockage de données réparties (iRODS, projet EOSC-pillar) pour l&#39;un, et en cloud computing et conteneurs (Docker/Kuberbetes, projet Phidias) pour l&#39;autre ; un développeur Java Front-end (Angular, programme VITAM-UI).