Title: Deep learning solar irradiance forecast from short-term to long-term.
Date: 2020-12-07 15:12
Slug: job_937324d4efb0a27f90df36d17fdc4ef0
Category: job
Authors: Boilley Alexandre
Email: alexandre.boilley@transvalor.com
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 6 months
Job_Website: http://www.soda-pro.com
Job_Employer: Transvalor
Expiration_Date: 2021-03-01
Attachment: job_937324d4efb0a27f90df36d17fdc4ef0_attachment.pdf

There is a growing need in solar and power forecasting to maintain the power grid in good operation.  Particularly for new plants submitted to energy market energy selling constraint as well as for solar plants in isolated area (island for example) where storage is mandatory. Since solar irradiance is the key input to estimate the power output of solar systems it is important to focus on forecasting solar irradiance.
Several methods are already in operation on SoDa server: simple method for short term forecast or machine learning methods for medium range forecast and NWP for long range forecast. Methods using image processing (motion flow) and deep learning methods are on the shelf waiting for you\!
