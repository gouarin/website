Title: Expert statisticien pour des études dans le domaine de l&#39;énergie éolienne
Date: 2020-11-06 13:43
Slug: job_bf020fad1216d6b3a3dbf3e2d53eeee9
Category: job
Authors: Julie Capo
Email: julie.capo@meteo.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Toulouse
Job_Duration: 2 ans
Job_Website: 
Job_Employer: Météo-France
Expiration_Date: 2020-11-18
Attachment: 

Le but principal de ce recrutement est de pouvoir disposer d&#39;outil permettant de générer une série de données de vent de 20 ans afin d&#39;étudier le potentiel éolien du site de mesure de manière significative, à partir d&#39;une année de données observées.

Le profil recherché devra disposer d&#39;une expertise assez élevée sur l&#39;analyse de données (ici périmètre météorologique) et de la modélisation statistique.

Niveau I : Bac + 5 Master, diplôme d&#39;ingénieur ou diplôme équivalent

Pour de plus amples informations, vous trouverez l&#39;annonce sur le potail de la Fonction Publique, Place de l&#39;Emploi Public (PEP) :
<https://www.place-emploi-public.gouv.fr/offre-emploi/expert-statisticien-pour-des-etudes-dans-le-domaine-de-l-energie-eolienne-reference-2020-476187>
