Title: Administrateur systéme - HPC
Date: 2020-03-16 11:32
Slug: job_82b6ef3dda2a6945492fdc8ec0a8c8ab
Category: job
Authors: Laurent Philippe
Email: laurent.philippe@univ-fcomte.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Besançon
Job_Duration: 
Job_Website: http://meso.univ-fcomte.fr
Job_Employer: Université de Franche-Comté
Expiration_Date: 2020-06-08
Attachment: job_82b6ef3dda2a6945492fdc8ec0a8c8ab_attachment.pdf

Le mésocentre de calcul de Franche-Comté recrute un ingénieur d&#39;étude pour participer à l&#39;administration de ses moyens de calcul. Le centre de calcul fournit les moyens de calcul haute performance nécessaires aux établissements d&#39;enseignement supérieur et de recherche franc-comtois, principalement à destination des chercheurs. L&#39;équipe informatique administre les moyens de calculs (plus de 3000 cœurs) et assiste les chercheurs dans le développement et l&#39;optimisation des codes s’exécutant sur ces moyens. 