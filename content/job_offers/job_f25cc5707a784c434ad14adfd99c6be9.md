Title: Modélisation numérique de sols meubles par la méthode Smoothed Particle Hydrodynamics
Date: 2020-10-27 16:37
Slug: job_f25cc5707a784c434ad14adfd99c6be9
Category: job
Authors: Sonia Portier
Email: emploi@nextflow-software.com
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Nantes, France
Job_Duration: 3 ans
Job_Website: https://www.nextflow-software.com/
Job_Employer: Nextflow Software
Expiration_Date: 2021-01-19
Attachment: 

**Contexte** 

La modélisation des grandes déformations du sol meuble et son interaction avec différentes structures/outils est au coeur de nombreuses problématiques : dans le domaine de l’agriculture avec les machines agricoles, dans le domaine du génie civil avec les machines d’excavation, dans le domaine de la géologie avec l’érosion des côtes et des berges ou les glissements de terrain. 
Ces différents domaines sont fortement sollicités compte tenu du réchauffement climatique et des phénomènes naturels qui en découlent tels que l’augmentation de l’occurrence d’évènements climatiques extrêmes, la montée des eaux, le réchauffement des sols. Les outils de simulation numérique disponibles actuellement demeurent trop imprécis pour prendre en compte les grandes déformations des sols meubles. 
Les industriels de ces secteurs ont ainsi majoritairement recours à l’expérimentation, ce qui engendre des coûts et délais importants. Dans l&#39;hypothèse où le sol est considéré comme un milieu continu, il existe un certain nombre de méthodes pouvant modéliser ses déformations. Les méthodes maillées classiques présentent des limitations dans la modélisation des grandes déformations et des fractures, limitations inhérentes aux erreurs générées par une trop grande distorsion du maillage. Les avancées récentes des méthodes particulaires basées sur la mécanique des milieux continus permettent aujourd’hui d&#39;envisager la modélisation de grandes déformations de terrain en utilisant des modèles constitutifs de sols conventionnels. 
Parmi ces méthodes, on peut citer la méthode Smoothed Particle Hydrodynamics (SPH), pour laquelle les dernières avancées techniques réalisées par la communauté académique sont particulièrement prometteuses. Cette méthode présente ici l’avantage d’être conservative, Lagrangienne et sans maillage.

Les avancées réalisées au cours de la thèse permettront d’entreprendre le développement d’un logiciel en rupture technologique avec les outils actuels de simulation. Cette méthode numérique innovante permettra de simuler le comportement du sol et, en outre, de prendre en compte ses interactions couplées avec des structures déformables.

**Travail proposé**

Le travail proposé peut se partager en plusieurs axes :

- Bibliographie, prise en main des codes de calcul et de l’algorithme de couplage actuel,compréhension des attentes académiques et industrielles, participation au recensement d&#39;unpanel de sols et à leur description. Constitution d&#39;une base de données physiques de solspour essais en laboratoire (essais réalisés hors thèse)

- Etude de lois de comportement permettant de représenter la physique des sols sélectionnés.Ces lois de comportement seront issues de la mécanique des milieux continus (mais pas nécessairement transposées dans le formalisme SPH)

- Amélioration du modèle de structure élasto-plastique présent dans le code SPH-flow (logicielbasé sur la méthode SPH, développé conjointement par Centrale Nantes et NextflowSoftware depuis une quinzaine d’années). Travaux relatifs à une implicitation partielle duschéma afin de maximiser la taille des pas de temps. Validation du modèle élasto-plastiquesur des cas tests académiques

- Discrétisation SPH des lois de comportement proposées

- Evaluation du modèle sur des cas simplifiés tels que l’enfoncement ou le cisaillementd’indenteurs dans un sol meuble aux caractéristiques contrôlées, validations parcomparaisons avec les résultats obtenus sur un montage expérimental.