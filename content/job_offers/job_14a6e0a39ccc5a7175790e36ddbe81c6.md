Title: Postdoc in Deep Learning for metrology of nanostructures (defectivity, classification)
Date: 2021-03-03 14:19
Slug: job_14a6e0a39ccc5a7175790e36ddbe81c6
Category: job
Authors: MARTINEZ Sergio
Email: sergio.martinez@pollen-metrology.com
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Moirans (38430)
Job_Duration: 18 mois
Job_Website: https://pollen-metrology.com/
Job_Employer: UCA (Université Clermont Auvergne)
Expiration_Date: 2021-05-26
Attachment: job_14a6e0a39ccc5a7175790e36ddbe81c6_attachment.pdf

Metrology and Detection/Classification of defects.
Your main task will be to perform defects detection and classification on electronic microscope images. There are different challenges, for instance the large range and variability of defects makes their detection highly challenging since the detection requires to be particularly accurate, in order to achieve precision measurement and characterisation on these defects. Additionally the classes can be unbalanced since some key defects are rare and therefore less representative in the dataset. 
You will be in charge of proposing, prototyping and developing Deep learning algorithms mainly for detection, classification and analyzing metrological data of various types. You will use frameworks such as TensorFlow or PyTorch, and internal technologies that you will help to develop. You will publish your key results in semiconductor domain conferences.