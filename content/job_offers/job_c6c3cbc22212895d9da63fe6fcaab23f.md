Title: Postdoctoral position in parallel computational linear algebra
Date: 2020-01-14 20:41
Slug: job_c6c3cbc22212895d9da63fe6fcaab23f
Category: job
Authors: Alfredo Buttari
Email: alfredo.buttari@irit.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Toulouse
Job_Duration: 18 months
Job_Website: 
Job_Employer: CNRS-IRIT
Expiration_Date: 2020-04-07
Attachment: 

We are seeking candidates for a 18-months post-doctoral position at
the IRIT laboratory of Toulouse (France). The subject is related to
the improvement of methods for the solution of sparse linear systems
on large scale parallel supercomputers. Specifically, this post-doc
will investigate methods for reducing the complexity and improving the
scalability of multigrid solvers through the use of low-rank
approximation techniques in the coarse grid solver or in the
intermediate levels smoothers.

The successful candidate will work in the context of the European
EoCoE-II project (<https://www.eocoe.eu/>) whose objective is to
leverage the potential offered by the ever-growing computing
infrastructure to foster and accelerate the European transition to a
reliable low carbon energy supply using High Performance Computing.

Requirements: PhD in Computer Science or Mathematics, strong expertise
in sparse linear algebra and high performance parallel computing.

**Detailed information:** 
<http://bit.ly/2rlTMly>


**Contact:**
Alfredo Buttari ([alfredo.buttari@irit.fr](mailto:alfredo.buttari@irit.fr))
