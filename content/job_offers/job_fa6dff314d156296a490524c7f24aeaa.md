Title: Un(e) Chef de Projet informatique (H/F)
Date: 2019-11-26 10:02
Slug: job_fa6dff314d156296a490524c7f24aeaa
Category: job
Authors: LEGRAIN Mallory
Email: recrutement@icm-institute.org
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: PARIS 
Job_Duration: 
Job_Website: 
Job_Employer: Institut du cerveau et de la moelle épinière
Expiration_Date: 2020-02-18
Attachment: job_fa6dff314d156296a490524c7f24aeaa_attachment.pdf

Création de poste au cœur de la direction du système informatique \!

Vous êtes passionné par l’informatique et voulez participer à l’effort de recherche dans le domaine médicale des neurosciences. Vous avez autant de compétences techniques que méthodologiques. Vous aimez relever les défis \! Rejoignez notre équipe dynamique et passionnée pour développer un monde agile au service de l’informatique scientifique.

MISSIONS PRINCIPALES

-	Accompagner les équipes de recherche afin de leur faire bénéficier du catalogue de services existants ou de projets pour répondre à leurs besoins 
-	Assurer le pilotage de projets informatique de l’ICM, à savoir : organiser et planifier la réalisation du projet avec des méthodes agiles, depuis sa conception jusqu’à son achèvement, en s’appuyant sur des compétences internes ou externes,
-	Rédiger les spécifications détaillées du besoin et techniques du projet, construction du backlog,
-	Garantir de la meilleure adéquation périmètre/qualité/coût /délai,
-	Définir et valider les jalons et livrables, organisation des sprints,
-	Définir les besoins en termes de ressources humaines et de compétences techniques,
-	Suivre et informer de l’avancement des projets (dashboard, indicateurs…), organiser les démos,
-	Effectuer la recette des solutions,
-	Suivre la mise en production, le déploiement des solutions et l’organisation de la maintenance,
-	Accompagner les utilisateurs dans la mise en œuvre des solutions, former,
-	Évaluer et suivre les risques liés aux projets.

SAVOIR

-	Titulaire d’un niveau Bac +4/5 (Master) spécialisée en informatique, réseaux et télécommunications
-	Compétence acquise comme chef de projet informatique, Scrum master ou product owner
-	Connaissance du secteur bio-informatique serait un plus,
-	Anglais lu, écrit, parlé.

SAVOIR-FAIRE

-	Maîtrise des processus et méthodes de gestion de projet informatique (Agile, planning, budget, indicateurs) et des outils (gestion de projet, modélisation),
-	Bonne connaissance des principaux outils de développement et des solutions applicatives et des bases de données
-	Capacité à comprendre les impacts de l’architecture du système d’information sur le projet, bonne connaissance des architectures applicatives et des bases de données,
-	Compréhension de l’environnement et des activités de recherche scientifique, des besoins et des contraintes des utilisateurs
-	Connaissance dans le domaine de la sécurité et des contraintes légales (RGPD, données de santé) des applications informatiques
-	Connaissances techniques permettant de venir en appui aux développeurs 
-	Maîtrise de l’anglais nécessaire : échange avec les utilisateurs, rédactions des spécifications…

SAVOIR-ETRE

-	Capacité à travailler en équipe, capacités d’organisation, de planification et de gestion,
-	Bonnes qualités relationnelles et de communication afin d’assurer une collaboration efficace avec les parties prenantes,
-	Rigueur et autonomie pour gérer tous les aspects des projets
