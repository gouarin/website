Title: Ingénieur de recherche en IA pour la conception (mécanique) (CDI) H/F
Date: 2019-12-23 12:38
Slug: job_dd169af48ceb09bacb2eeac34afe6543
Category: job
Authors: Adoc Talent Management
Email: apply-43825786bf9001@adoc-tm.breezy-mail.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Magny-les-hameaux
Job_Duration: 
Job_Website: https://www.adoc-tm.com/?lang=en
Job_Employer: Adoc Talent Management
Expiration_Date: 2020-03-16
Attachment: 

Adoc Talent Management recherche un·e ingénieur·e de recherche en IA pour la conception mécanique pour son client, centre de recherche industriel d’un groupe Français présent au niveau international sur le secteur des technologies et équipements aéronautiques.

Poste

Dans le cadre d&#39;un projet pluridisciplinaire amont lancé par le centre de recherche sur l&#39;utilisation de l&#39;IA pour la conception et la simulation, vous serez intégré au sein d&#39;une équipe de spécialistes en charge de la recherche sur les modèles et les simulations les plus avancées.

Vous travaillerez sur le développement des outils issus de l’IA afin de proposer de nouvelles stratégies d’optimisation de simulations numériques et de représentativité des modèles.

En étroite collaboration avec le reste de l’équipe et du Groupe, vous interviendrez sur plusieurs challenges métiers et scientifiques, en particulier via l’étude des méthodes dites de multi-fidélité ou de « transfer learning ». Dans ce cadre vous définirez et conduirez les actions de recherche et de développement amont des outils et des méthodologies pour l&#39;accélération des calculs numériques coûteux, et mènerez le développement de simulations et modélisations mécaniques, à partir des données expérimentales issues des matériaux fournies par le Groupe.

Vous étudierez les besoins des ingénieurs de recherche en mécanique, puis les déclinerez au niveau recherche scientifique et technique.

Pour mener à bien ces développements, vous réaliserez une veille scientifique et technique et participerez au réseau interne d&#39;échanges scientifiques autour de l&#39;IA.

Enfin vous communiquerez sur les travaux de recherche auprès de tous les pôles du centre de recherche, et les valoriserez à travers des publications scientifiques et la participation à des congrès et conférences internationales.

Profil

Doté(e) d’une forte expertise en mathématiques et en mécanique du solide, vous êtes titulaire d’un diplôme d’ingénieur ou de doctorat, et avez pu travailler environ 3 ans avec des outils IA et mathématiques autour de structures mécaniques (machine learning, modélisation statistique autour de structures mécaniques, simulation…).

Vous avez une réelle appétence pour le développement de code et maitrisez Python ou R.

Vous avez une connaissance en analyse numérique ainsi qu’en réduction de modèle, et êtes passionné par la recherche.

A travers la gestion de vos projets, vous avez pu développer une bonne capacité de communication ainsi qu’une grande autonomie, tout en ayant un fort esprit d’échange et de partage.

Si vous souhaitez vous investir au sein d’une structure Française ayant su faire sa place au niveau international, sur un poste vous permettant autonomie et forte interactions humaines, envoyez-nous votre candidature \!