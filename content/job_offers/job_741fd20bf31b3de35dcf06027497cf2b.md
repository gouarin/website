Title: Research engineer in marine ecosystem modeling
Date: 2020-06-18 12:47
Slug: job_741fd20bf31b3de35dcf06027497cf2b
Category: job
Authors: Olivier Titaud
Email: otitaud@groupcls.com
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Toulouse
Job_Duration: 18 mois
Job_Website: https://www.cls.fr/
Job_Employer: CLS
Expiration_Date: 2020-10-10
Attachment: job_741fd20bf31b3de35dcf06027497cf2b_attachment.pdf

18 months Reaserch Engineer position at CLS, Toulouse, France, to work on the ecosystem dynamics model SEAPODYM.

The ecosystem dynamics model SEAPODYM is the numerical model simulating the dynamics of low and mid-trophic level components of marine trophic chain and spatiotemporal population dynamics of exploited fish species driven by physical and biogeochemical oceanic variables. The underlying continuous model in SEAPODYM is the system of advection-diffusion-reaction equations with initial and boundary conditions. The model is numerically solved with help of ADI method and the integral model code is written in C++. The model parameter estimation is based on maximum likelihood estimation method and analytical derivative (adjoint) code allowing robust and exact, subject to machine error only, computation of likelihood gradient. Under several research projects, the modeling team at CLS is now developing quantitative model applications that can be used in real-time, high-resolution operational runs and to produce long term projections to study the impact of climate change on exploited fish stocks.
Successful candidate will contribute to ongoing research projects devoted to marine species management, downscaling the low spatiotemporal resolution MLE model for bigeye tuna and applying the SEAPODYM model to Atlantic mackerel stocks. Duties will include the data analysis, sensitivity analysis and parameter estimation, exploring the sources of statistical and structural uncertainty of the model and performing model validation.

Job activities:

- Using and contributing to the development of existing numerical model.
- Preparing input data: oceanographic forcing and data to inform different dynamic processes and to estimate model parameters.
- Analyzing the model in relation to data: parameter sensitivity, likelihood profiling, identical twin experiments.
- Quantitative modelling: designing and running parameter estimation experiments, model error analysis and model validation.
- Writing the reports, participation in group meetings, seminars and conferences.

Qualifications:
Minimal:
PhD in Applied Mathematics, Mathematical Ecology or a related field with a background in numerical analysis and modelling.
Knowledge and understanding of partial differential equations, their analytical and numerical solutions.
Knowledge and practical experience in solving optimization problems.
Knowledge of programming language(s) applied to numerical problems.
Ability to write academic texts in English.
Good communications skills, and ability to work as part of a research group as well as independently.

Desirable:
Knowledge of Eulerian models describing animal movement and dispersal.
Knowledge of statistical models and methods for data integration.
Experience in data diagnostics, analysis and interpretation.
Familiarity with oceanography and fisheries
Programming experience in C++.

Technical:
Linux environment
R, Python or similar scripting language for manipulating large volumes of data

To apply:
Please send the application including cover letter, CV and a copy of one or two representative publications. The cover letter should mention the specific tasks you would like to work on based on your background, previous experience and research interests. Please state also your preferred starting date