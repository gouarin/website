Title: Ingénieur Dev/Ops pour l&#39;institut d&#39;Intelligence Artificielle ANITI (Toulouse)
Date: 2020-03-03 09:55
Slug: job_f2f0821c40ef5c2c21fa1f4b1913a75e
Category: job
Authors: Jacques Thomazeau
Email: candidature@univ-toulouse.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Toulouse
Job_Duration: 1 an
Job_Website: https://www.univ-toulouse.fr/universite/universite-recrute/uftmp-ingenieur-devops-pour-institut-aniti-hf-0
Job_Employer: Université Fédérale de Toulouse Midi-Pyrénées
Expiration_Date: 2020-03-31
Attachment: 

Bonjour à tous,

L&#39;Université Fédérale de Toulouse Midi-Pyrénées recrute, pour l&#39;Institut d&#39;Intelligence Artificielle ANITI, 1 ingénieur sur CDD (niveau IE ou IR suivant profil) pour accompagner la mise en oeuvre de plateformes de calcul et de support à l&#39;enseignement de l&#39;intelligence artificielle.
L&#39;annonce du poste à pourvoir est disponible sur le site de l&#39;université de Toulouse  :
<https://www.univ-toulouse.fr/universite/universite-recrute/uftmp-ingenieur-devops-pour-institut-aniti-hf-0>

Merci de diffuser cette offre autour de vous.  
Cordialement 