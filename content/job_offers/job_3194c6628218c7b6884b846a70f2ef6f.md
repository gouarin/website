Title: Closed-Loop Flow Control by Plasma Discharge and Machine Learning
Date: 2021-03-09 14:30
Slug: job_3194c6628218c7b6884b846a70f2ef6f
Category: job
Authors: Traoré Philippe
Email: philippe.traore@univ-poitiers.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Institut Pprime Poitiers
Job_Duration: 3 ans
Job_Website: 
Job_Employer: Université de Poitiers
Expiration_Date: 2021-06-01
Attachment: job_3194c6628218c7b6884b846a70f2ef6f_attachment.pdf

Key-words: Computational Fluid Dynamics, flow control, closed-loop, plasma actuators, Machine Learning, Neural Networks, Reinforcement Learning.
