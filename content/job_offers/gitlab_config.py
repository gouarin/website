import os
# Gitlab configuration
GITLAB_URL = "https://gitlab.math.unistra.fr"
GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
GITLAB_TARGET_ID = 309  # groupe-calcul/website
GITLAB_TARGET_BRANCH = os.getenv("OPENSHIFT_BUILD_REFERENCE", "develop")
GITLAB_SOURCE_ID = 309  # groupe-calcul/website
GITLAB_REF_BRANCH = GITLAB_TARGET_BRANCH # Head of the job branch
GITLAB_LABELS = ["Offre d'emploi"]
