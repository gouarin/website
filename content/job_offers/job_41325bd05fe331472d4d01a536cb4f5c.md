Title: Méthode des éléments virtuels pour la résolution des équations intégrales en furtivité électromagnétique
Date: 2020-10-13 07:24
Slug: job_41325bd05fe331472d4d01a536cb4f5c
Category: job
Authors: Emanuele Arcese 
Email: emanuele.arcese@cea.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Bordeaux
Job_Duration: 6 mois
Job_Website: 
Job_Employer: CEA-CESTA
Expiration_Date: 2021-01-05
Attachment: 

Dans le cadre de la furtivité radar, le CEA développe des codes de calcul simulant le comportement d’objets 3D complexes. L’un de ces codes utilise la méthode de résolution par équations intégrales pour les problèmes de diffraction d’ondes électromagnétiques et la couple à une méthode des éléments finis pour le traitement des éventuels matériaux non homogènes. L’approximation numérique de la partie en équation intégrale (resp. en éléments finis) repose sur des schémas traditionnels de type éléments finis de surface (resp. d’arêtes). Toutefois, avec les besoins accrus en simulation 3D, le manque de souplesse de ces approches dans la construction du maillage (non-conforme, peu régulier, etc.), dans le cadre de géométries complexes, limite rapidement les performances numériques du solveur : la conformité requise entre les mailles ainsi que leur niveau maximum de déformation locale exigé peuvent entrainer une augmentation significative de la taille du système linéaire global.
L’idée est d’entreprendre une première réflexion sur un nouveau schéma numérique intitulé &#34;méthode des éléments virtuels&#34;. Il permet de pallier la limitation issue de la rigidité des maillages afin de réduire la complexité des algorithmes. Cette approche récente permet de construire une méthode classique des éléments finis sur maillages «quelconques» (i.e. caractérisés par des éléments polygonaux et polyédriques généraux). Bien établi pour les problèmes elliptiques (et encore peu exploré pour les problèmes d’ondes), un tel schéma présente l’avantage de nécessiter moins de degrés de liberté tout en assurant une meilleure consistance qu’une approche discontinue, telle que la méthode de Galerkin discontinue. L’objectif est ainsi d&#39;évaluer le potentiel de cette méthode pour résoudre par équations intégrales les équations de Maxwell en 3D.
