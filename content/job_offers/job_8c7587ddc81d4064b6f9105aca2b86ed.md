Title: Ingenieur de Recherche en calcul scientifique
Date: 2019-10-14 19:08
Slug: job_8c7587ddc81d4064b6f9105aca2b86ed
Category: job
Authors: Oruba
Email: ludivine.oruba@sorbonne-universite.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: LATMOS, Site de Jussieu, PARIS
Job_Duration: 1 an
Job_Website: http://swag.latmos.ipsl.fr/
Job_Employer: Sorbonne Universite
Expiration_Date: 2020-02-28
Attachment: job_8c7587ddc81d4064b6f9105aca2b86ed_attachment.pdf

**Poste / Position: ** 

CDD de 12 mois (Niveau de rémunération Ingénieur d’Études), à pourvoir dès que possible 
12 months contract (salary based on level chart for Research Engineer), to be filled as soon as possible

**Laboratoire d&#39;accueil / Host laboratory:**

Laboratoire Atmosphères, Milieux, Observations Spatiales - LATMOS
Sorbonne Université, Campus Pierre et Marie Curie, 4 place Jussieu, 75005 PARIS

**Catégorie / Category:**  A    

**Corps:**  Ingénieur d&#39;études de recherche et formation
**BAP:** E – Informatique, Statistique et Calcul Scientifique
**Profil recherché / Required profile:**
Titulaire d’un niveau bac+5, ayant des connaissances en développement numérique
Bac+5 Level, with skills in scientific computing

**Compétences recherchées / Required skills: **

•	Maîtrise du langage de programmation Fortran 
•	Connaissances en développement numérique
•	Maîtrise d&#39;un langage de traitement de donnée (de préférence Python)
•	Expérience en calcul parallèle
	
Advanced working knowledge of the Fortran programming language   
Knowledge in numerical developments
Working knowledge of a data-processing language (preferably Python)
Experience in parallel computing

**Contexte et missions confiées / Context and aims: **

	La région de l’Atlantique tropical Est et de l’Afrique de l’Ouest joue un rôle clé dans le climat global. Elle est pourtant l’une des régions du monde où les prévisions météorologiques et climatiques sont les plus entachées d’erreurs. Cette région est en effet le siège de couplages complexes entre la surface océanique, la surface continentale et la circulation atmosphérique. Le projet SWAG, dans lequel s’inscrit ce poste, vise à mieux comprendre l’impact de ces différents couplages sur la circulation atmosphérique régionale et sur la cyclogenèse tropicale (http://swag.latmos.ipsl.fr/).   
La personne recrutée aura pour mission principale de porter à l’IDRIS le couplage entre le modèle régional couplé WRF (Weather Research Forecast) / NEMO (Nucleus for European Modelling of the Ocean), déjà développé au LEGOS en 2018 pour la région d’intérêt et mis à disposition du projet SWAG, et d’implémenter le package chimie de WRF-Chem. 
Le modèle développé au LEGOS est composé de WRF3.7 et de NEMO4.0, couplés par OASIS3_MCT, et s&#39;étend sur la zone 60W-20E / 35N-30S. La résolution horizontale est d&#39;environ 25 km pour l&#39;océan et l&#39;atmosphère, 35 niveaux verticaux pour l&#39;atmosphère et 70 pour l&#39;océan. Notons que les paramétrisations choisies pour le moment optimisent les biais dans les simulations couplées air-mer, mais l&#39;implémentation de WRF-Chem nécessitera de les revoir afin d&#39;accorder les paramétrisations physiques et chimiques. Le modèle ainsi développé permettra d’étudier comment les aérosols (d’origine naturelle, comme les sels de mer, les poussières désertiques, ou anthropiques) modifient la circulation atmosphérique et l’océan superficiel, en modifiant l’équilibre radiatif.
Les tâches qui seront confiées à la personne recrutée seront :
•	Installer et gérer les logiciels nécessaires pour l’installation de WRF-Chem et le couplage WRF-Chem/NEMO sur la machine Jean-Zay de l’IDRIS
•	Porter le modèle couplé WRF/NEMO à l’IDRIS, installer WRF-Chem sur la machine Jean-Zay de l’IDRIS et tester le couplage WRF-Chem/NEMO. 
•	Effectuer les étapes nécessaires de validation du couplage, notamment concernant le choix des paramétrisations physique et radiative, harmoniser les paramétrisations physiques dans WRF/NEMO et chimiques dans Chem et les valider grâce aux observations
•	En fonction des simulations « test » obtenues : installer un « zoom » (i.e. domaines de plus haute résolution implantés au sein des modèles WRF et NEMO) dans la région côtière au nord du golfe de Guinée
•	 (Tâche indépendante, spécifique à l’étude de la cyclogenèse tropicale et relevant du seul modèle d’atmosphère WRF) Préparer des fichiers d’initialisation pour le modèle d’atmosphère WRF, à partir des outputs de simulations numériques réalisées avec WRF par la NOAA (cas de cyclones tropicaux matures). 


The tropical East Atlantic and West Africa regions play a key role in the global climate. It is, however a part of the world where weather and climate forecasts are most uncertain. This region is indeed prone to complex couplings between the ocean surface, the continental surface and the atmospheric circulation. The SWAG project, to which this position is attached, aims at a better understanding of the impact of these different couplings on the regional atmospheric circulation and on tropical cyclogenesis (http://swag.latmos.ipsl.fr/). 
The person to be recruited will be in charge of implementing the coupling between the coupled regional model WRF (Weather Research Forecast) / NEMO (Nucleus for European Modeling of the Ocean), already developed at LEGOS in 2018 for the region of interest, and available to the SWAG project, as well as the chemistry package of WRF-Chem. Regarding the later, the work will consist in testing the physical and radiative parametrisations compatible with the chemistry package providing the most realistic results. The model developed at LEGOS is composed of WRF3.7 and NEMO4.0, coupled by OASIS3_MCT, and extends over the area 60W-20E / 35N-30S. The horizontal resolution is about 25 km for the ocean and atmosphere, 35 vertical levels for the atmosphere and 70 for the ocean. Note that the parameterizations currently chosen optimize the biases in air-sea coupled simulations, but the implementation of WRF-Chem will require a review to match the physical and chemical parameterizations. The resulting model will allow the study of aerosols (natural, such as sea- salt, Saharan dust, or anthropogenic) impact on the atmospheric circulation and the superficial ocean, by alteration of the radiative balance.
The tasks that will be entrusted to the recruited person will be:
• Install the software required for the installation of WRF-Chem and WRF-Chem/NEMO on the Jean-Zay machine of IDRIS.
• Install WRF/NEMO at Idris and compile the WRF-Chem on the  Jean-Zay machine of IDRIS,
• Set compatible physics and radiative parameterisations in WRF with chemical ones in WRF-Chem by comparing test simulations with observations for validation, 
• If needed, install a nested coupled zoom (i.e. small coupled domains of higher resolution embedded in WRF and NEMO) around the Guinean coastal region.
• (Independent task, related to the study of tropical cyclogenesis and relying on the WRF atmosphere model only) Prepare initialization files for the WRF atmosphere model, based on the outputs of numerical simulations carried out by the American National Oceanic and Atmospheric Administration (NOAA) using WRF (cases of mature tropical cyclones).

**Contact pour plus d’informations / Contact for more information: **

gaelle.decoetlogon@latmos.ipsl.fr
ludivine.oruba@latmos.ipsl.fr

**Candidature/Applications: **

Veuillez envoyer votre candidature par courrier électronique à gaelle.decoetlogon@latmos.ipsl.fr et ludivine.oruba@latmos.ipsl.fr, en joignant les documents suivants sous forme d’un unique fichier pdf : lettre de motivation (maximum 1 page), curriculum vitae (maximum 2 pages) et liste de publications. Indiquez également les coordonnées de deux personnes pouvant fournir une lettre de référence sur demande.

Please submit your application by email to gaelle.decoetlogon@latmos.ipsl.fr and ludivine.oruba@latmos.ipsl.fr, and attach the following documents as a single pdf file: motivation letter (max 1 page), CV (max 2 pages) and publication list. Please also include the contact information of two references who can provide a letter of support upon request.


