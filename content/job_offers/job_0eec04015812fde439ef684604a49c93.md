Title: Portabilité de performance des schémas LBM en contexte AMR
Date: 2020-09-03 13:48
Slug: job_0eec04015812fde439ef684604a49c93
Category: job
Authors: Alain Genty
Email: alain.genty@cea.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: CEA Saclay (91)
Job_Duration: 12 mois
Job_Website: 
Job_Employer: Commissariat à l&#39;Energie Atomique et aux Energies Alternatives
Expiration_Date: 2021-01-15
Attachment: job_0eec04015812fde439ef684604a49c93_attachment.pdf

Voir fichier joint.