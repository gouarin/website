Title: Développeur Django/Python pour des services cloud de calcul scientifique
Date: 2020-11-04 14:27
Slug: job_52d725e02b94f4a52b81c99d8c3334f0
Category: job
Authors: Christophe Blanchet; Olivier COLLIN; Valentin SAINT-LEGER
Email: contact@groupes.france-bioinformatique.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Lyon
Job_Duration: 12 mois
Job_Website: https://www.france-bioinformatique.fr/developpeur-django-python-pour-des-services-cloud-de-calcul-scientifique
Job_Employer: CNRS
Expiration_Date: 2020-11-12
Attachment: 

Vous avez une expérience forte en développement Django/Python, et vous souhaitez l&#39;appliquer à des technologies de dernière génération (cloud computing, machines virtuelles et conteneurs) pour fournir des services scientifiques innovants aux chercheurs et ingénieurs en science de la vie, rejoignez-nous. 

L&#39;Institut Français de Bioinformatique (IFB) [1] est l’infrastructure nationale de bioinformatique qui assure un support, déploie des services, organise des formations et réalise des développements innovants pour les communautés des sciences du vivant. Les services disponibles comprennent une infrastructure informatique de calcul scientifique (en mode cloud et cluster de calcul), des environnements logiciels, des bases de données, une assistance aux utilisateurs, du conseil pour la conception et la réalisation de projets scientifiques et des formations. L&#39;IFB est composé d&#39;une unité coordinatrice, IFB-Core, et de 30 plateformes régionales. 

Le cloud de l&#39;IFB permet aux chercheurs et ingénieurs en sciences de la vie de déployer des logiciels de calcul scientifique sous la forme de machines virtuelles et de conteneurs. Ils disposent pour cela du portail web Biosphère [2] pour déployer et gérer leurs environnements virtuels. Les différents environnements existants sont référencés dans le catalogue RAINBio [3]. Le cloud de l&#39;IFB comprend 6 000 coeurs de calcul répartis sur 8 sites [4].

Le portail web Biosphère est codé en Django/Python associé à une base de données PostgreSQL. Il est couplé avec des APIs Python ou RESTful aux systèmes Openstack, Keycloak et Slipstream. Il est déployé dans des conteneurs en environnement Docker. Le développement des codes sources est versionné dans un projet git et enregistré dans un portail gitlab.

Recruté(e) par l&#39;unité IFB-core (CNRS UMS3601) de l&#39;Institut Français de Bioinformatique (IFB), l&#39;ingénieur(e) sera placé(e) sous la responsabilité administrative de la directrice d&#39;unité et sous la responsabilité scientifique des responsables du cloud de l&#39;IFB. L&#39;unité est implantée administrativement à Evry mais l&#39;agent exercera ses missions à Lyon au sein de l&#39;équipe cloud de l&#39;IFB. Il/elle travaillera en lien avec les équipes de l&#39;IFB-core, des autres sites cloud et du centre de calcul lyonnais hébergeant le noeud ifb-core-cloud.

Vos missions seront :
- Assurer le développement et le déploiement des services centraux du cloud de l&#39;IFB (portail Biosphère, catalogue RAINBio...). 
- Contribuer au pilotage, au maintien en condition opérationnelle et à l&#39;exploitation des services cloud centraux.
- Contribuer à l&#39;assistance aux utilisateurs.

Postuler : <https://bit.ly/3m9desI>

Références :

[1] <https://www.france-bioinformatique.fr>  
[2] <https://biosphere.france-bioinformatique.fr>  
[3] <https://biosphere.france-bioinformatique.fr/catalogue>  
[4] <https://biosphere.france-bioinformatique.fr/cloud/system_status>