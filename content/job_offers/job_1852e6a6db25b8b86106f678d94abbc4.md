Title: Scientific Software Developer - Package Management at Scale
Date: 2021-04-03 11:58
Slug: job_1852e6a6db25b8b86106f678d94abbc4
Category: job
Authors: Sylvain Corlay
Email: sylvain.corlay@quantstack.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Remote
Job_Duration: 
Job_Website: https://quantstack.net
Job_Employer: QuantStack
Expiration_Date: 2021-06-26
Attachment: 

QuantStack is looking for a software developer to join our team of contributors to the jupyter and conda ecosystems. In this role, you will become a member of a vibrant open-source community, contribute to a high-impact project used by millions in the world, and grow to become a key maintainer of the ecosystem.

**About QuantStack**

QuantStack is an open-source development studio specializing in scientific computing. The team comprises core developers of major open-source projects, such as Jupyter, Conda-forge, Voilà, Xtensor, and many others. We strive to build high-impact software and grow communities.

**Your role at QuantStack**

You will support the community of users of the Conda and Mamba ecosystems, and help shape the future of software adopted by millions of engineers and scientists in the world. In this role, you will:

* Contribute to the development of the Quetz package server, and the JupyterHub project.
* Improve the Quetz open-source package server to enable large-scale public-facing deployments of the service.
* Contribute to the development of JupyterHub to enable large-scale high-availability deployments of Jupyter-based services.
* Contribute to the development of build pipelines for creating conda packages.

While prior experience with conda or Jupyter is not required, a track record of contributing to open-source software is a huge plus.

**Where we can hire**

Our engineering team is fully remote. The company is headquartered in Paris, France, and the majority of our team is Europe-based. We will consider applications in the European Union.
