#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Job offer submission form.

WARNING: attachment has multiple copies in memory thus leading to a non
    negligible memory footprint.

FOR DEVELOPERS:

# General workflow (see process_form function)
1) The form data are put into a FormData instance
2) Form sanity is checked and a FormError is filled with potential errors
3) If there is an error, the form is displayed with additional error message
4) Otherwise, a job offer is filled using the JobOffer class
5) A new pull-request is created with the create_job_request function

# How to add a form field
1) if it can raises an error (ie an invalid form), first add a property to
    FormError class (contains the error message) and had a condition in the
    has_error method.
2) add a corresponding property to FormData class (with default value) and
    add (if necessary) a sanity check in the check method. Also check if
    the field is mandatory.
3) update the web form template and/or the update_html_form function.
    Don't forget to escape Jinja script so that the pelican pass don't
    remove it. Add `required` attribute and a `*` if necessary.
4) describe how to fill the FormData from the input request in the process_*
    functions.
5) add a corresponding property in JobOffer class
6) include this property in the id calculation (_calc_id method of JobOffer).
7) add a corresponding line in the job offer file template
    (content/job_offers/job_offer.md.template) and add corresponding line when
    launching the rendering (_render method of JobOffer).
8) update the Pelican job offer template (job_offer.html).
9) add corresponding line in the interface processors
   (process_cmdline and process_cgi so far).

# How has SimpleMDE editor been customized ?
1) Firstly, please consider using another editor since it seems to be
    abandoned.
2) OK, as you want. Install SimpleMDE using the Node.js package manager :
    npm install simplemde --save
    (you may need to install dependencies, I don't remember...)
3) Fix some bugs that prevent "compilation" of SimpleMDE:
    * in src/css/simplemde.css, line 121-122 (in the .editor-toolbar a block),
        add a space before `!important` for `text-decoration` and `color`
        properties.
    * in src/js/simplemde.js, remove the backslash before `-` in each regular
        expression, ie replace `\-` by `-`. Should be the case for lines 75,
        174, 904, 905 and 1012.
4) Disable HTML in Markdown syntax: in src/js/simplemde.js, add after
    `var marked = require("marked");` line 15, the following line :
    marked.setOptions({sanitize: true});
5) "Compile" the package by running `gulp` (install it using npm) from the root
    of SimpleMDE.
6) Enjoy the unreadable files in dist subfolder.

# What about security ?
1) access to *.template files should be denied in the web server configuration
    (e.g. through a .htaccess file, like the one already there).
2) in case of security issue (e.g. this script has been displayed on client side),
    consider revoking and updating the GITLAB_TOKEN below.

# Common errors:
- if script fails with error
    "AttributeError: module 'magic' has no attribute 'from_buffer'"
    you may want to install python-magic module ;) (otherwise, import magic will
    load Magic-file-extensions).
"""

import datetime
import os
import re
import sys
import io
import base64
import itertools
import pprint
import mimetypes
import magic
import hashlib
import pickle
import uuid

###############################################################################
# General configuration

from gitlab_config import * # For Gitlab, see gitlab_config.py

TEMPLATE_PATH = './'
TEMPLATE_JOB_OFFER_FORM = 'job_offer_form.html.template'
TEMPLATE_JOB_OFFER = 'job_offer.md.template'

JOBOFFER_TYPE = {
    'cdi': 'CDI',
    'cdd': 'CDD',
    'postdoc': 'Post-doctorat',
    'these': 'Thèse',
    'stage': 'Stage',
    'concours': 'Concours',
}

PELICAN_JOB_OFFER_PATH = 'content/job_offers'
ATTACHMENT_MIME_TYPE = ['application/pdf']
JOBOFFER_EXPIRATION_DELAY = datetime.timedelta(weeks=3*4)

# The folder where files associated to each submission are created.
# Should be a folder that depends on the current website to avoid collisions.
FLOOD_PATH = "./flood"

# Global flood limits (for all submissions)
FLOOD_GLOBAL_TIMEOUT = datetime.timedelta(minutes=60)
FLOOD_GLOBAL_LIMIT = 10

# Local flood limits (for one filler)
FLOOD_LOCAL_TIMEOUT = datetime.timedelta(minutes=60)
FLOOD_LOCAL_LIMIT = 5

# Mail config in case of flood
MAIL_SMTP_SERVER = "172.16.101.1"
MAIL_SENDER = "calcul-owner@math.cnrs.fr"
MAIL_RECEIVERS = ["bureau.calcul@services.cnrs.fr"]


###############################################################################
class Debug(object):
    """ Debugging parameters. """

    def __init__(self, verbose=False, offline=False, local=False):
        self.verbose = verbose  # Explains all options
        self.offline = offline  # Do not connect to Gitlab
        self.local = local      # Print job offer files locally

###############################################################################
class FloodChecker():
    """ Anti-flood protection """

    class FloodStats():
        """ Local or global flood stats """
        def __init__(self):
            self.first = datetime.datetime.max
            self.last = datetime.datetime.min
            self.cnt = 0

        def add(self, dt):
            self.first = min(self.first, dt)
            self.last = max(self.last, dt)
            self.cnt += 1

        def __repr__(self):
            return "FloodStats(first={}, last={}, cnt={})".format(
                pprint.pformat(self.first),
                pprint.pformat(self.last),
                self.cnt
            )


    def __init__(self):
        self.dt_now = datetime.datetime.now() # To have a consistent current time

    def _read_files(self):
        """ Read submission files and clean outdated ones """

        self.global_stats = self.FloodStats()
        self.local_stats = dict()

        # Submission file name pattern: hexdigest followed by an uuid
        pattern = re.compile("(?P<datetime>[0-9-]+)_(?P<id>[a-z0-9]+)_[a-z0-9-]+")

        for entry in os.scandir(FLOOD_PATH):
            if entry.is_file():
                file_name_match = pattern.fullmatch(entry.name)
                if file_name_match:

                    try:
                        file_mtime = datetime.datetime.strptime(file_name_match.group('datetime'), "%Y-%m-%d-%H-%M-%S-%f")

                        # Clean if outdated file
                        if file_mtime < self.dt_now - max(FLOOD_GLOBAL_TIMEOUT, FLOOD_LOCAL_TIMEOUT):
                            try:
                                os.remove(entry.path)
                            except OSError:
                                pass # File may have been deleted by other script instance
                            continue

                        # Add to global stats
                        if file_mtime >= self.dt_now - FLOOD_GLOBAL_TIMEOUT:
                            self.global_stats.add(file_mtime)

                        # Add to local stats
                        if file_mtime >= self.dt_now - FLOOD_LOCAL_TIMEOUT:
                            self.local_stats.setdefault(file_name_match.group('id'), self.FloodStats()).add(file_mtime)

                    except OSError:
                        pass # File may have been deleted by other script instance

    def _client_id_hexdigest(self, client_id):
        """ Return the hexadecimal digest of the given client identification data """
        return hashlib.sha1(pickle.dumps(client_id)).hexdigest()

    def _submission_delay(self, stats, timeout, limit):
        """ Return delay before a form can be submitted (0 if no delay) """
        if stats.cnt <= limit:
            return datetime.timedelta()
        else:
            return max(datetime.timedelta(), timeout - (self.dt_now - stats.first))

    def _global_submission_delay(self):
        """ Return delay before a form can be submitted (0 if no delay) by anyone """
        return self._submission_delay(
            self.global_stats,
            FLOOD_GLOBAL_TIMEOUT,
            FLOOD_GLOBAL_LIMIT
        )

    def _local_submission_delay(self, client_id):
        """ Return delay before a form can be submitted (0 if no delay) by a given client """
        return self._submission_delay(
            self.local_stats.get(self._client_id_hexdigest(client_id), self.FloodStats()),
            FLOOD_LOCAL_TIMEOUT,
            FLOOD_LOCAL_LIMIT
        )

    def _create_submission_file(self, client_id):
        """ Add a file to register a valid submission """
        # Submission file name is composed of
        # - the current datetime
        # - hexadecimal digest of the client identification data (to ease dump to string and increase condifendiality)
        # - a random suffix to avoid generating same file name in multiple instances.
        self.file_name = "{}_{}_{}".format(
            datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f"),
            self._client_id_hexdigest(client_id),
            uuid.uuid4()
        )

        # Touch the file
        with open(os.path.join(FLOOD_PATH, self.file_name), 'w'):
            pass

    def _remove_submission_file(self):
        """ Remove previously created submission file """
        os.remove(os.path.join(FLOOD_PATH, self.file_name))

    def _read_and_calc_delays(self, client_id):
        """ Read submission files and return new submission delays for the given client """
        self._read_files()

        # Calculate submission delays
        global_delay = self._global_submission_delay()
        local_delay = self._local_submission_delay(client_id)
        delay = max(global_delay, local_delay)

        return global_delay, local_delay, delay

    def _create_flood_report(self, client_id, global_delay, local_delay):
        """ Return message that describe current flooding state for the given client """
        return "[FLOOD] client_id={} client_hash={} global_stats={} global_delay={} local_stats={} local_delay={}".format(
            pprint.pformat(client_id),
            pprint.pformat(self._client_id_hexdigest(client_id)),
            pprint.pformat(self.global_stats),
            pprint.pformat(global_delay),
            pprint.pformat(self.local_stats),
            pprint.pformat(local_delay),
        )

    def approve_submission(self, client_id):
        """ Check if a new submission can be accepted. Return bool and waiting delay. """

        # First, check current flooding state so that to avoid spamming mail
        # if it has already been reported.
        global_delay, local_delay, delay = self._read_and_calc_delays(client_id)

        # If flooding was already reported, send new report only to webserver log
        if delay.total_seconds() > 0:
            flood_report = self._create_flood_report(client_id, global_delay, local_delay)
            print(flood_report, file=sys.stderr)
            return False, delay

        # Then, create a submission file and recheck flood state.
        # It avoids flooding at the exact same time.
        self._create_submission_file(client_id)
        global_delay, local_delay, delay = self._read_and_calc_delays(client_id)

        # If new flood is detected, send report to webserver log *and* send a mail.
        if delay.total_seconds() > 0:
            flood_report = self._create_flood_report(client_id, global_delay, local_delay)
            print(flood_report, file=sys.stderr)

            # Send report by mail
            import smtplib
            with smtplib.SMTP(MAIL_SMTP_SERVER) as smtp:
                smtp.sendmail(MAIL_SENDER, MAIL_RECEIVERS, "Subject: Flooding du formulaire d'offre d'emploi\n\n" + flood_report)

            return False, delay

        # No delay => OK
        return True, delay

###############################################################################
class FormData(object):
    """ Job offer data extracted from the form. """

    def __init__(self,
                 client_id=None, # Client identification data
                 title='',
                 description='',
                 author='',
                 employer='',
                 email='',
                 job_type='',
                 location='',
                 duration='',
                 website='',
                 expiration='',
                 attachment_content=None,
                 attachment_name=''):

        self.client_id = client_id
        self.title = title.strip()
        self.description = description.strip()
        self.author = author.strip()
        self.employer = employer.strip()
        self.email = email.strip()
        self.job_type = job_type
        self.location = location.strip()
        self.duration = duration.strip()
        self.website = website.strip()
        self.expiration = expiration if expiration else (datetime.datetime.now() + JOBOFFER_EXPIRATION_DELAY).strftime('%Y-%m-%d')
        self.attachment_content = attachment_content
        self.attachment_name = attachment_name

    def has_attachment(self):
        """ True if there is a given attachment. """
        return self.attachment_content is not None

    def check(self):
        """ Checks form validity and returns errors.

        If multi-language is needed, we could replace the
        error messages by codes.
        """

        errors = FormError()

        # Title must be set
        if not self.title:
            errors.title = 'Titre manquant'

        # Author must be set
        if not self.author:
            errors.author = 'Nom manquant'

        # Employer must be set
        if not self.employer:
            errors.employer = 'Employeur manquant'

        # Description must be set
        if not self.description:
            errors.description = 'Description manquante'

        # Checking email
        if self.email:
            if not re.match(r'^[^@]+@[^@]+\.[^@]+$', self.email):
                errors.email = 'Adresse mail invalide'
        else:
            errors.email = 'Adresse mail manquante'

        # Checking job type
        if self.job_type not in JOBOFFER_TYPE:
            errors.job_type = "Type d'offre d'emploi invalide"

        # Checking attachment type
        if self.has_attachment():
            # Getting mime type from magic numbers.
            attachment_mime = magic.from_buffer(self.attachment_content, mime=True)

            # Checking allowed mime types.
            if attachment_mime not in ATTACHMENT_MIME_TYPE:
                errors.attachment = 'Type de fichier invalide'

        # Checking expiration date
        try:
            expiration_date = datetime.datetime.strptime(self.expiration, '%Y-%m-%d')
            if expiration_date <= datetime.datetime.now():
                errors.expiration = "L'offre doit expirer dans le futur"
        except:
            errors.expiration = "Format de date invalide"

        return errors

    def __str__(self):
        """ String representation with attachment length instead of his content. """
        return pprint.pformat(dict(itertools.chain(
            {(k, v) for k, v in vars(self).items() if k != 'attachment_content'},
            {('attachment_content_length', len(self.attachment_content) if self.has_attachment() else None)}
        )))


###############################################################################
class FormError(object):
    """ Error messages in the form. """

    def __init__(self,
                 title='',
                 description='',
                 author='',
                 employer='',
                 email='',
                 job_type='',
                 expiration='',
                 attachment=''):

        self.title = title
        self.description = description
        self.author = author
        self.employer = employer
        self.email = email
        self.job_type = job_type
        self.expiration = expiration
        self.attachment = attachment

    def has_error(self):
        """ True if there is any error set. """
        return (
            self.title
            or self.description
            or self.author
            or self.employer
            or self.email
            or self.job_type
            or self.expiration
            or self.attachment
        )

    @property
    def general(self):
        """ Error displayed at the top of the form. """
        if self.has_error():
            return 'Un ou plusieurs champs sont mal renseignés.'
        else:
            return ''

    def __str__(self):
        """ String representation. """
        return pprint.pformat(dict(itertools.chain(
            {('general', self.general)},
            vars(self).items()
        )))


###############################################################################
class GitlabFile(object):
    """ File commited to Gitlab repo. """

    def __init__(self, file_path, encoding, content):
        self.file_path = file_path
        self.encoding = encoding
        self.content = content

    def __str__(self):
        """ String representation. """
        return pprint.pformat(vars(self))


###############################################################################
class JobOffer(object):
    """ Job offer formated for a Pelican website. """

    def __init__(self, form_data):

        # Copying form fields
        self.title = form_data.title
        self.description = form_data.description
        self.author = form_data.author
        self.employer = form_data.employer
        self.email = form_data.email
        self.job_type = form_data.job_type
        self.attachment_user_name = form_data.attachment_name
        self.location = form_data.location
        self.duration = form_data.duration
        self.website = form_data.website
        self.expiration = datetime.datetime.strptime(form_data.expiration, '%Y-%m-%d')

        # Attachment special care
        if form_data.has_attachment():
            # Ensuring valid extension
            attachment_mime = magic.from_buffer(form_data.attachment_content, mime=True)
            self.attachment_ext = mimetypes.guess_extension(attachment_mime)

            # Encoding attachment in base64
            self.attachment_content = base64.b64encode(
                form_data.attachment_content).decode()

        else:
            self.attachment_content = None

        # Submission date
        self.date = datetime.datetime.now()

        # Generate unique id
        self.id = self._calc_id()

        # Rendering job offer description
        self.main_content = self._render()


    def has_attachment(self):
        """ True if the job-offer has an attachment. """
        return self.attachment_content is not None

    @property
    def name(self):
        """ Base name of the job offer. """
        return 'job_{self.id}'.format(self=self)

    @property
    def file_name(self):
        """ Name of the job-offer description file. """
        return '{self.name}.md'.format(self=self)

    @property
    def attachment_name(self):
        """ Name of the job-offer attachment. """
        if not self.has_attachment():
            return ''

        return '{self.name}_attachment{self.attachment_ext}'.format(self=self)

    @property
    def blog_file(self):
        """ The main file of the blog entry. """
        return GitlabFile(
            file_path=PELICAN_JOB_OFFER_PATH + '/' + self.file_name,
            encoding='text',
            content=self.main_content
        )

    @property
    def blog_attachment(self):
        """ Return the job offer attachment. """
        if not self.has_attachment():
            return None

        return GitlabFile(
            file_path=PELICAN_JOB_OFFER_PATH + '/' + self.attachment_name,
            encoding='base64',
            content=self.attachment_content
        )

    @property
    def gitlab_files(self):
        """ The files to be commited in Gitlab repo. """
        yield self.blog_file

        if self.has_attachment():
            yield self.blog_attachment

    def _calc_id(self):
        """ Calculate job offer id. """
        # MD5 hash
        import hashlib
        m = hashlib.md5()

        # Feeding the hash algo with the job_offer fields
        m.update(b'|'.join(str(getattr(self, field)).encode() for field in
            ('title', 'job_type', 'author', 'employer', 'date', 'description', 'email', 'attachment_user_name', 'location', 'duration', 'website', 'expiration')
        ))

        # Feeding the hash algo with the job_offer attachment
        if self.has_attachment():
            m.update(b'|' + self.attachment_content.encode())

        return m.hexdigest()

    def _render(self):
        """ Render a job offer to a Markdown file using Jinja2. """

        from jinja2 import Environment, FileSystemLoader

        # Jinja2 environment
        env = Environment(
            loader=FileSystemLoader(TEMPLATE_PATH),  # Path to the templates
            autoescape=False                         # Manual HTML escaping
        )

        # Adding custom filters
        env.filters.update({
            'markdown': filter_markdown,
            'pelican': filter_pelican,
            'linebreaks': filter_linebreaks,
            'datetime': filter_datetime
        })

        # Gets job offer template
        template = env.get_template(TEMPLATE_JOB_OFFER)

        # Rendering
        return template.render(
            title=self.title,
            date=self.date,
            slug=self.name,
            attachment_name=self.attachment_name,
            description=self.description,
            job_type=JOBOFFER_TYPE[self.job_type],
            tag=self.job_type,
            author=self.author,
            employer=self.employer,
            email=self.email,
            location=self.location,
            duration=self.duration,
            website=self.website,
            expiration=self.expiration,
        )


###############################################################################
# Jinja2 custom filters

def filter_markdown(text):
    """ Escapes all special characters of Markdown syntax. """
    return re.sub('([' + re.escape(r'\`*_{}[]()#>+-.!|') + '])', r'\\\1', text)


def filter_pelican(text):
    """
    Escapes special characters of Pelican while keeping most of
    the Markdown syntax.

    {} to avoid {filename}, {attach} and such special tags
    |  since it is the old syntax for {}
    >  (blockquote) since it will be escaped for html
    !  for inline image links.

    TODO: also escape named links ?
    TODO: modify SimpleMDE configuration accordingly
    """
    return re.sub('([' + re.escape(r'{}>|!') + '])', r'\\\1', text)


def filter_linebreaks(text, replacement='<br>'):
    """ Replaces newlines and carriage returns by the given string. """
    return re.sub('[\r\n]+', replacement, text)


def filter_datetime(date, date_format='%Y-%m-%d %H:%M'):
    """ Format a date and time. """
    return date.strftime(date_format)


###############################################################################
def create_job_request(job_offer, debug):
    """ Creates a merge request for the given job offer.

    TODO: generates job id in this function.
    """

    # Connecting to Gitlab
    gitlab_private_token = GITLAB_TOKEN
    if gitlab_private_token is None:
        print("[ERROR] GITLAB_PRIVATE_TOKEN environment variable not set!", file=sys.stderr)
        raise

    if debug.verbose:
        print('[DEBUG] Gitlab connection to {} '.format(GITLAB_URL) +
              'with token {}'.format(gitlab_private_token), file=sys.stderr)

    if not debug.offline:
        import gitlab

        gl = gitlab.Gitlab(
            GITLAB_URL,
            private_token=gitlab_private_token,
            api_version=4
        )

    # Checking connexion ?

    # Branch name associated to this job offer
    branch_name = job_offer.name

    # Accessing project
    if debug.verbose:
        print('[DEBUG] Accessing bot projet {}'.format(GITLAB_SOURCE_ID), file=sys.stderr)

    if not debug.offline:
        project = gl.projects.get(GITLAB_SOURCE_ID)

    # Creating new branch
    if debug.verbose:
        print('[DEBUG] Creating branch {} from master'.format(branch_name), file=sys.stderr)

    if not debug.offline:
        branch = project.branches.create({
            "branch": branch_name,
            "ref": GITLAB_REF_BRANCH
        })

    # Creating commit
    data = {
        "branch": branch_name,
        "commit_message": "Adding new job offer {}".format(branch_name),
        "actions": []
    }

    # Adding files to the commit
    for gitlab_file in job_offer.gitlab_files:
        data['actions'].append({
            'action': 'create',
            'file_path': gitlab_file.file_path,
            'encoding': gitlab_file.encoding,
            'content': gitlab_file.content
        })

    # Committing
    if debug.verbose:
        print('[DEBUG] Committing:', file=sys.stderr)
        print(pprint.pformat(data), file=sys.stderr)

    if not debug.offline:
        commit = project.commits.create(data)

    # Creating merge request
    pr_data = {
        'source_branch': branch_name,
        'target_branch': GITLAB_TARGET_BRANCH,
        'target_project_id': GITLAB_TARGET_ID,
        'title': 'New job offer {}'.format(branch_name),
        'description': job_offer.main_content.replace("\n", "  \n"),
        'remove_source_branch': True,
        'labels': GITLAB_LABELS
    }

    if debug.verbose:
        print('[DEBUG] Creating PR:', file=sys.stderr)
        print(pprint.pformat(pr_data), file=sys.stderr)

    if not debug.offline:
        project.mergerequests.create(pr_data)


###############################################################################
def local_write_job(job_offer):
    """ Write job offer locally. """

    for gitlab_file in job_offer.gitlab_files:
        if gitlab_file.encoding == 'text':
            with open(os.path.basename(gitlab_file.file_path), 'w') as f:
                f.write(gitlab_file.content)
        else:
            with open(os.path.basename(gitlab_file.file_path), 'wb') as f:
                f.write(base64.b64decode(gitlab_file.content.encode()))


###############################################################################
def update_html_form(form_data=FormData(), errors=FormError(), debug=Debug(), internal_error=False, success=False, flood_error=False, flood_delay=None):
    """ Update the form with error messages. """

    from jinja2 import Environment, FileSystemLoader

    # Jinja2 environment
    env = Environment(
        loader=FileSystemLoader(TEMPLATE_PATH),  # Path to the templates
        autoescape=True                          # Auto HTML escaping
    )

    # Gets job offer template
    template = env.get_template(TEMPLATE_JOB_OFFER_FORM)

    # Allowed attachment types
    file_accept = (
        ','.join([ext
            for mime_type in ATTACHMENT_MIME_TYPE
            for ext in mimetypes.guess_all_extensions(mime_type)])
        + ','
        + ','.join(ATTACHMENT_MIME_TYPE)
    )

    # Rendering
    print(template.render(
        form=form_data,
        errors=errors,
        file_accept=file_accept,
        job_type_list=JOBOFFER_TYPE,
        internal_error=internal_error,
        success=success,
        flood_error=flood_error,
        flood_delay=flood_delay,
    ))



###############################################################################
def process_form(form_data, debug=Debug()):
    """ Check and submit job-offer. """

    errors = FormError()

    try:
        # Displaying form data in debug mode
        if debug.verbose:
            print('[DEBUG] Form data: {}\n'.format(form_data), file=sys.stderr)

        # Checking form validity
        errors = form_data.check()

        if debug.verbose:
            print('[DEBUG] Form errors: {}\n'.format(errors), file=sys.stderr)

        if errors.has_error():
            update_html_form(form_data, errors, debug)
            return

        # Checking undergoing flood
        is_flood_ok, flood_delay = FloodChecker().approve_submission(form_data.client_id)
        if not is_flood_ok:
            update_html_form(form_data, errors, debug, flood_error=True, flood_delay=flood_delay)
            return

        # Creating job offer
        job_offer = JobOffer(form_data)

        # Creating pull request
        if debug.local:
            local_write_job(job_offer)
        else:
            create_job_request(job_offer, debug)

        # Success page
        update_html_form(debug=debug, success=True)

    except Exception as error:
        # Internal error
        import traceback
        print("[ERROR] Error while submitting job offer: {}".format(error), file=sys.stderr)
        print("[ERROR] {}".format(traceback.format_exc()), file=sys.stderr)
        update_html_form(form_data, errors, debug, internal_error=True)



###############################################################################
def process_cmdline():
    """ Process command-line arguments. """

    import argparse

    parser = argparse.ArgumentParser()

    # Debug options
    debug_parser = parser.add_argument_group('debug arguments')
    debug_parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='output process informations'
    )
    debug_parser.add_argument(
        '-o', '--offline',
        action='store_true',
        help='Disable Gitlab connection and pull-request creation'
    )
    debug_parser.add_argument(
        '-l', '--local',
        action='store_true',
        help='Print job offer files locally (implies offline)'
    )

    # Form options
    form_parser = parser.add_argument_group('form arguments')
    form_parser.add_argument(
        '--type',
        default='',
        help='Offer type within ' + ','.join(JOBOFFER_TYPE.keys())
    )
    form_parser.add_argument(
        '--title',
        default='',
        help='Offer title'
    )
    form_parser.add_argument(
        '--author',
        default='',
        help='Offer author name'
    )
    form_parser.add_argument(
        '--employer',
        default='',
        help='Employer'
    )
    form_parser.add_argument(
        '--email',
        default='',
        help='Offer author email'
    )
    form_parser.add_argument(
        '--location',
        default='',
        help='Job working location'
    )
    form_parser.add_argument(
        '--duration',
        default='',
        help='Job duration'
    )
    form_parser.add_argument(
        '--website',
        default='',
        help='Web page'
    )
    form_parser.add_argument(
        '--expiration',
        default='',
        help='Offer expiration date'
    )
    form_parser.add_argument(
        '--description',
        default='',
        help='Offer description'
    )
    form_parser.add_argument(
        '--attachment',
        default='',
        help='File attachment'
    )
    form_parser.add_argument(
        '--client_id',
        default=None,
        help='Client id'
    )

    # Parsing arguments
    args = parser.parse_args()

    # Set offline to true if local is true
    args.offline = args.offline or args.local

    # Debug options
    debug = Debug(verbose=args.verbose, offline=args.offline, local=args.local)

    # Reading attachment
    if args.attachment:
        with open(args.attachment, 'rb') as f:
            attachment_content = f.read()
        attachment_name = os.path.basename(args.attachment)
    else:
        attachment_content = None
        attachment_name = ''

    # Creating form data
    form_data = FormData(
        job_type=args.type,
        title=args.title,
        author=args.author,
        employer=args.employer,
        email=args.email,
        location=args.location,
        duration=args.duration,
        website=args.website,
        expiration=args.expiration,
        description=args.description,
        attachment_name=attachment_name,
        attachment_content=attachment_content,
        client_id=args.client_id,
    )

    # Continue submission process
    process_form(form_data, debug)


###############################################################################
def process_cgi():
    """ Process cgi arguments. """

    import cgi

    # Accessing CGI form data
    cgi_form = cgi.FieldStorage()

    # Default form data
    form_data = FormData()

    # Filling form fields
    if 'title' in cgi_form:
        form_data.title = cgi_form.getlist('title')[-1]

    if 'author' in cgi_form:
        form_data.author = cgi_form.getlist('author')[-1]

    if 'employer' in cgi_form:
        form_data.employer = cgi_form.getlist('employer')[-1]

    if 'email' in cgi_form:
        form_data.email = cgi_form.getlist('email')[-1]

    if 'description' in cgi_form:
        form_data.description = cgi_form.getlist('description')[-1]

    if 'job_type' in cgi_form:
        form_data.job_type = cgi_form.getlist('job_type')[-1]

    if 'location' in cgi_form:
        form_data.location = cgi_form.getlist('location')[-1]

    if 'duration' in cgi_form:
        form_data.duration = cgi_form.getlist('duration')[-1]

    if 'website' in cgi_form:
        form_data.website = cgi_form.getlist('website')[-1]

    if 'expiration' in cgi_form:
        form_data.expiration = cgi_form.getlist('expiration')[-1]

    # Checking attachment
    # TODO: checking 'done' attribute for transfer error
    if 'file' in cgi_form:
        file_item = cgi_form["file"]
        if file_item.filename:
            form_data.attachment_name = file_item.filename
            form_data.attachment_content = file_item.file.read(-1)

    # Client identification data (IP)
    form_data.client_id = os.getenv("REMOTE_ADDR")

    # Checking if the form was submitted
    if 'submit' in cgi_form:
        # Continue submission process
        process_form(form_data)
    else:
        # Displaying form without errors, possibly pre-filled through
        #   "GET" parameters (eg http://.../add_job_offer?job_type=cdd)
        update_html_form(form_data)


###############################################################################
# TODO: Tornado interface


###############################################################################
if __name__ == '__main__':

    # See https://stackoverflow.com/questions/11842547/distinguish-from-command-line-and-cgi-in-python
    if 'GATEWAY_INTERFACE' in os.environ:

        # Set UTF8 encoding for standard output (to avoid CGI encoding errors)
        # See https://stackoverflow.com/a/14860540
        sys.stdout = io.TextIOWrapper(
            sys.stdout.detach(),
            encoding='utf8',
            errors='scrict',
            line_buffering=sys.stdout.line_buffering
        )

        # HTTP header
        print('Content-type: text/html; charset=utf-8')
        print('')

        process_cgi()

    else:
        process_cmdline()
