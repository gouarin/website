Green HPC - Energie et calcul haute performance
###############################################

:date: 2015-06-17 13:06:15
:start_date: 2015-09-21
:end_date: 2015-09-25
:category: formation
:slug: Green_HPC_2015
:place: Rocquencourt, France

.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul organise une école CEA-EDF-INRIA intitulée "Green HPC - Energie et calcul haute performance" qui se déroulera sur le site INRIA Rocquencourt du 21 au 25 septembre 2015.
    
    La consommation énergétique est l'un des principaux facteurs limitant le déploiement de grandes infrastructures de calcul haute performance. Plus qu'une nouvelle métrique, elle doit être prise en compte à tous les niveaux (du matériel aux outils logiciels) et pose de nouveaux défis scientifiques et opérationnels. Le but de cette école est de rassembler des experts de premier plan sur ces sujets afin de fournir des bases solides de compréhension sur le calcul haute performance tenant compte de l'efficacité énergétique. Les aspects concrets du « Green HPC » seront explorés avec 5 journées thématiques sur les nouvelles architectures, le profilage et les mesures énergétiques, l'algorithmique efficace en consommation énergétique et les nouveaux outils logiciels verts.


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 21-09-2015

                .. event:: Green HPC : Past, present, futur
                    :begin: 09:00
                    :end: 18:00
                    :speaker: Wu Feng (Virginia Tech, USA)

            .. day:: 22-09-2015

                .. event:: Journée Nouvelles Architectures et bas niveau
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Xavier Vigouroux   (BULL)

                .. event:: Accélération par processeurs pluri-coeurs: une alternative aux solutions GPU et FPGA
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Benoît Dupond de Dinechin  (Kalray SA)

            .. day:: 23-09-2015

                .. event:: Mesure, Consolidation et Analyse de métriques systèmes pour le Cloud
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Jean-Marc Menaud  (Ecole Mines de Nantes)

                .. event:: Wattmètres logiciels — des modèles aux outils pour optimiser la consommation de vos traitements
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Romain Rouvoy  (Université de Lille / Inria)

            .. day:: 24-09-2015

                .. event:: Approches algorithmiques pour l'efficacité énergétique dans les centres de calcul
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Anne-Cécile Orgerie  (CNRS, IRISA, Rennes)

                .. event:: Efficacité énergétique et HPC : comment concilier les deux ?
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Jean-Marc Pierson  (IRIT, Université de Toulouse)

            .. day:: 25-09-2015

                .. event:: Evolution des plates-formes HPC sur le chemin de l'Exascale: Tendances et Défis 
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Sébastien Varrette  (Univ. Luxembourg)

                .. event:: Greenddleware: Ou comment les middleware deviennent vert ?
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Eddy Caron  (Ecole Normale Supérieure de  Lyon)

.. section:: Organisation
    :class: orga

    - Laurent Lefevre (Inria, Laboratoire LIP, Ecole Normale Superieure de Lyon)
    - Loïc Gouarin (CNRS, Laboratoire de Mathématiques d'Orsay)

