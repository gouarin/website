Formation Calcul Scientifique en lien avec le LEM2I
###################################################

:date: 2013-01-24 09:59:55
:category: formation
:place: Maghreb
:start_date: 2011-09-12
:end_date: 2013-01-17


.. contents::

.. section:: Description 
    :class: description

    - Cette formation est organisée dans le cadre du LEM2I et comporte plusieurs modules couvrant plusieurs aspects du calcul scientifique.
    
    - Module 1 : Montage et gestion d'un centre de calcul du 12 au 13 septembre 2011, Tipaza, Algérie
    - Module 2 : Informatique scientifique, du 12 au 14 décembre 2011, Tunis, Tunisie
    - Module 3 : Calcul parallèle, du 9 au 12 avril 2012, Rabat, Maroc
    - Module 4 : Bibliothèques et outils du calcul scientifique, du 13 au 17 janvier 2013, Tipaza, Algérie

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 12-09-2011

                .. event:: Architecture matérielle
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Introduction](attachments/spip/Documents/Ecoles/LEM2I/Mod1/architectureBase.pdf)
                        [Architecture des calculateurs](attachments/spip/Documents/Ecoles/LEM2I/Mod1/architectureBench.pdf)

                .. event:: Problématiques d'hébergement
                    :begin: 12:00
                    :end: 13:00
                    :speaker: Françoise Berthoud, LPMMC, CNRS
                    :support: attachments/spip/Documents/Ecoles/LEM2I/Mod1/Environnement_des_serveurs.pdf
                    
                .. event:: Installation de cluster
                    :begin: 13:00
                    :end: 14:00
                    :speaker: Romaric David, Université de Strasbourg
                    :support: attachments/spip/Documents/Ecoles/LEM2I/Mod1/2011_09_alger.pdf
                    
                .. event:: Gestion de ressources
                    :begin: 14:00
                    :end: 15:00
                    :speaker: Bruno Bzeznik, CIMENT, Université de Grenoble
                    :support: attachments/spip/Documents/Ecoles/LEM2I/Mod1/rjms_tipaza_2011.pdf
                    
                .. event:: Benchmarks
                    :begin: 15:00
                    :end: 16:00
                    :speaker: Françoise Berthoud, LPMMC, CNRS
                    :support: attachments/spip/Documents/Ecoles/LEM2I/Mod1/Benchmarks-Alger-13sept2011V2.pdf
                    
            .. day:: 12-12-2011

                .. event:: Architecture et programmation
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Support de cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/archi.pdf)
                        [Exemples du cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/archi_ex.tgz)

                .. event:: Compilation et édition de liens
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Romaric David, Université de Strasbourg
                    :support:
                        [Cours "Compilation"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/compil_cours.pdf)
                        [Support du cours "Compilation"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/compil_pres.pdf)
                        [Cours "Edition de liens"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/edition_liens_cours.pdf)
                        [Support du cours "Edition de liens"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/edition_liens_pres.pdf)
                        [TP "Compilation"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_compil.tgz)
                        [TP "Edition de liens"](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_editions_liens.tgz)

                .. event:: CMake
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Loïc Gouarin, Laboratoire de Mathématique d'Orsay, CNRS
                    :support:
                        [Enoncé du TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_cmake.pdf)
                        [Sources du TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_cmake.tgz)

                .. event:: Debuggage
                    :begin: 12:00
                    :end: 13:00
                    :speaker: Romaric David, Université de Strasbourg
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/cours_debug.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/TPDebug.tgz)

                .. event:: Profilage, optimisation
                    :begin: 13:00
                    :end: 14:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/optim.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_optim.tgz)

                .. event:: Subversion
                    :begin: 14:00
                    :end: 15:00
                    :speaker: Loïc Gouarin, Laboratoire de Mathématique d'Orsay, CNRS
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/cours_svn.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_subversion.pdf)

                .. event:: Doxygen
                    :begin: 15:00
                    :end: 16:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod2/doxygen.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/doxygen.tgz)

                .. event:: Mise en pratique sur un exemple complet : résolution de l'équation de la chaleur
                    :begin: 16:00
                    :end: 17:00
                    :speaker: Loïc Gouarin, Laboratoire de Mathématique d'Orsay, CNRS & Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Enoncé](attachments/spip/Documents/Ecoles/LEM2I/Mod2/Mise_en_pratique.pdf)
                        [Sources du TP](attachments/spip/Documents/Ecoles/LEM2I/Mod2/tp_mise_en_pratique.tgz)

            .. day:: 09-04-2012

                .. event:: Rappels de programmation Fortran
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Support de cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/fortran.pdf)
                        [Exemples du cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/fortran_ex.tgz)

                .. event:: Introduction au calcul parallèle
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Laurent Series, Ecole Centrale Paris
                    :support:
                        [Support de cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/paral.pdf)

                .. event:: OpenMP
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Laurent Series, Ecole Centrale Paris
                    :support:
                        [Support de cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/openMP.pdf)
                        [Exemples du cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/openmp_ex.tgz)

                .. event:: MPI
                    :begin: 12:00
                    :end: 13:00
                    :speaker: Loïc Gouarin, Laboratoire de Mathématiques d'Orsay, CNRS
                    :support:
                        [Support de cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/introductionMPI.pdf)
                        [Exemples du cours](attachments/spip/Documents/Ecoles/LEM2I/Mod3/SolutionsMPI.tgz)

                .. event:: Mise en pratique sur un exemple complet : résolution de l’équation de la chaleur en parallèle
                    :begin: 13:00
                    :end: 14:00
                    :speaker: Loïc Gouarin, Laboratoire de Mathématiques d'Orsay, CNRS, Violaine Louvet, Institut Camille Jordan, CNRS, Laurent Series, Ecole Centrale Paris
                    :support:
                        [Solution openMP](attachments/spip/Documents/Ecoles/LEM2I/Mod3/TP_openmp.tgz)
                        [Solution MPI](attachments/spip/Documents/Ecoles/LEM2I/Mod3/chaleurParallel.tgz)

            .. day:: 13-01-2013

                .. event:: Introduction aux bibliothèques scientifique
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Violaine Louvet, Institut Camille Jordan, CNRS
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Biblio/biblio.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Biblio/tp_biblio.tgz)

                .. event:: SAGE/Python
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Thierry Dumont, Institut Camille Jordan, Université Lyon 1
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Sage/sage_cours.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Sage/SageAlgérie.tar.gz)

                .. event:: Introduction à Petsc
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Jérémy Foulon, Institut du Calcul,  UPMC Paris 06
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Petsc/petsc.pdf)
                        [TP](attachments/spip/Documents/Ecoles/LEM2I/Mod4/Petsc/tp_petsc.tgz)

                .. event:: Introduction à FreeFem++
                    :begin: 12:00
                    :end: 13:00
                    :speaker: Georges Sadaka,  MAP5, Université Paris 5
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/LEM2I/Mod4/FreeFempp.pdf)

