C++ avancé, STL et librairies scientifiques
###########################################

:date: 2010-11-02 14:30:18
:start_date: 2005-12-02
:end_date: 2005-12-02
:category: journee
:tags: c++
:place: Lyon, France


.. contents::

.. section:: Prérequis
    :class: description

    Connaissance minimale du langage C++. Les notions de classes, d'objets et de conversions (cast) seront supposées connues.

.. section:: Lieu
    :class: description

    Lyon, Campus de la Doua, Ecole Supérieure Chimie Physique Electronique (CPE), salle C223.

.. section:: Programme
    :class: programme

    .. schedule::

       .. day:: 02-12-2005

	  .. break_event:: Accueil
	     :begin: 09:20
	     :end: 9:30

	  .. event:: Les design patterns
	     :begin: 9:30
	     :end: 10:45
	     :speaker: Aurélien Géron (Institut National Agronomique Paris-Grignon)
	     :support: attachments/spip/Documents/Journees/dec2005/C_avance.pdf

		- Quelques rappels utiles :

		  - templates ;
		  - namespaces ;
		  - surcharge d'opérateurs ;
		  - conversions de types.

		- Introduction aux design patterns (GoF).

		- Les principaux design patterns :

		  - conteneur/itérateur ;
		  - stratégie ;
		  - adaptateur ;
		  - composite ;
		  - fabrique ;
		  - singleton ;
		  - observateur Proxy ;
		  - visiteur.

	  .. break_event:: Pause
	     :begin: 10:45
	     :end: 11:00

	  .. event:: La STL, Standard Template Library
	     :begin: 11:00
	     :end: 12:30
	     :speaker: Aurélien Géron (Institut National Agronomique Paris-Grignon)
	     :support: attachments/spip/Documents/Journees/dec2005/C_avance.pdf

		- Objectifs et organisation de la STL
		- Les conteneurs séquentiels : vector, deque, list, bit_vector
		- Les conteneurs associatifs : set, multiset, map, multimap
		- Les itérateurs de la STL
		- Algorithmes et stratégies de la STL (find, for_each, sort, binary_search)

	  .. break_event:: Repas
	     :begin: 12:30
	     :end: 13:30

	  .. event:: La STL, suite
	     :begin: 13:30
	     :end: 14:30
	     :speaker: Aurélien Géron (Institut National Agronomique Paris-Grignon)
	     :support: attachments/spip/Documents/Journees/dec2005/C_avance.pdf

		- Les adaptateurs de conteneurs (stack, queue, priority_queue), d'itérateurs et de fonctions
		- Les chaînes de caractères (string)
		- La gestion de la mémoire avec la STL (affectations, copies, allocateurs, auto_ptr)
		- Conclusion STL : extensions, interactions avec les autres librairies

	  .. event:: Tour d'horizon des librairies scientifiques en C++
	     :begin: 14:30
	     :end: 14:45
	     :speaker: Aurélien Géron (Institut National Agronomique Paris-Grignon)

		Le tour d'horizon des librairies a pris la forme d'un discussion générale où tout le monde a fait part de ses expériences.
		On peut citer une liste non exhaustive de bibliothèques C++ scientifiques qui sont utilisées (ou simplement ont été testées) par les personnes présentes :

		- LAPACK++, ARPACK++.
		- NEWMAT : librairie d'algèbre linéaire (calculs matrices-vecteurs mais aussi des algo d'algèbre linéaire comme décomposition en valeur singulière, jacobi, ....). Un atout de cette bibliothèque est sa taille : elle est petite et se compile facilement. Son principal défaut : si on l'utilise "basiquement", les perfos sont très mauvaises (elle utilise un mécanisme d'héritage qui plombe souvent les perfos). Mais on peut tirer profits de ses avantages sans avoir à subir (tous) ses inconvénients en faisant attention.
		- BLITZ++ par Veldhuizen, l'auteur de "Techniques for scientific C++", qui utilise la technique des expression templates,
		- POOMA, développée initialement par le LANL, repris maintenant sous le nom de Free POOMA. Permet de gèrer du parallélisme et du multithreading.
		- PETE, Portable Expression Template Engine, utilisé par FreeFEM.
		- MTL, ITL, Matrix Template Library et Iterative Template Library.
		- CLN, Class Library for Numbers, pour l'arithmétique exacte.
		- APFLOAT, pour les grands rationels à précision arbitraire.
		- GMP, Gnu Multiple Precision.
		- BOOST, qui se veut la continuité du standard.Cette librairie comprend entre autres BOOSTGRAPH, système de gestion de graphe et BOOST UBLAS.
		- VTK, ITK pour la visualisation et le traitement d'images.
		- QT sur laquelle est basée KDE.
		- GTk++, pour les interfaces graphiques.
		- Binding C++ pour MPI.
		- Openthread, librairie de thread.
		- PETSC, manipulation de maillages, matrices... distribués, au dessus de MPI (il existe aussi une version openMP).
		- libXML2
		- JACKX pour le calcul symbolique.
		- `Lorene <http://www.lorene.obspm.fr/>`__ : résolution d'EDP (elliptiques et hyperboliques) à l'aide de méthodes spectrales multi-domaines et en coordonnées sphériques.
		       
	  .. break_event:: Pause
	     :begin: 14:45
	     :end: 15:00

	  .. event:: Présentation des principales librairies scientifiques (précision arbitraire, matrices, algèbre linéaire)
	     :begin: 15:00
	     :end: 16:30
	     :speaker: Pierre Raybaut

	  .. event:: Discussion sur l'utilisation du C++ en calcul scientifique
	     :begin: 16:30
	     :end: 17:00
