Outil de placement de processus Placement
##########################################################

:date: 2021-01-21 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-01-21 10:00:00
:end_date: 2021-01-21
:place: En ligne
:summary: Présentation de l'outil Placement, développé au Calmip à Toulouse, permettant de placer et de monitorer des processus.
:inscription_link: https://indico.mathrice.fr/event/244
:calendar_link: https://indico.mathrice.fr/export/event/244.ics

.. contents::

.. section:: Description
    :class: description
    
    Le placement de processus est essentiel à l'optimisation des performances au moment de l'attribution des ressources sur un cluster de calcul. Il est cependant parfois délicat, notamment dans le cas de parallélisation avec des processus légers de bien suivre si les processus s'exécutent bien suivant l'ordonnancement prévu. Cet outil permet de faciliter cette étape et d'aider à sa validation. Il permet également de vérifier que les GPUs sont effectivement utilisés le cas échéant.

    Placement est préconfiguré pour utilisation avec SLURM, mais il est simple de l'adapter à un autre gestionnaire de travaux, et on peut même l'utiliser sur un serveur en mode interactif. Il peut être installé sans avoir les droits root.
    
    La présentation durera 30-40 minutes et sera suivie d'une séance de questions.
    
    Elle est accessible à tous et aura lieu sur la plateforme BBB de Mathrice (https://greenlight.lal.cloud.math.cnrs.fr/b/ann-caa-d79). Pour des raisons techniques nous sommes obligés de limiter le nombre de participants et nous vous demandons de bien vouloir vous inscrire.

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/outils_de_placement_de_processus.59587

.. section:: Orateur
    :class: orateur

      - Emmanuel Courcelle (Calmip, Toulouse)
      
