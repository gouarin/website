ANGD "Masse de données : structuration , visualisation"
#######################################################

:date: 2011-03-25 08:01:21
:category: formation
:summary: None
:place: Lyon
:start_date: 2011-09-27
:end_date: 2011-09-30

.. contents::

.. section:: Présentation
  :class: description

  La part grandissante de l'informatique et de la simulation dans la plupart des champs disciplinaires conduit à la production de données en quantités de plus en plus importantes. C'est le cas par exemple en biologie avec le séquençage des différents génomes, en astronomie avec la multiplication des images transmises par les sondes ou les observatoires, en météorologie avec la simulation des phénomènes atmosphériques et leur visualisation, en sciences expérimentales avec l'accroissement de la taille et de la complexité des expériences, ou encore en mécanique des fluides avec des calculs de plus en plus gourmands en entrées/sorties.
  Par ailleurs, l’augmentation de la puissance des machines de calcul et la particularité de leur architecture massivement parallèle permettent aux chercheurs de progresser dans  la complexité des simulations numériques qu'ils réalisent, par exemple en accroissant la taille des domaines géométriques utilisés pour modéliser les problèmes. Si cette augmentation est plutôt bien maîtrisée au niveau calculatoire, il n'en va pas de même pour la gestion des entrées et sorties des codes de calcul, ce qui pose des problèmes par exemple en vue de leur couplage ou plus simplement de l'exploitation des résultats. Le volume de données ne doit pas devenir un facteur bloquant.
  Ainsi l'objectif de cette formation est d'approfondir les techniques relatives à ces grandes masses de données : structuration des fichiers et des données, entrées/sorties adaptées , visualisation. Pour cela, la formation dressera un panel des méthodes, formats et outils existants ... afin de permettre aux participants de choisir les éléments les plus adaptés à leur pratique quotidienne.


.. section:: Objectifs
  :class: description

  Cette formation propose une approche à la fois théorique et pratique, afin que les participants soient rapidement opérationnels sur les techniques étudiées dès leur retour dans leur laboratoire.

  Les objectifs principaux de cette formation sont d'apporter aux stagiaires les bases théoriques et pratiques liées de façon générale aux entrées/sorties des codes de calcul.

  Différents points seront abordés :

    - Problématique générale des entrées/sorties séquentielles et parallèles
    - Format de fichier HDF5, NetCDF
    - Routines d'E/S parallèles dans les codes scientifiques :MPIIO …
    - Visualisation a posteriori
    - Visualisation in situ

  Les pré-requis pour cette formation sont :

    - expérience en développement de code de calcul.

  Cette formation prendra la forme de cours théoriques qui seront complétés par des parties pratiques.
.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 27-09-2011

      .. event:: Introduction au post-traitement
        :begin: 09:00
        :end: 12:00
        :speaker: Matthieu Haefele
        :support: attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_post-processing_HDF5-basic.pdf

        Ce cours permet de poser les bases et le vocabulaire commun dans le domaine du post-traitement. Il aborde en particulier HDF5. Ce premier TP d'introduction sur Hdf5 permet d'utiliser l'API en C ou Fortran, selon vos pratiques.

      .. event:: Description des données scientifiques : HDF5, XDMF
        :begin: 14:00
        :end: 15:00
        :speaker: Matthieu Haefele
        :support: attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_HDF5-XDMF.pdf

        Au délà des seules notions d'I/O, il existe de nombreux standards et mécanismes permettant de décrire les données scientifiques. Ce cours dresse un panorama de quelques outils et formats. Il est complété par un TP utilisant les fonctionnalités avancées de Hdf5.

      .. event:: Rappels sur MPI + Architectures
        :begin: 15:00
        :end: 17:00
        :speaker: Philippe Wautelet
        :support:
          [Cours MPI](attachments/spip/Documents/Ecoles/Data-2011/IDRIS_MPI_cours_couleurs.pdf)
          [Introduction aux FS parallèles](attachments/spip/Documents/Ecoles/Data-2011/io_idris_autrans2011.pdf)
          [IO, les méthodes](attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_paralle-IO.pdf)

        La suite la formation s'appuie sur le parallélisme, ces rappels permettent de donner à tous les bases sur la principale API utilisée pour mettre en oeuvre le parallélisme dans les applicaitons scientifiques.

        Systemes de fichiers paralleles (principes, fs existants, quelques resultats...)


    .. day:: 28-09-2011

      .. event:: Le post-traitement données scientifiques
        :begin: 09:00
        :end: 10:30
        :speaker: Matthieu Haefele
        :support: attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_post-processing_chain.pdf

        Que faire de ses données une fois la simulation passée ? Comment réaliser des diagnostics ? Le cours dressera un panorama des outils utilisés suivant les disciplines (Matlab, IDL, Matplotlib, Yorick, ...)

      .. event:: Sessions parallèles
        :begin: 11:00
        :end: 12:00
        :speaker: Matthieu Haefele et Romaric David
        :support:
          [Inkscape](attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_inkscape.pdf)
          [TP](attachments/spip/Documents/Ecoles/Data-2011/2011-09_haefele_inkscape_handson.pdf)
          [Matplotlib](attachments/spip/Documents/Ecoles/Data-2011/2011_06_matplotlib.pdf)
          [NetCDF](attachments/spip/Documents/Ecoles/Data-2011/2011_06_Netcdf.pdf)

      .. event:: Sessions parallèles
        :begin: 14:00
        :end: 18:00
        :speaker: Participants

        La visualisation et vous Sous la forme de démonstrations, chaque participant présente ses pratiques actuelles de visu

    .. day:: 29-09-2011

      .. event:: Les principes de la visualisation
        :begin: 09:00
        :end: 12:00
        :speaker: Julien Jomier (Kitware)
        :support: attachments/spip/Documents/Ecoles/Data-2011/vtk_exercices.tgz

        Les principes de la visualisation,  à travers l'API  VTK, les principes du pipeline de visualisation sont étudiés. En particulier, les goulets d'étranglement liés au traitement de données volumineuses seront mis en évidence.

      .. event:: Outils de visualisation scientifique
        :begin: 14:00
        :end: 17:00
        :speaker: Jean Favre
        :support:
          [Outils de visualisation scientifique](attachments/spip/Documents/Ecoles/Data-2011/VisualizationTools.Introduction.pdf)

        Ce cours dresse un panorama des outils de visualisation disponibles, en particulier Visit. Visit est un des logiciels phares de visualisation scientifique du domaine public.

      .. event:: Soirée  Méthodes de visualisation à distance
        :begin: 18:00
        :end: 20:00
        :speaker: Société Oxalya

        Soirée  Méthodes de visualisation à distance (de type Remote Frame Buffer)  Dans un cadre général, comment déporter de manière performante une session graphique ?

    .. day:: 30-09-2011

      .. event:: Couplage simulation/visualisation in situ
        :begin: 09:00
        :end: 12:00
        :speaker: Jean Favre
        :support:
          [cours](attachments/spip/Documents/Ecoles/Data-2011/CouplageSimulationVisualization.pdf)
          [TP](attachments/spip/Documents/Ecoles/Data-2011/pjacobi_insitu.tgz)

        Le couplage avec Visit permet de mettre a disposition de la simulation, une librairie complete d'algorithmes de visualisation, avec une interface directe avec les données en mémoire pendant l'exécution de la simulation et de manière complétement indépendente des formats de fichiers.
