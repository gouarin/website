Atelier "Profilage de codes de calcul"
######################################

:date: 2014-05-12 16:31:40
:category: journee
:authors: Anne Sophie Mouronval
:place: Paris
:start_date: 2014-06-11
:end_date: 2014-06-11

.. contents::

.. section:: Description
  :class: description

  Mercredi 11 juin 2014 à l'Ecole Centrale Paris

  Face au besoin croissant de puissance de calcul des applications scientifiques, l’optimisation des codes de calculs est devenue un point clef de la simulation numérique. L’étape préalable à toute action d’optimisation consiste à **analyser les performances du code (profilage) afin d’identifier les points critiques de celui-ci dans son environnement d’exécution** . Pour ce faire, il existe de nombreux outils d’analyse qu'il convient de savoir choisir et utiliser en fonction de ses besoins.

  Le Groupe Calcul en collaboration avec le `Mésocentre de Centrale Paris <http://www.mesocentre.ecp.fr/>`__ ont accueilli Laurent Gatineau (Support applicatif, NEC HPC Europe) pour un premier atelier sur le profilage des codes de calcul séquentiels. Cette formation a eu lieu le mercredi 11 juin 2014 dans les locaux de l'Ecole Centrale Paris.

  L'enjeu de cet atelier était de donner aux participants de **bonnes bases pour débuter en profilage** .

  Cet atelier pratique comportait :

    - une introduction au profilage des codes séquentiels ;
    - une présentation des outils Gprof et PAPI ;
    - des travaux pratiques sur machine utilisant ces outils.

  Les supports de cet atelier sont ci-dessous :

    - `Introduction au profilage <attachments/spip/IMG/pdf/Profilage_Introduction.pdf>`__ (PDF)
    - `Gprof <attachments/spip/IMG/pdf/Profilage_GPROF.pdf>`__  (PDF)
    - `PAPI <attachments/spip/IMG/pdf/Profilage_PAPI.pdf>`__ (PDF)
    - `TP Gprof <attachments/spip/IMG/zip/TP_gprof.tar.zip>`__ (tar)
    - `TP PAPI <attachments/spip/IMG/zip/TP_PAPI.tar.zip>`__ (tar)

  Une deuxième session, plus orientée pour les applications multithreadées, a eu lieu en octobre 2014.
