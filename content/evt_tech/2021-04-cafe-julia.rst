Pourquoi Julia ?
################

:date: 2021-04-08 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-04-08 10:00:00
:end_date: 2021-04-08
:place: En ligne
:summary: Comment Julia résout le problème des deux langages.
:inscription_link: https://indico.mathrice.fr/event/253
:calendar_link: https://indico.mathrice.fr/export/event/253.ics

.. contents::

.. section:: Description
    :class: description

    Pour ce Café, nous accueillons François Févotte

    Sa présentation prendra la forme d'une démonstration visant à illustrer l'approche retenue par Julia pour résoudre le "problème des deux langages". Nous passerons en revue et commenterons deux petits morceaux de code Julia illustrant:

    a. le scripting de haut niveau, en assemblant des briques de base provenant de bibliothèques tierces,

    b. le développement de bibliothèques et d'algorithmes plus intensifs en calcul.

    La présentation durera 30-40 minutes et sera suivie d'une séance de questions.

    Elle est accessible à tous et aura lieu sur la plateforme BBB de Mathrice. Merci de bien vouloir vous inscrire pour suivre cette session.
    
.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/pourquoi_julia.60773

.. section:: Orateur
    :class: orateur

      - Francois Févotte (`TriScale innov <https://www.triscale-innov.com>`_)
      
