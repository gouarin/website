Neuvièmes journées mésocentres
###############################

:date: 2016-07-08 10:30:02
:start_date: 2016-10-11
:end_date: 2016-10-12
:category: journee
:authors: Bernard Dussoubs
:place: Paris


.. contents::

.. section:: Description
    :class: description

    Les 9èmes journées mésocentres auront lieu les 11 et 12/10/2016 à l'`Institut Henri Poincaré <http://www.ihp.fr>`__ à Paris.

    Au programme cette année, vous trouverez entre autres des présentations techniques (réseaux faible latence, filesystems distribués), des ateliers, des débats sur le positionnement des mésocentres dans les régions et leur modèle économique.

    Les inscriptions sont ouvertes, gratuites et obligatoires pour des questions d'organisation.

    Ces journées seront suivies, le 13 octobre 2016, même lieu, par la **4ème journée MesoChallenge** organisée par le projet Equip@Meso.
    Contact : Michel Kern


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 11-10-2016

            .. event:: Introduction
                :begin: 13:30
                :end: 13:40

            .. event:: Mellanox - Interconnecter votre futur
                :begin: 13:40
                :end: 14:10
                :speaker: Saddik El Arguioui
                :support:
                  [présentation](attachments/spip/IMG/pdf/mellanox_presentation_-_journees_mesocentres_2016_-_2.pdf)

            .. event:: Bull Exascale Interconnect (BXI), un nouveau réseau pour le calcul de haute performance
                :begin: 14:10
                :end: 14:40
                :speaker: Jean-Pierre Panziera
                :support:
                  [présentation](attachments/spip/IMG/pdf/2016_10_11_jouneesmesocentres_bxi.pdf)

            .. event:: Intel® Omni-Path: la nouvelle génération de Fabrics HPC
                :begin: 14:40
                :end: 15:10
                :speaker: Alexandre Chauvin
                :support:
                  [présentation](attachments/spip/IMG/pdf/intel-omni-path-for-journees-mesocentre.pdf)

            .. event:: Ateliers en parallèle
                :begin: 15:40
                :end: 17:30
                :speaker: Alexandre Chauvin
                :support:
                  [Présentation de B. Bzeznik](attachments/spip/IMG/pdf/2016-10-11-nix_journees_meso.pdf)
                  [Présentation de L. Nussbaum–](attachments/spip/IMG/pdf/grid5000.pdf)
                  [Restitution](attachments/spip/IMG/pdf/2016_10_11_atelier_calcul.pdf)

                - Schedulers : quelles fonctionnalités, pour quels usages, quid de la fiabilité et de la scalabilité, etc., animation **(Matthieu Marquillie)**
                - Déploiement/virtualisation/conteneurs : comment, dans les mésocentres, est mise en oeuvre la virtualisation légère (via des conteneurs) ou classique, et quels nouveaux services peuvent ainsi être offerts aux utilisateurs, animation **(Romaric David)**

            .. event:: Restitution à l'ensemble des participants
                :begin: 17:30
                :end: 18:00

            .. event:: Apéritif de conclusion de la journée
                :begin: 18:00
                :end: 19:00


        .. day:: 12-10-2016

            .. event:: Intervention de Genci
                :begin: 9:00
                :end: 9:30
                :speaker: Philippe Lavocat
                :support:
                  [présentation](attachments/spip/IMG/pdf/journee_mesocentres_genci_12-10-2016.pdf)


            .. event:: Intervention du Ministère
                :begin: 9:30
                :end: 10:00
                :speaker: Laurent Crouzet
                :support:
                  [présentation](attachments/spip/IMG/pdf/menesr_crouzet.pdf)

            .. event:: Intervention du CNRS/COCIN
                :begin: 10:00
                :end: 10:30
                :speaker: Denis Veynante
                :support:
                  [présentation](attachments/spip/IMG/pdf/journees_mesocentres.pdf)

            .. event:: Table-ronde : quelles évolutions pour les mésocentres dans le contexte des nouvelles régions ?
                :begin: 11:00
                :end: 12:30
                :speaker: animé par Marie-Alice Foujols

                  Un mésocentre est par nature innovant, tourné vers l’avenir, en extension, en lien avec les autres mésocentres et avec les autres ressources nationales, européennes et internationales. En interaction forte avec ses utilisateurs c’est un maillon essentiel de l’écosystème HPC. Le besoin de renouveler régulièrement les matériels implique de trouver des partenaires financiers de confiance, ce qui a été construit dans de nombreuses régions. La mise en place des nouvelles régions nous questionne donc sur les fondements de chacun des mésocentres. Quelles évolutions dans ce contexte nouveau?

            .. event:: Lustre et Lustre-HSM au CINES
                :begin: 14:00
                :end: 14:20
                :speaker: Olivier Rouchon
                :support:
                  [présentation](attachments/spip/IMG/pdf/2016-10-12_jmc.pdf)

            .. event:: BeeGFS
                :begin: 14:00
                :end: 14:20
                :speaker: Romaric David
                :support:
                  [présentation](attachments/spip/IMG/pdf/2016_10_11_filesystems_meso_unistra.pdf)

            .. event:: Glusterfs
                :begin: 14:40
                :end: 15:00
                :speaker: Emmanuel Quemener
                :support:
                  [présentation](attachments/spip/IMG/pdf/glusterfs.pdf)

            .. event:: Initiative PME/SIMSEO, la simulation au service des entreprise
                :begin: 15:00
                :end: 15:30
                :speaker: Thomas Palychata
                :support:
                  [présentation](attachments/spip/IMG/pdf/simseo_12_10_16.pdf)

            .. event:: Table-ronde : Modèle économique des mésocentres
                :begin: 16:00
                :end: 17:30
                :speaker: animé par Violaine Louvet
                :support:
                    [présentation](attachments/spip/IMG/pdf/iscd_mesocentres_2016.pdf)
                    [présentation](attachments/spip/IMG/pdf/2016-10-12_mesocentres_modeles_eco.pdf)

                Chaque mésocentre a un mode de financement qui lui est propre, mais la problématique de la jouvence régulière des machines et de la prise en charge de leur fonctionnement est une question récurrente et partagée par tous.
                La pérennisation de ces infrastructures doit passer par une réflexion globale, et cette table ronde a pour objectif d'identifier les points clés permettant de conduire cette réflexion.

                Intervenants :

                - Bijan Mohammadi, `HPC@LR <https://hpc-lr.umontpellier.fr/>`__
                - Edouard Brunel, GENCI
                - Pascal Frey, UPMC
                - Sylvie Niessen, MENESR

            .. event:: Conclusion
                :begin: 17:30
                :end: 17:35
