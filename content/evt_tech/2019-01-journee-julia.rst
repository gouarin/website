Journée Julia
#############

:date: 2019-06-19 09:10:03
:category: journee
:tags: julia
:start_date: 2019-01-31
:end_date: 2019-01-31
:place: Lyon
:summary: Journée autour du langage Julia.
:inscription_link: http://lyoncalcul.univ-lyon1.fr/events/2019/Journee_Julia/

.. contents::

.. section:: Description
    :class: description
    
    Le langage Julia est un langage de haut niveau, relativement récent, orienté vers le calcul scientifique et la haute performance. Il est de plus en plus utilisé pour le calcul scientifique et notamment pour le traitement des données. Le réseau a déjà proposé en 2015 une première introduction à ce langage. La version 1.0 du langage est sortie e juillet 2018.

    Afin de suivre de cette évolution, le Groupe Calcul organise Le jeudi 31 janvier 2019 une journée sur Julia à Lyon, sur le campus de la Doua, avec le soutien de la Fédération Lyonnaise de Calcul.

    **Lieu** : salle René Michel, bât. Oméga, campus de la Doua, Lyon

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 31-01-2019

            .. event:: Introduction au langage
                :speaker: Thierry Clopeau
                :begin: 9:30
                :end: 11:30
                :support: [Lien vers les supports](https://github.com/clopeau/Julia_Introduction)

            .. event:: MapReduce
                :speaker: Xavier Vasseur
                :begin: 11:30
                :end: 12:30
                :support: 
                    [slides](attachments/spip/Documents/Journees/jan2019/julia_map_reduce.pdf)
                    [notebooks](attachments/spip/Documents/Journees/jan2019/notebooks-2.zip)

            .. break_event:: Pause
                :begin: 12:30
                :end: 14:00

            .. event:: Packaging
                :speaker: Pierre Navaro
                :begin: 14:00
                :end: 15:00
                :support: [Lien vers les supports](https://pnavaro.github.io/LyonCalcul.jl/dev/slides/)

            .. event:: Interfaçage avec d’autres langages
                :speaker: Marc Fuentes
                :begin: 15:00
                :end: 16:00
                :support: [Lien vers les supports](https://gitlab.inria.fr/fuentes/juliabindings)

            .. event:: Benchs de performance
                :speaker: Benoît Fabrèges
                :begin: 16:00
                :end: 17:00
                :support: [Lien vers les supports](https://plmlab.math.cnrs.fr/fabreges/julia-2019)


.. section:: Organisation
    :class: orga

        - Anne Cadiou
