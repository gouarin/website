Atelier Visualisation in Situ
#############################

:date: 2017-12-18 17:14:50
:start_date: 2017-12-11
:end_date: 2017-12-13
:category: formation
:place: Toulouse, France
:summary: L'objectif de l'atelier est de découvrir et de mettre en oeuvre des solutions techniques avec les librairies de VisIt et ParaView pour faire de la Visualisation in Situ : génération d'images et visualisation interactive distante en cours de calcul.

.. contents::

.. section:: Description
    :class: description

    L'objectif de l'atelier est de découvrir et de mettre en oeuvre des solutions techniques avec les librairies de VisIt et ParaView pour faire de la Visualisation in Situ : génération d'images et visualisation interactive distante en cours de calcul.

    **Lieu**

    Institut de Mécanique des Fluides de Toulouse

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 11-12-2017

                .. event:: Visualisation scientifique parallèle de gros volumes de données
                    :begin: 09:00
                    :end: 10:00
                    :speaker: J. M. Favre
                    :support:
                        [jeanfavre_parallelvisualization.pdf](attachments/spip/IMG/pdf/jeanfavre_parallelvisualization.pdf)

                .. event:: Introduction à Paraview
                    :begin: 10:15
                    :end: 12:00
                    :speaker: J. M. Favre
                    :support:
                        [vtk_visualization_pipeline.pdf](attachments/spip/IMG/pdf/vtk_visualization_pipeline.pdf)
                        [vtk_scripts.tar.gz](attachments/spip/IMG/gz/vtk_scripts.tar.gz)
                        [paraview_scripts.tar.gz](attachments/spip/IMG/gz/paraview_scripts.tar.gz)

                .. event:: Présentation Libsim
                    :begin: 14:00
                    :end: 16:00
                    :speaker: J. M. Favre
                    :support:
                        [visit_libsim.pdf](attachments/spip/IMG/pdf/visit_libsim.pdf)

            .. day:: 12-12-2017

                .. event:: Introduction à la problématique de la visualisation et à VisIt et démonstration de VisIt
                    :begin: 09:00
                    :end: 10:00
                    :speaker: P. Wautelet
                    :support:
                        [philippewautelet_introduction_a_visit.pdf](attachments/spip/IMG/pdf/philippewautelet_introduction_a_visit.pdf)

                .. event:: Présentation du mésocentre CALMIP et de sa solution de visualisation à distance
                    :begin: 10:00
                    :end: 11:00
                    :speaker: E. Courcelle
                    :support:
                        [calmip_2017.pdf](attachments/spip/IMG/pdf/calmip_2017.pdf)

                .. event:: Démo de visualisation distante avec VisIt/libsim et ParaView/Catalyst sur le super-calculateur du mésocentre CALMIP
                    :begin: 11:00
                    :end: 12:00
                    :speaker: A. Pedrono et H. Neau

                .. event:: Retour d'expérience "Analyse et Visu in Situ"
                    :begin: 14:00
                    :end: 15:00
                    :speaker: A. Cadiou
                    :support:
                        [Workshop Traitement de données pour la Mécanique des Fluides](https://indico.lal.in2p3.fr/event/4644/timetable/#20171130)

                .. event:: Atelier "Démonstrateur VisIt/Libsim"
                    :begin: 15:00
                    :end: 16:00
                    :speaker: A. Pedrono, E. Courcelle, A. Cadiou, H. Neau, J. M. Favre, P. Elyakime
                    :support:
                        [tp_visu2017_visit_libsim.tar.gz](attachments/spip/IMG/gz/tp_visu2017_visit_libsim.tar.gz)

            .. day:: 13-12-2017

                .. event:: Atelier "Démonstrateur ParaView/Catalyst"
                    :begin: 09:00
                    :end: 12:00
                    :speaker: H. Neau, A. Pedrono, E. Courcelle, A. Cadiou
                    :support:
                        [tp_visu2017_catalyst.tar.gz](attachments/spip/IMG/gz/tp_visu2017_catalyst.tar.gz)

.. section:: Organisation
    :class: orga

    - Annaïg Pedrono (INP Toulouse & Institut de Mécanique des Fluides de Toulouse)
    - Hervé Neau (CNRS & IMFT)
    - Emmanuel Courcelle (CNRS & Mésocentre CALMIP)
