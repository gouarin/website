ANF Julia - Fréjus 
###################

:date: 2021-05-05
:category: formation
:tags: julia, langage
:start_date: 2021-09-13
:end_date: 2021-09-17
:place: CAES de Fréjus
:summary: Action Nationale de Formation portée par le groupe Calcul pour promouvoir l'utilisation du langage Julia
:inscription_link: https://indico.mathrice.fr/event/252/registration

.. contents::

.. section:: Description de la formation

    Le langage `Julia <https://julialang.org>`_, a été concu pour faire des sciences et en particulier, coder des mathématiques. Il se révèle particulièrement adapté pour le calcul scientifique et statistique et offre de nombreux packages sur ces deux thématiques.

    Cette Action Nationale de Formation (ANF) a pour but d'enseigner le langage Julia pour écrire des codes de recherche performants. Elle s'adresse à tous les ingénieur·e·s et chercheur·e·s utilisant le calcul en ayant le souci de partager et d'assurer un minimum de reproductibilité à leurs expériences numériques. En fonction des objectifs propres à chacun des participant·e·s, cette semaine de formation peut permettre d'enrichir la communauté francophone de développeurs Julia issus du monde académique.

    **Hébergement et prise en charge**

    - Villa Clythia à Fréjus

    Pour en savoir plus sur le lieu de la formation et comment s'y rendre, consultez le `site du CAES <https://www.caes.cnrs.fr/sejours/la-villa-clythia/>`_.

    La formation est ouverte à tous mais les personnels CNRS seront prioritaires pour suivre la formation.

    Les frais d’hébergement en chambre individuelle ainsi que les frais pédagogiques sont pris en charge par le CNRS. Les frais de transport des agents CNRS sont pris en charge par la délégation d’origine de l’agent à sa demande. Ils doivent faire une demande d'inscription à une formation sur la plateforme `Ariane <https://ariane.cnrs.fr/>`_. Pour les non CNRS, les frais de transport doivent être pris en charge par votre organisme de tutelle ou laboratoire.

    **Prérequis**

    - Être autonome en environnement Linux / shell bash
    - Avoir une expérience de la programmation dans un langage interprété (Python/R) et/ou un langage compilé (C/C++/Fortran).
    - Connaître les rudiments de `git` pour récupérer facilement les matériels pour les travaux pratiques.
    - Être équipé d'un ordinateur portable personnel pouvant se connecter sur un réseau sans-fil.

    **Un nombre de places limité**

    La formation pourra accueillir ``25 participants``, les organisateurs se laissent la possibilité d'opérer une sélection en fonction des renseignements portés sur la fiche d’inscription. Vos motivations, votre projet, la thématique sur lequel vous travaillez, votre expérience en programmation dans d'autres langages sont les critères qui seront pris en compte lors de l'évaluation des candidatures. Avoir un projet de développement en Julia est un plus, les experts présents durant la formation pourront y consacrer du temps.
    La formation ne sera pas filmée et les supports seront accessibles uniquement aux inscrit·e·s.

    Date limite de pré-inscription : **11/06/2021**

    La réponse vous parviendra courant juin. Votre inscription suppose que vous êtes pleinement disponibles durant la période de formation du **13 au 17 septembre inclus**. Nous comptons sur votre bienveillance pour limiter au maximum les annulations tardives. Nous allons mettre en place une liste d'attente.

    `Pour s'inscrire <https://indico.mathrice.fr/event/252/registration/>`_

.. section:: Programme
    :class: programme

    .. schedule::
        :indico_url: https://indico.mathrice.fr/
        :indico_event: 252

.. section:: Partenaires
    :class: description

    .. container:: text-align-center

        .. image:: attachments/logo-partenaires/cnrs.png
           :alt: CNRS
           :target: https://www.cnrs.fr
           :height: 100px
        .. image:: attachments/logo/Logo_small_noir.png
           :alt: Groupe Calcul
           :target: http://calcul.math.cnrs.fr/
           :height: 100px
        .. image:: attachments/logo-partenaires/triscale_couleur.png
           :alt: TriScale innov
           :target: https://www.triscale-innov.com
           :height: 100px

.. section:: Organisation
    :class: orga

    - Pierre Navaro (IRMAR Rennes)
    - Céline Parzani (TSE Toulouse)
    - Benoît Fabrèges (ICJ Lyon)

