ANF Évaluation de |_| performance pour les |_| codes HPC
########################################################

:date: 2019-05-02
:category: formation
:tags: performance, hpc, outils
:start_date: 2019-09-16
:end_date: 2019-09-20
:place: Observatoire de Haute Provence
:summary: Action Nationale de Formation portée par le groupe Calcul pour diffuser l'utilisation des outils d'évaluation de performance.
:inscription_link: https://indico.mathrice.fr/event/160/registration


.. contents::

.. section:: Description
    :class: description


    Le processus d'optimisation de code se décompose principalement en deux phases :

    - Mesure de performance
    - Modification du programme dans le but d'améliorer la performance

    Cette Action Nationale de Formation (ANF) contribue à la première étape qui est généralement faite de manière incomplète car les outils d'évaluation de performance sont malheureusement encore difficiles à mettre en place et à utiliser. L'objectif est de vous mettre le pied à l'étrier et vous rendre autonomes au maximum dans l'utilisation de ces outils, de les intégrer dans vos codes de calcul, de produire des métriques de performance et d'établir une feuille de route de développement logiciel pour optimiser votre code.

    Cet objectif est réaliste grâce à l'intervention des experts du centre d'excellence européen `POP <https://pop-coe.eu/>`_ qui regroupe les meilleurs experts dans ce domaine au niveau européen et les développeurs des outils Scalasca et Paraver.

    **Deux parcours :**

    - Parcours « Base des outils » sur 2 jours ouvert à 25-30 personnes : présentation des outils Scalasca et Paraver (cours + TP).
    - Parcours « Diagnostic HPC » sur 3 jours supplémentaires et ouvert à 10 personnes : intégration des outils dans vos codes, productions et analyse des métriques de performance et établissement d'une feuille de route d'optimisation.

    **Prise en charge**

    Les cours, l'hébergement et les repas sont pris en charge par l'Action Nationale de Formation grâce au soutient financier du centre d'excellence EoCoE et du CNRS. Les frais de transports sont à la charge des participants. Les agents CNRS et membres des UMR associées peuvent s'addresser au service de formation permanente de leur Délégation Régionale pour la prise en charge de leur déplacement.

    **Un nombre de places limité**

    Vos motivations, votre projet et le code de calcul sur lequel vous souhaitez travailler, sa maturité et son impact sur les communautés scientifiques sont les critères qui seront pris en compte lors de l'évaluation des candidatures.

    Date limite de pré-inscription : **17/05/2019**

    Réponse mi-juin.

    `Pour s'inscrire <https://indico.mathrice.fr/event/160/registration/>`_


    Nombre de participants : 22

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 16-09-2019
                
                .. event:: Paraver : cours
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Judit Gimenez, Germain Llort
        
                    
                    Paraver (https://tools.bsc.es/paraver) a été développé pour répondre au besoin d'avoir une perception qualitative globale du comportement de l'application via une approche graphique pour ensuite se concentrer sur une analyse quantitative détaillée des problèmes. 
                    
                    Paraver n'est attaché à aucun modèle de programmation tant que le modèle peut être calé sur les trois niveaux de parallélisme exprimé dans la trace Paraver. Un exemple de parallélisme à deux niveaux serait une application hybride MPI + OpenMP. 
                    Le système de mesures lors des exécutions Extrae qui génèrant les traces Paraver supporte actuellement les interfaces de programmation MPI , OpenMP, pthreads, OmpSs et CUDA.
                
                .. break_event:: Pause
                    :begin: 15:30
                    :end: 16:00
        
                
                
                .. event:: Paraver : pratique 1
                    :begin: 16:00
                    :end: 18:00
                    :speaker: Judit Gimenez, Germain Llort
                    :support:
                        [support 1](attachments/evt/2019-09-anf-perf-eval-hpc/support00.pdf)
                        [support 2](attachments/evt/2019-09-anf-perf-eval-hpc/support01.pdf)
                        [support 3](attachments/evt/2019-09-anf-perf-eval-hpc/support02.pdf)
        
                    
                    
                
                .. break_event:: Dîner
                    :begin: 19:30
                    :end: 21:00
        
                
        .. day:: 17-09-2019
                
                .. event:: Paraver : pratique 2
                    :begin: 09:00
                    :end: 12:30
                    :speaker: Judit Gimenez
        
                    
                    
                
                .. break_event:: Déjeuner
                    :begin: 12:30
                    :end: 14:00
        
                
                
                .. event:: Scalasca : cours
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Brian Wylie
                    :support:
                        [support 1](attachments/evt/2019-09-anf-perf-eval-hpc/support03.pdf)
                        [support 2](attachments/evt/2019-09-anf-perf-eval-hpc/support04.pdf)
                        [support 3](attachments/evt/2019-09-anf-perf-eval-hpc/support05.pdf)
                        [support 4](attachments/evt/2019-09-anf-perf-eval-hpc/support06.pdf)
                        [support 5](attachments/evt/2019-09-anf-perf-eval-hpc/support07.pdf)
                        [support 6](attachments/evt/2019-09-anf-perf-eval-hpc/support08.pdf)
                        [support 7](attachments/evt/2019-09-anf-perf-eval-hpc/support09.pdf)
        
                    
                    Scalasca est un logiciel qui aide à l'optimisation des applications parallèles en mesurant et en analysant leur comportement durant l'exécution. L'analyse permet d'identifier des goulots d'étranglements potentiels – en particulier ceux concernant les communications et les synchronisations – et permet d'explorer leurs causes.
                    
                    Scalasca supporte les codes pure MPI ainsi que les codes hybrides MPI + OpenMP.
                
                .. break_event:: Pause
                    :begin: 15:30
                    :end: 16:00
        
                
                
                .. event:: Scalasca : pratique 1
                    :begin: 16:00
                    :end: 17:30
                    :speaker: Brian Wylie
        
                    
                    
                
                .. break_event:: Dîner
                    :begin: 19:30
                    :end: 21:00
        
                
        .. day:: 18-09-2019
                
                .. event:: Scalasca : pratique 2
                    :begin: 09:00
                    :end: 12:30
                    :speaker: Brian Wylie
        
                    
                    
                
                .. break_event:: Déjeuner
                    :begin: 12:30
                    :end: 14:00
        
                
                
                .. break_event:: Social event
                    :begin: 14:00
                    :end: 19:00
        
                
                
                .. break_event:: Dîner
                    :begin: 19:30
                    :end: 21:00
        
                
        .. day:: 19-09-2019
                
                .. event:: Code des participants : instrumentation / analyse
                    :begin: 09:00
                    :end: 12:30
                    :speaker:  Tous les intervenants
        
                    
                    
                
                .. break_event:: Déjeuner
                    :begin: 12:30
                    :end: 14:00
        
                
                
                .. event:: Code des participants : instrumentation / analyse
                    :begin: 14:00
                    :end: 17:30
                    :speaker:  Tous les intervenants
        
                    
                    
                
                .. break_event:: Dîner
                    :begin: 19:30
                    :end: 21:00
        
                
        .. day:: 20-09-2019
                
                .. event:: Code des participants: instrumentation / analyse
                    :begin: 09:00
                    :end: 12:30
                    :speaker:  Tous les intervenants
        
                    
                    
                
                .. break_event:: Déjeuner
                    :begin: 12:30
                    :end: 14:00
        
                

.. section:: Partenaires
    :class: description

    .. container:: text-align-center

        .. image:: attachments/logo-partenaires/eocoe.png
           :alt: Projet H2020 EoCoE
           :target: https://www.eocoe2.eu/
           :height: 100px
        .. image:: attachments/logo-partenaires/cnrs.png
           :alt: CNRS
           :target: https://www.cnrs.fr
           :height: 100px
        .. image:: attachments/logo/Logo_small_noir.png
           :alt: Groupe Calcul
           :target: http://calcul.math.cnrs.fr/
           :height: 100px
        .. image:: attachments/logo-partenaires/pop.png
           :alt: Projet H2020 POP
           :target: https://pop-coe.eu/
           :height: 80px


.. section:: Organisation
    :class: orga

    - Matthieu Haefele
    - Anne Cadiou
    - Fabrice Roy
