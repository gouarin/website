"use strict";

/*
Script to parse JSON arriving from indico "timetable" and to insert
 these information in a div.
*/


(function(exports){


  function create_authors_str(collab){
    let result = collab.name;
    if(collab.affiliation !==''){ result += ' ('+collab.affiliation+')';}
    return result;
  };


  function create_time_str(t_start, t_end){
    let start = t_start.time.split(':');
    start.pop(); start = start.join(':');
    let result = start;

    if(t_end){
      let end = t_end.time.split(':');
      end.pop(); end = end.join(':');
      result += '-' + end;
    }
    return result;
  }

  function serialize_Indico(base, level, obj)
  {
    // convert object into array
    let sortable = [];

    for(let key in obj){
      if(obj[key].hasOwnProperty('startDate')){
	let newkey = base + ':' + obj[key]['startDate']['time'];
	obj[key].level = level;

	if(obj[key].hasOwnProperty('presenters')){
	  obj[key].orators_str = obj[key].presenters.map(create_authors_str).join(', ') + '';
	}

	if(level === 1){
	  obj[key].times_str = create_time_str(obj[key].startDate,obj[key].endDate);
	}
	else{
	  obj[key].times_str = create_time_str(obj[key].startDate);
	  }

	sortable.push([newkey, obj[key]]); // each item is an array in format [key, value]
      }
      if(obj[key].hasOwnProperty('entries')){
	obj[key]['entries'] = serialize_Indico(base, level+1, obj[key]['entries']);
      }
    }
    // sort items by value
    sortable.sort(function(a, b){
      let x=a[0].toLowerCase(),
  	  y=b[0].toLowerCase();
      return x<y ? -1 : x>y ? 1 : 0;
    });
    return sortable.map(function(d){return d[1];}); // array
  }


  var template = ''+
    '{{#data}}'+
    '<div class="indico_1">' +
    ' <div class="indico_contain">'+
    '  <div class="indico_time">{{times_str}}</div>' +
    '  <div class="indico_header">' +
    '   <div class="indico_title">{{title}}</div>' +
    '   <div class="indico_desc">{{description}}</div>' +
    '  </div>' +
    ' </div>' +
    ' {{#entries}}'+
    '   <div class="indico_2">'+
    '    <div class="indico_contain">'+
    '     <div class="indico_time">{{times_str}}</div>'+
    '     <div class="indico_header">'+
    '      <div class="indico_title">{{title}}</div>'+
    '      <div class="indico_desc">{{description}}</div>'+
    '     </div>'+
    '    </div>'+
    '    {{#orators_str}}'+
    '     <div class="indico_conveners">Contributeurs : {{orators_str}}</div>'+
    '    {{/orators_str}}'+
    '    {{#material}}'+
    '     <div class="indico_doc"> Document : '+
    '      <a href="{{resources.0.url}}">{{title}}</a>'+
    '     </div>'+
    '    {{/material}}'+
    '    </div>'+
    ' {{/entries}}'+
    ' <div class="indico_loc">Lieu : {{room}}</div>' +
    '</div>'+
  '{{/data}}';


  function MakeUp_Indico(key,data){

    // day level
    moment.locale('fr');
    let m = moment(key, ['YYYYMMDD']).format('dddd DD MMMM YYYY');

    //date
    let newele = document.createElement('div');
    newele.className = 'indico_event';


    let date = document.createElement('div');
    date.className = 'indico_date';
    date.innerHTML = m;
    newele.appendChild(date);


    newele.innerHTML += Mustache.to_html(template,{ data : data});


    return newele;
    console.log(data);
}



  function parse_indico_json(data, nb_event, where){
    let event = data.results[nb_event],
	element = document.getElementById(where),
	newdiv = document.createElement('div');

    // loop on days
    for (let key in event){

      // any day is serialized
      let day_event = serialize_Indico(key,1,event[key]);

      // for any day the div is created and appended in where element
      newdiv.appendChild( MakeUp_Indico(key,day_event));
    }

    element.appendChild(newdiv);
  }

  /* Exported fuctions */
  exports.parse = parse_indico_json;

})(this.IndicoPlanning = {});
