Journée "Méthode de Galerkin discontinue et ses applications"
#############################################################

:date: 2015-04-15 08:49:58
:modified: 2015-04-15 08:49:58
:category: journee
:start_date: 2015-06-19
:end_date: 2015-06-19
:place: Paris
:attendees: 42
:summary: Une journée sur les méthodes de Galerkin discontinues.

.. contents::

.. section:: Description
    :class: description

    Ce colloque se déroulera le vendredi 19 juin 2015 dans les locaux du Conservatoire National des Arts et Métiers, 292 rue Saint-Martin, 75003 Paris, amphi Jean Prouvé.

    Le but de cette journée est regrouper des chercheurs et des scientifiques à l'interface des méthodes de discrétisation, du calcul numérique et des sciences appliquées.

    Depuis une vingtaine d'années, la méthode de Galerkin discontinue a progressivement étendu ses champs d'application en mécanique des fluides, en aérodynamique, en acoustique, en électromagnétisme, en modélisation du plasma, etc.

    Ce colloque se propose de présenter les applications de la méthode de Galerkin discontinue dans ces différents domaines.

    La présence de jeunes chercheurs ou d'étudiants en thèse est vivement encouragée.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 19-06-2015

            .. break_event:: Accueil
                :begin: 09:30
                :end: 10:00

            .. event:: Discontinuous Galerkin methods for first-order PDEs
                :begin: 10:00
                :end: 11:00
                :speaker: Alexandre Ern (Université Paris-Est, CERMICS, ENPC)
                :support: attachments/spip/IMG/pdf/ern_cnam.pdf

                The talk will consist in three parts. In the first one, we review the analysis of the discontinuous Galerkin (dG) approximation of systems of steady linear PDEs endowed with the symmetry and positivity properties introduced by Friedrichs. In the second part, we move on to linear transport (unsteady advection-reaction) and use dG for time discretization. We present new analysis results concerning, in particular, bounds on the error derivatives. In the last part, we consider linear transport with rough solutions and nonlinear conservation laws, and we offer some thoughts, supported by theoretical and numerical results, on the use of linear stabilization in this context.


            .. event:: Discontinuous Galerkin Methods for Phase Transition and Dispersed Multiphase Flows
                :begin: 11:00
                :end: 12:00
                :speaker: Jaap van der Vegt (University of Twente)

                In this presentation we will discuss several discontinuous Galerkin methods for phase transition and dispersed multiphase flows. We will start with a local discontinuous Galerkin method for the Navier-Stokes-Korteweg (NSK) equations to compute phase transition between liquid and vapor in compressible fluids. The NSK equations contain, next to the viscous stress tensor, also the Korteweg tensor, and are suitable for a diffuse interface method, where the phases are distinguished using different values of the density. The addition of the Korteweg tensor to the Navier-Stokes equations results in a system of third order partial differential equations, which can locally change type in the phase transition region. Both the isothermal and non-isothermal NSK equations will be discussed. The NSK equations are closed using the Van der Waals equation of state. Since the LDG discretisation of the NSK equations results in a stiff system of ordinary differential equations, we use a Singly Diagonally Implicit Runge-Kutta method for the time integration. The resulting nonlinear algebraic equations are solved with a Newton method. We will demonstrate the LDG discretization for several test cases, including the coalescence of two vapor bubbles.

                In the second part of the presentation we will consider a space-time discontinuous Galerkin discretization for dispersed multiphase flows, modeled by non-conservative hyperbolic partial differential equations. Using the theory of non-conservative products we will present a discontinuous Galerkin method for this type of equations, including a special numerical flux based on the HLL approximate Riemann solver. We will demonstrate this DG method on several dispersed flow models, in particular the Pitman and Le model.

            .. break_event:: Repas
                :begin: 12:00
                :end: 13:30

            .. event:: Discontinuous Galerkin Methods for Computational Electromagnetics?
                :begin: 13:30
                :end: 14:30
                :speaker: Jan Hesthaven (École polytechnique fédérale de Lausanne)

                During the last decade, discontinuous Galerkin methods have risen to become a widely used tool in computational electromagnetics, moving from purely academic uses to large scale industrial applications and emerging also as an option in commercial software.

                We shall discuss the central elements of the formulation, focusing on time-domain methods where the impact has been most pronounced. After reviewing the basis elements of the formulation and its properties, we discuss some of the more recent developments that have allowed this method to become a flexible and efficient tool for the modeling of a variety of complex problems.

                While the impact of discontinuous Galerkin methods for frequency domain problems has been less pronounced, we briefly also touch on the main development in this domain.

                Throughout the talk, we shall illustrate the performance by examples and conclude with a few remarks on open questions and challenges.

            .. event:: Méthodes de Galerkine discontinu pour des problèmes d'aérodynamique
                :begin: 14:30
                :end: 15:30
                :speaker: Vincent Perrier (INRIA, Université de Pau)
                :support: attachments/spip/IMG/pdf/perrier_cnam.pdf

                Nous commencerons par présenter les problèmes d'aérodynamique auxquels nous nous intéressons, et nous montrerons en quoi les méthodes de Galerkine discontinu sont bien adaptées à ces problèmes. Nous présenterons le schéma numérique retenu, et nous détaillerons son implémentation.

                Nous intéresserons ensuite à deux problèmes plus spécifiques: l'imposition de conditions aux limites de parois adiabatiques ou isotherme d'une part, et d'autre part le comportement des schémas à bas nombre de Mach dans le cadre stationnaire et instationnaire.

                Nous présenterons enfin des résultats obtenus sur des écoulements de jets en écoulement traversier.

            .. break_event:: Pause
                :begin: 15:30
                :end: 16:00

            .. event:: Hybrid High-Order methods
                :begin: 16:00
                :end: 17:00
                :speaker: Daniele Di Pietro (Université de Montpellier)
                :support: attachments/spip/IMG/pdf/di_pietro_cnam.pdf

                Hybrid High-Order (HHO) methods are last generation discretization methods for PDE problems with a number of appealing features: the capability of handling general polyhedral meshes with a construction independent of the space dimension; the possibility of arbitrarily selecting the approximation order; the reproduction of desirable continuum properties at the discrete level (integration by parts formulas, operator kernels, symmetries, commuting properties, etc.); reduced computational cost.

                HHO methods for diffusive problems rely upon two ingredients: a high-order reconstruction operator inside cells from the cell- and face-based unknowns, and a stabilization operator linking locally cell-and face-based unknowns while preserving the high order of the reconstruction. The discrete problem is assembled cell-wise as in standard finite elements, and cell-based unknowns can be eliminated by static condensation, leading to a symmetric, positive de-nite linear system coupling the face-based unknowns.

                We provide here an introduction to HHO methods for model problems relevant in continuum mechanics, highlight the general ideas, and present a panel of theoretical results and numerical examples.

.. section:: Les conférenciers
    :class: orga

    - Alexandre Ern (Université Paris-Est, CERMICS , ENPC)
    - Jan Hesthaven (École polytechnique fédérale de Lausanne)
    - Daniele Di Pietro (Université de Montpellier)
    - Vincent Perrier (INRIA, Université de Pau)
    - Jaap van der Vegt (University of Twente)

.. section:: Le comité d'organisation
    :class: orga

    - Stéphane Descombes
    - Thierry Dumont
    - Loïc Gouarin
    - Daniel Le Roux
