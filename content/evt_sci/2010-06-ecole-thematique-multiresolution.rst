École thématique "Méthodes multirésolution et |_| méthodes de |_| raffinement adaptatif de |_| maillage"
########################################################################################################

:date: 2010-02-18 10:36:59
:category: journee
:tags: multirésolution
:start_date: 2010-06-14
:end_date: 2010-06-18
:place: Fréjus
:summary: Le Groupe Calcul propose une école thématique d'une semaine sur les méthodes de multirésolutions et de raffinement adaptatif de maillage.

.. contents::

.. section:: Description
    :class: description

    Depuis quelques années, le besoin se fait sentir de réaliser la simulation
    numérique de problèmes multi-échelles de plus en plus complexes.

    La complexité croissante de ces problèmes, intéressants des disciplines
    scientifiques diverses (combustion, médecine, fusion ...), nécessite des
    méthodes adaptées, tant du point de vue numérique, que du point de vue
    informatique au niveau de leur implémentation. Ces deux aspects sont fortement
    liés.

    Nous proposons, dans cette école thématique, de faire le point sur les méthodes
    de multirésolution adaptatives et les méthodes de raffinement adaptatif de
    maillage pour la simulation des équations aux dérivées partielles évolutives,
    qu'elles soient de type paraboliques ou hyperboliques. Ces méthodes sont
    particulièrement efficaces pour des solutions qui présentent soit des
    singularités, soit des fronts raides, fortement localisés en espace et se
    propageant dans le temps. Elles permettent alors de traiter le caractère multi-
    échelle spatial, et éventuellement temporel, d'un ensemble de problèmes
    couvrant un large spectre d'applications (dynamique des gaz, biomédical,
    combustion, dynamique chimique non-linéaire, décharges plasmas).

    Les objectifs de cette école thématique sont :

        - de faire connaître les avancées récentes du calcul scientifique sur ces méthodes,
        - d'évoquer les questions liées à leur implémentation (structures des données, algorithmes, ...) en lien avec les architectures de calcul émergentes (et notamment le massivement parallèle),
        - d'aborder ces deux points sur le plan pratique.

    L'école s'est déroulée du 14 juin 2010 au 18 juin 2010  à Fréjus. Elle a été constituée de 3 cours théoriques de 6h donnés par K. Schneider, R. Deiterding, Sonia Gomez, Kolja Brix et Sorana Melian (collaborateurs de S.  Mueller). Et s'est terminée par 3h de cours et 3h de TP sur l'implémentation de ces méthodes donnés par Guillaume Latu, Christian Tenaud et Max Duarte. Ces cours ont été complétés en soirée par des exposés scientifiques donnant des applications concrètes de ces méthodes.

    Cette école s'appuie sur un colloque "Multiresolution and Adaptive Methods for Convection-Dominated Problems" organisé en janvier 2009 au Laboratoire Jacques-Louis Lions à Paris, un `mini-symposium du congrès SMAI 2009 sur le sujet <2009-05-mini-symposium-smai.html>`_, ainsi qu'une `journée scientifique du GDR Calcul en automne 2009 <2009-11-journees-groupe-calcul.html>`_.
    Les cours donnés vont être publiés sous la forme de `ESAIM proceedings <http://www.esaim-proc.org/>`__ :

      - Publication editor(s) : E. Cancès (ESAIM, CERMICS - Ecole des Ponts ParisTech),  V. Louvet (Institut Camille Jordan, Lyon), M. Massot (EM2C, ECP, Paris).

    Les supports de cours et les TP sont disponibles sur demande auprès de Violaine Louvet.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 14-06-2010

            .. event:: Adaptive Mesh Refinement Methods for Conservation Laws Theory, Implementation and Application
                :speaker: Ralf Deiterding, Oak Ridge National Laboratory, USA
                :begin: 09:00
                :end: 18:00

        .. day:: 15-06-2010

            .. event:: Adaptive Multiresolution Methods for PDEs: Fundamentals, Modeling and Applications
                :speaker: Kai Schneider, CMI Universite de Provence, France
                :begin: 09:00
                :end: 18:00

        .. day:: 16-06-2010

            .. event:: Adaptive Multiresolution Methods for Conservation Laws: Numerical analysis
                :speaker: Sonia Gomes, Unicamp, Brasil
                :begin: 09:00
                :end: 12:00

            .. event:: Adaptive Multiresolution Methods: Practical issues on Data Structures, Implementation and Parallelization
                :speaker: Kolja Brix  and Sorana Melian, RWTH Aachen, Germany 
                :begin: 14:00
                :end: 18:00

        .. day:: 17-06-2010

            .. event:: Cours Informatique et implémentation
                :speaker: Guillaume Latu, CEA, France 
                :begin: 09:00
                :end: 12:00

                - Multi-resolution methods: some applications
                - Algorithmic and high-performance tools
                - Data structures for multiresolution
                - Some wavelet algorithms (1D)
                - Optimization

            .. event:: TP Informatique et implémentation
                :speaker: Christian Tenaud, LIMSI, France et Max Duarte ECP/EM2C, France
                :begin: 14:00
                :end: 18:00


        .. day:: 18-06-2010

            .. event:: An adaptive mesh refinement method for incompressible two phase-flows
                :speaker: Thibaut Ménard, CORIA, Rouen
                :begin: 09:00
                :end: 09:45

            .. event:: Seminar
                :speaker: Max Duarte, EM2C, ECP
                :begin: 09:45
                :end: 10:30

            .. event:: Seminar
                :speaker: Kolja Brix, RWTH Aachen, Germany
                :begin: 11:00
                :end: 11:45


.. section:: Comité scientifique
    :class: orga

    - Stéphane Descombes (Dieudonné, Nice)
    - Marc Massot (ECP, Paris)
    - Marie Postel (LJLL, Paris)
    - Kai Schneider (CMI, Marseille)
    - Eric Sonnendrücker (IRMA, Strasbourg)
    - Christian Tenaud (LIMSI, Orsay)


.. section:: Comité d'organisation
    :class: orga

    - Max Duarte (EM2C, ECP, Paris)
    - Thierry Dumont (Institut Camille Jordan, Lyon)
    - Sylvain Faure (Maths, Orsay)
    - Loïc Gouarin (Maths, Orsay)
    - Violaine Louvet (Institut Camille Jordan, Lyon)
