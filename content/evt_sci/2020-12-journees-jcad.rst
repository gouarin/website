JCAD, Journées Calcul & Données
###############################

:date: 2021-01-07 10:00:00
:modified: 2021-01-07 10:00:00
:category: journee
:tags: jcad
:start_date: 2020-12-02
:end_date: 2020-12-04
:attendees: 100
:place: En ligne depuis Dijon
:summary: Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GDR RSD, GENCI et les partenaires de l'Equipex EQUIP@MESO organisent ensemble les JCAD, Journées Calcul Données.

.. contents::

.. section:: Description
    :class: description

    Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GDR RSD, GENCI et les partenaires d'EQUIP@MESO organisent ensemble les JCAD 2020, Journées Calcul Données : Rencontres scientifiques et techniques du calcul et des données.

    Elles se dérouleront du 2 au 4 décembre 2020, en partenariat avec l'Université de Bourgogne (uB), l'Université de Franche-Comté (uFC), la Direction du Numérique (DNum) et le Mésocentre Bourgogne-Franche-Comté (MesoBFC).

    Les JCAD sont dédiées à la fois aux utilisateurs et aux experts techniques des infrastructures et des services associés. Les objectifs de ces rencontres sont de présenter des travaux scientifiques, dans toutes les disciplines, réalisés grâce au soutien des infrastructures de grilles de calcul, de méso-centres ou de cloud, les travaux de la recherche en informatique associée et les évolutions techniques et travaux des administrateurs de ces infrastructures.

    Ces journées associeront exposés pléniers, "lightning talks", et démonstrations sur des sujets d'actualité. L’ensemble sera webcasté.

    Pages des journées : https://jcad2020.sciencesconf.org/

