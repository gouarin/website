Algorithmic Differentiation Workshop
####################################

:date: 2019-01-24
:category: journee
:tags: machine learning
:start_date: 2019-01-24
:end_date: 2019-01-24
:place: Paris
:summary: Le groupe Calcul propose un atelier d'une journée sur la différenciation automatique et ses usages courants.

.. contents::

.. section:: Description
    :class: description

    Calculating a derivative can sometimes be more delicate than one might think, specially when the function to be derived is itself the result of a numerical calculation.
    In practice, this poses numerical and algorithmic problems: on the one hand, it is difficult to control the truncation error of a finite difference computation and on the other hand the symbolic computation can lead to an explosion of the complexity of the expressions.

    This need is encountered in problems where one seeks to optimize a quantity resulting itself from an approximate calculation.
    This is formalized through the optimization of a criterion under constraint.
    For example:

    - the optimal control (aerodynamic stability analysis, ...),
    - data assimilation (meteorology, risk analysis, ...),
    - the inverse problems (geophysics, medical imaging, design, ...),
    - sensitivity analysis (quantification of uncertainties, ...),
    - deep learning (neural networks, ...).

    The algorithmic differentiation (AD) allows to get rid of a part of the calculations.
    The AD takes a calculation code, as well as a description of the input and output variables, and produces a new code that calculates the derivatives of the outputs with respect to the inputs.
    This uses concepts from compilation and program analysis (not computer algebra).
    There are two modes of AD, which differ in the way of applying the rule of derivation of a compound function:

    - the direct mode, which is used when the number of inputs is smaller than the number of outputs;
    - the reverse mode, which is used when the number of inputs is greater than the number of outputs, in particular to calculate the gradient of a functional with respect to a large vector.

    The inverse mode is equivalent to the adjoint state calculation (used in optimal control) and is close to retro-propagation in neural networks.
    These results, well known to specialists in each discipline, are not new, however they deserve to be presented from a broader perspective.

    The purpose of this day is to bring together specialists in these different technologies to highlight the common needs, the different use cases and the limits of the different methods.
    Another goal of this day is to meet different communities, who share these techniques.

    The workshop will include educational presentations presenting the principles of the methods, feedback on different applications and presentations recent or more original extensions.

.. section:: Program
    :class: programme

    .. schedule::

        .. day:: 24-01-2019

            .. break_event:: Welcome and introduction
                :begin: 9:00
                :end: 9:30

            .. event:: Challenges and Achievements of Source-Transformation Algorithmic Differentiation
                :begin: 9:30
                :end: 10:30
                :speaker: Laurent Hascoët
                :support: attachments/journee-da-2019/slidesjan19.pdf

            .. event:: Derivative Code by Overloading in C++
                :begin: 10:30
                :end: 11:30
                :speaker: Uwe Naumann
                :support: attachments/journee-da-2019/naumann.pdf

            .. event:: Use of AD in elsA CFD solver
                :begin: 11:30
                :end: 12:00
                :speaker: Sébastien Bourasseau
                :support: attachments/journee-da-2019/sebastienbourasseau_use_of_ad_in_elsa_cfd_solver.pdf

            .. event:: Shape optimisation using AD of complete CFD workflows, including CAD geometry
                :begin: 13:30
                :end: 14:30
                :speaker: Jens-Dominik Mueller
                :support: attachments/journee-da-2019/mueller_small.pdf

            .. event:: Arbogast: "Du calcul des dérivations" to higher order AD
                :begin: 14:30
                :end: 15:30
                :speaker: Isabelle Charpentier
                :support: attachments/journee-da-2019/expo_gdr_calcul_2018.pdf

            .. break_event:: Coffee break
                :begin: 15:30
                :end: 16:00

            .. event:: Beyond backprop: automatic differentiation in machine learning (cancelled)
                :begin: 16:00
                :end: 17:00
                :speaker: Atilim Günes Baydin

                This talk was cancelled. `Here <https://docs.google.com/presentation/d/1m9xp5OoFNBwU8NnTtkmOz5-W9N-YV6S9aK6IzQ34YRs/edit?usp=sharing>`__ is a link provided by the author as an alternative.

.. section:: Location
    :class: description

    Institut de Physique du Globe, 1 rue Jussieu, Paris

.. section:: Contact
    :class: orga

    - Michel Kern
    - Anne Cadiou
