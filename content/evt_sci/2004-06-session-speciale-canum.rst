CANUM 2004 : session spéciale
#############################

:date: 2008-02-07 10:29:27
:modified: 2008-02-07 10:29:27
:category: journee
:tags: canum
:place: Obernai
:start_date: 2004-06-03
:end_date: 2004-06-03
:summary: La session spéciale du Groupe Calcul du CNRS au CANUM en 2004.

.. contents::

.. section:: Description
    :class: description

    La session spéciale a consisté en une discussion autour du calcul puis de trois présentations.

    La discussion a d'abord été l'occasion de présenter, d'une part
    le rôle du chargé de mission calcul scientifique au département
    SPM du CNRS, d'autre part le groupe calcul, la liste de diffusion
    et le site web.

    Les échanges qui ont eu lieu ensuite ont portés sur les points
    suivants :

    - La communauté du calcul a souvent l'impression d'être mal
      considérée et souffre d'un manque de reconnaissance. Il est donc
      important de se regrouper et de se structurer afin de marquer
      une forte cohésion de notre communaut.

    - Il faut aussi que le travail de développement des codes soit
      reconnu à sa juste valeur. Ainsi, au même titre qu'une liste de
      publication, il serait intéressant qu'il y ait la possibilité de
      joindre une liste de logiciels dans les dossiers de carrières ou
      d'avancement, À l'exemple de l'Allemagne. La question qui se pose
      alors est l'évaluation de cet aspect du travail et la prise en
      compte des contraintes qui y sont associées, notamment en matière
      de formation.

    La valorisation des codes est aussi une question importante. Il
    semble ainsi nécessaire de constituer une base de données des
    logiciels développés dans la communauté et de concevoir une
    méthode de publication des codes. Cette démarche pourrait aussi
    favoriser l'utilisation de nos programmes dans d'autres
    disciplines.


    - Les matheux n'utilisent que marginalement les ressources des
      grands centres de calcul que sont l'IDRIS et le CINES et nous
      vous invitons à ne pas hésiter à faire des demandes sur
      http://www.idris.fr/ et http://www.cines.fr/.

    - Concernant la liste de diffusion : La liste calcul a diffusé 210 messages lors des 150 premiers jours de 2004, on peut distinguer plusieurs grands types de messages :

        - Les offres et les demandes d'emploi
        - Les achats et l'installation de cluster
        - Les (choix de) logiciels spécifiques, sorties de nouvelles versions
        - Annonce de rencontres, écoles, congrès, conférences, workshop
        - question de programmation en C, C++, fortran
        - et des messages divers qui ne rentrent pas dans ces types...

    Il semble qu'il y ait une autocensure pour poser des questions
    de [programmation] et il est demandé à chacun de ne pas hésiter
    à poser des questions dans ce domaine.

    Afin d'aider les abonnés à lire les mails qui les intéressent
    vraiment, nous vous demandons d'essayer d'indiquer des sujets
    les plus précis possibles.

    Le Groupe Calcul envisage de monter d'autres actions, notamment
    des journées de formation (Conception et Programmation, Brevet et
    Licence ...). Si vous avez des demandes dans ce domaine, n'hésitez
    pas à nous en faire part.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 03-06-2004

            .. event:: Discussion
                :begin: 10:40
                :end: 11:40

            .. event:: Calcul scientifique et équations de réaction-diffusion
                :begin: 11:40
                :end: 12:00
                :speaker: Thierry Dumont
                :support: attachments/spip/Documents/Manifestations/CANUM2004/canum2004-t.dumont.pdf

            .. event:: Quelques outils pour le parallélisme et la gestion de clusters. Le cas de Hydre
                :begin: 12:00
                :end: 12:20
                :speaker: Pascal Havé
                :support: attachments/spip/Documents/Manifestations/CANUM2004/canum2004-p.have.pdf

            .. event:: Méthodologies de programmation orientées objets en calcul scientifique : le projet Csimoon
                :begin: 12:20
                :end: 12:40
                :speaker: Stéphane Labbé
                :support: attachments/spip/Documents/Manifestations/CANUM2004/canum2004-s.labbe.pdf

