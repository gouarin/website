ANGD "Calcul parallèle et application aux plasmas froids"
#########################################################

:date: 2011-02-08 08:13:10
:modified: 2011-02-08 08:13:10
:category: formation
:tags: angd, plasma
:start_date: 2011-10-10
:end_date: 2011-10-14
:place: Autrans
:summary: Cette formation a pour but d'aider de façon concrète les personnes utilisant la simulation numérique, notamment celles travaillant dans le domaine des plasmas froids, à mieux utiliser les matériels pour le calcul haute performance mis à leur disposition.

.. contents::

.. section:: Introduction
    :class: description

    La modélisation mathématique et la simulation numérique deviennent de plus en plus des outils majeurs dans de nombreux domaines scientifiques. Elles permettent d'une part la réalisation d'un grand nombre d'"expériences numériques" où l'influence de chaque paramètre d'un procédé peut être testée indépendamment de celle des autres, ce qui est souvent impossible sur le plan expérimental. D'autre part, elles s'avèrent précieuses pour réduire les coûts de fabrication d'un banc expérimental, en dégageant les voies les plus prometteuses. Enfin, les résultats numériques, confrontés à l'expérience, permettent d'aller plus loin dans la compréhension des phénomènes mis en jeu, et par la même servent à faire progresser la connaissance scientifique.
    L'évolution des matériels informatiques et des logiciels a débouché sur une augmentation considérable de la puissance de calcul (tout PC même basique possède aujourd'hui un processeur bi- ou quadri-coeurs) et de la taille des problèmes pouvant être traités. Par ailleurs, les outils expérimentaux permettent une observation de la matière à des échelles de plus en plus réduites, entraînant de fait une augmentation de la complexité de la physique mise en jeu dans les codes de calcul et se nourrissant de l'augmentation de la puissance informatique. Toutefois, le numéricien est de plus en plus confronté à un double problème :

    - les temps de simulation d'une expérience de façon réaliste (en prenant en compte toute la complexité) sont de plus en plus longs, voire rédhibitoires,
    - la puissance de calcul dont peut disposer le chercheur/l'ingénieur n'est pas forcément exploitée de manière optimale par le logiciel qu'il utilise.

    Une des possibilités pour pallier ces difficultés consiste à mettre en œuvre des approches de calcul parallèle. Or bien souvent, le spécialiste d'un domaine scientifique donné peut posséder des connaissances de base en calcul scientifique, mais pas forcément suffisamment pour paralléliser un code de calcul qu'il aurait développé.

    Ces constatations s'appliquent par exemple au domaine des plasmas froids à basse pression et à pression atmosphérique. Dans de nombreuses applications de ces plasmas comme le dépôt, la gravure, la dépollution d'effluents gazeux, la combustion assistée par plasma, on a un couplage fort entre la décharge, la cinétique chimique, l'écoulement et des surfaces de géométries souvent assez complexes. Une des difficultés actuelles du point de vue de la modélisation et de la simulation de ces applications plasmas est de pouvoir prendre en compte de manière efficace le grand nombre d'échelles temporelles (de la nanoseconde à la milliseconde) et spatiales (du micromètre au centimètre).

    Un certain nombre de méthodes numériques utilisées dans le domaine des plasmas froids se prêtent bien à une parallélisation. L'objectif de cette formation, conjointe aux réseaux Calcul et Plasmas Froids, mais ouverte à toutes les communautés scientifiques, est précisément de donner aux personnels techniques et aux chercheurs une première approche des possibilités de parallélisation qu'ils peuvent mettre en œuvre dans leurs codes afin qu'ils puissent tirer au mieux profit de la puissance des ressources mises à leur disposition.

    De nombreuses formations existent sur l'apprentissage des outils de parallélisation (par exemple les cours de l'IDRIS). Un point novateur de l'action proposée est que la mise en pratique des outils présentés sera basée sur une application concrète issu d'un domaine de recherche, ici les plasmas froids, qui servira de fil conducteur tout au long de la formation.


.. section:: Objectifs de la formation
    :class: description

    Cette formation a pour but d'aider de façon concrète les personnes utilisant la simulation numérique, notamment celles travaillant dans le domaine des plasmas froids, à mieux utiliser les matériels mis à leur disposition pour résoudre :

    - des problèmes de taille plus importante en un même temps de calcul,
    - des problèmes de même taille plus rapidement,
    - des problèmes plus complexes,

    et ainsi obtenir dans leur domaine de nouveaux résultats scientifiques ou une meilleure compréhension des phénomènes physiques.

    Elle pourra également permettre d'initier et de faciliter les interactions entre la communauté du calcul et les communautés utilisatrices comme celle des plasmas froids (échanges d'expériences, diffusion des connaissances, aide mutuelle), qui pourraient déboucher sur des groupes de travail communs et le développement de collaborations, bons indicateurs du succès de cette formation.


.. section:: Points abordés
    :class: description

    Les objectifs principaux seront de former de manière globale les participants aux connaissances théoriques et pratiques indispensables au développement de codes scientifiques parallèles appliqués à leur domaine :

    - architectures des machines et modèle de programmation parallèle,
    - environnements de développement parallèle (MPI, Open MP),
    - outils de haut niveau (PETSc, etc.),
    - débogage parallèle, profilage et optimisation.

    Le but est de détailler de manière théorique puis (et surtout) pratique comment mettre en œuvre des calculs parallèles dans le cadre d'un code de calcul, en insistant sur le lien entre les algorithmes, l'architecture et la programmation afin d'écrire des logiciels optimisés, portables et réutilisables.

    L'idée n'est pas d'apprendre un langage de programmation (on supposera que les utilisateurs en maîtrisent un) ou des méthodes numériques (objet d'autres formations par ailleurs) mais de savoir implémenter les outils du calcul parallèle en fonction du problème à traiter.

    Cette formation prendra la forme de cours théoriques mis en application lors de TP (au moins la moitié du temps). La densité du programme sera importante.

    Cette ANGD se présente dans la continuité de la formation organisée en 2010 par le `réseau Plamas Froids <http://plasmasfroids.cnrs.fr/>`__ sur la modélisation, et a pour objectif la parallélisation d'un problème type issu de la communauté des Plasmas Froids mais ouvert très largement à toutes les communautés scientifiques. Ceci nous permettra d'aborder les points suivants :

    - architectures des machines et modèle de programmation parallèle (rappel de et/ou complément à la présentation de l'ANGD Modélisation),
    - parallélisation par des méthodes de splitting, décomposition de domaine,
    - modèles de parallélisme (threads et passage de messages) et environnements de développement parallèle (MPI, Open MP, bibliothèques de threads),
    - outils de plus haut niveau (PetSc, etc.),
    - débogage parallèle, profilage et optimisation des codes,
    - post-traitement des données (stockage des résultats, outils de visualisation).

    Les pré-requis pour cette formation sont :

    - notions sur le calcul scientifique
    - connaissance d'un langage de programmation (fortran, C, etc.)
    - notions sur les méthodes numériques pour les équations aux dérivées partielles

    Notion signifiant (pour les méthodes numériques) : connaissance des algorithmes (l'étude mathématique fine des méthodes n'est évidemment ni exigée comme pré-requis, ni envisagée au cours de la formation).

.. section:: Exposés
    :class: description

    - *Architecture et modèles de programmation*, **Françoise Roch**, Observatoire de Grenoble

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/Archi.pdf>`__

    - *openMP*, **Françoise Roch**, Observatoire de Grenoble, Université Joseph Fourier

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/openMP.pdf>`__
        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_BASE_OMP.tgz>`__

    - *Présentation du problème de Laplace et méthodes numériques associées*, **Violaine Louvet**, Institut Camille Jordan, CNRS/Université Lyon 1

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/laplace_num.pdf>`__

    - *Présentation du problème de Laplace*, **Jean  Paillol**, SIAME/IPRA, Université de Pau et des Pays de l'Adour

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/Laplace.pdf>`__

    - *MPI*, **Guy Moebs**, Laboratoire Jean Leray, Université de Nantes

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/ANGD_Plasmas_Froids_Moebs_Cours_MPI.pdf>`__
        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_BASE_MPI.tgz>`__

    - *Débogage et optimisation*, **Jean-Guillaume Piccinali**, Swiss Supercomputing Centre, Suisse
    
        - `Support du cours Débogage <attachments/spip/Documents/Ecoles/PF-2011/Cours/debug_autrans2011.pdf>`__
        - `Support du cours Optimisation <attachments/spip/Documents/Ecoles/PF-2011/Cours/perf_autrans2011.pdf>`__
        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_DEBUG_OPT.tgz>`__

    - *TP Equation de Poisson : openMP, MPI, hybride openMP + MPI*, **Françoise Roch et Guy Moebs**

        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_LAPLACE.tgz>`__

    - *Bibliothèque PETSc*, **Marc Médale**, IUSTI, Université de Provence

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/Intro_PETSc_Autrans.pdf>`__
        - `Support du TP introduction <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_intro_PETSc.tgz>`__
        - `Support du TP PETSc non linéaire <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_non_lineaire_PETSc.tgz>`__

    - *Valorisation, licences et logiciels libres*, **Violaine Louvet**, ICJ, Université Lyon 1 

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/valorisation_pres.pdf>`__

    - *Visualisation avec Gnuplot*, **Fabien Tholin**, École Centrale Paris 

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/gnuplot_THOLIN.pdf>`__
        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_GNUPLOT.tgz>`__

    - *Visualisation avec Gnuplot et Paraview*, **Jean Paillol** 

        - `Support du cours <attachments/spip/Documents/Ecoles/PF-2011/Cours/paraview.pdf>`__
        - `Support des TP <attachments/spip/Documents/Ecoles/PF-2011/TP/TP_PARAVIEW.tgz>`__
